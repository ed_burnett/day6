﻿package 
{
	
	import RoadView;
	import Globals;
	import CarList;
	import Destinations;
	import Shop;
	
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import flash.utils.Timer;
	
	public class Main extends MovieClip {
		
		public var _xDestinations:Destinations;
		public var _xCars:CarList;
		public var _xCarInfo:Object;
		public var _xRoadInfo:Object;
		public var _xRoadTimer:Timer;
		public var _xDestinationInfo:Object;
		public var _bThereFlag = 0;
		public var _nLoadOrder:int = 0;
		
		public function Main(){
			_xDestinations = new Destinations();
			_xDestinations.makeQuoteList();
			
			_xCars = new CarList();
			
			Globals._xRoot = this;
			
			Globals._bGameDay = true;
			Globals._nX = 51;
			Globals._nY = 100;
			Globals._nCurrentHour = 11;
			Globals._nCurrentMin = 0;
			Globals._sCurrentTime = '11:00am';
			Globals._nCurrentGas = 100;
			Globals._nCurrentFood = 100;
			Globals._nTotalMiles = 0;
			Globals._nCurrentDay = 1;
			
			Globals._nMilesToPixel = 4.593;
			
			Globals._nSleepTime = 22;
			Globals._nWakeTime = 7;
			
			Globals._xRoadInfo = new Object;
			Globals._xRoadInfo._nCurrentSpeed = 50;
			
			_xRoadTimer = new Timer(500);
			
			
		}
		
		
		public function startGame(){
			Globals._xRoadInfo._sBg = 'f_refinery';
			Globals._xRoadInfo._sDefaultStop = 'dest_southwest';
			
			setDestination('dest_charleston');
			Globals._sGameMode = 'mode_road';
			
			_xRoadTimer.addEventListener(TimerEvent.TIMER, roadListener);
			_xRoadTimer.start();
			mc_roadView.switchRoad();
		}
		
		public function setDestination(_sDestination:String){
			Globals._sCurrentDestination = _sDestination;
			Globals._xCurrentDestinationInfo = _xDestinations.getDestinationInfo(_sDestination);
		}
		
		public function changeSpeed(p_nSpeedDiff:int){
			if(Globals._sGameMode == 'mode_road'){
				Globals._xRoadInfo._nCurrentSpeed += p_nSpeedDiff;
				if(Globals._xRoadInfo._nCurrentSpeed < 5) Globals._xRoadInfo._nCurrentSpeed = 5;
			}
		}
		
		public function roadLoop(){
			
			advanceTime();
			
			if(Globals._bGameDay){
				eatGas();
				moveCar();
				checkLocation();
			}
			mc_roadView.updateRoadDash();
		}
		
		public function checkLocation(){
			
			if(_bThereFlag){
				_bThereFlag = 0;
				loadTown(Globals._sCurrentDestination);
			}
			
			
			
		}
		
		public function loadTown(p_sTown:String){
			_xDestinationInfo = _xDestinations.getDestinationInfo(p_sTown);
			mc_roadView.freeze();
			loadTownCycle();
			
		}
		
		public function handleTownCycle(){
			
			if(Globals._xHandler._bAdvance){
				loadTownCycle();
			}
			
		}
		
		private function loadTownCycle(){
			
			switch(_nLoadOrder++){
				
				case 0:
					
					if(_xDestinationInfo._sWelcome){
						Globals._sGameMode = 'mode_popup';
						mc_popupView.loadWelcome(_xDestinationInfo._sWelcome);
					}
					//trace('wh');
					
					break;
				
				case 1:
					
					if(_xDestinationInfo._sIntroScreen){
						Globals._sGameMode = 'mode_popup';
						mc_popupView.loadIntroScreen(_xDestinationInfo._sIntroScreen);
						
					}
					break;
				case 2:
					
					if(_xDestinationInfo._xTownInfo){
						Globals._sGameMode = 'mode_town';
						mc_townView.loadTown(_xDestinationInfo._xTownInfo);
						
					}
					
					break;
				
			}
			
			
		}
		
		
		public function moveCar(){
			
			_xDestinationInfo = Globals._xCurrentDestinationInfo;
			var l_nTotalDistance:Number = Math.sqrt( Math.pow( ( _xDestinationInfo._nX - Globals._nX ),2)+ Math.pow(( _xDestinationInfo._nY - Globals._nY ),2)   );
			var l_nDistance:Number;
			_xCarInfo = _xCars.getCarInfo(Globals._xRoadInfo._sCar);
			l_nDistance = (Globals._xRoadInfo._nCurrentSpeed  / 6)/Globals._nMilesToPixel;
			var l_nRatio = l_nTotalDistance/l_nDistance;
			var l_nNewX:Number;
			l_nNewX =   ( _xDestinationInfo._nX - Globals._nX ) / l_nRatio;
			var l_nNewY:Number;
			l_nNewY = ( _xDestinationInfo._nY - Globals._nY ) / l_nRatio;
			
			if(l_nRatio  < 1){
				_bThereFlag = true;
				Globals._nY = _xDestinationInfo._nY ;
				Globals._nX = _xDestinationInfo._nX ;
				Globals._nDistanceToDestination = 0;
			}
			else{
				Globals._nY = Globals._nY + l_nNewY ;
				Globals._nX = Globals._nX + l_nNewX ;
				Globals._nDistanceToDestination = (l_nTotalDistance-l_nDistance)*Globals._nMilesToPixel;
			}
			
			trace(Globals._nX+' '+Globals._nY);
			
		}
		
		public function eatGas(){
			_xCarInfo = _xCars.getCarInfo(Globals._xRoadInfo._sCar);
			Globals._nCurrentGas -= Globals._xRoadInfo._nCurrentSpeed  / (6 *  _xCarInfo.mpg);
		}
		
		public function roadListener(event:TimerEvent){
			if(Globals._sGameMode == 'mode_road'){
				roadLoop();
			}
		}
		
		public function advanceTime(){
			if(Globals._bGameDay){
				Globals._nCurrentMin ++;
				if(Globals._nCurrentMin == 6){
					Globals._nCurrentHour++;
					Globals._nCurrentMin = 0;
				}
			}else{
				Globals._nCurrentHour ++;
			}
			if(Globals._nCurrentHour == 24){
				Globals._nCurrentHour = 0;
				Globals._nCurrentDay++;
			}
			if(Globals._nCurrentHour == 0)
				Globals._sCurrentTime = '12';
			else if(Globals._nCurrentHour > 12)
				Globals._sCurrentTime = String(Globals._nCurrentHour - 12);
			else
				Globals._sCurrentTime = String(Globals._nCurrentHour);
				
			Globals._sCurrentTime += ':'+String(Globals._nCurrentMin)+'0'; 
			Globals._sCurrentTime += (Globals._nCurrentHour > 11)?'pm':'am';
			
			if(Globals._bGameDay && Globals._nCurrentHour == Globals._nSleepTime){
				mc_roadView.freeze();
				Globals._bGameDay = false;
			}
			if((!Globals._bGameDay) && Globals._nCurrentHour == Globals._nWakeTime){
				mc_roadView.unfreeze();
				Globals._bGameDay = true;
			}
		}
		public function changeCar(p_sCarType:String){
			Globals._xRoadInfo._sCar = p_sCarType;
		}
		
		
	}
	
}
	
	