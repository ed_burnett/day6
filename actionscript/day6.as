package {
	import com.uglypersonsface.Controller;
	
	import flash.display.Sprite;
	import flash.display.StageAlign; 
	import flash.display.StageScaleMode;

	[SWF(widthPercent="100", heightPercent="100", backgroundColor="#000000")] 
	
	public class day6 extends Sprite {
		
		
		public function day6 () {
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT; 
			Controller.instance.initialize(this); 
		}
	}
}