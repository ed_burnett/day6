package com.uglypersonsface.models {
	import com.codeandtheory.core.models.CoreModelEvent;
	
	import flash.events.Event;


	public class ModelEvent extends CoreModelEvent {
		
		// Data events
		public static const CHANGE_DATA:String = "CHANGE_DATA";
		public static const CHANGE_SCENERIO:String = "CHANGE_SCENERIO";
		public static const INPUT_ACCEPT:String = "INPUT_ACCEPT";
		public static const PICK_AILMENT:String = "PICK_AILMENT";
		
		
		// Intro events
		public static const START_INTRO_VIEW:String = "START_INTRO_VIEW";
		public static const END_INTRO_VIEW:String = "END_INTRO_VIEW"; 
		
		
		// Road events
		public static const START_ROAD_VIEW:String = "START_ROAD_VIEW";
		public static const END_ROAD_VIEW:String = "END_ROAD_VIEW";
		public static const FREEZE_ROAD_VIEW:String = "FREEZE_ROAD_VIEW";
		public static const TMP_FREEZE_ROAD_VIEW:String = "TMP_FREEZE_ROAD_VIEW";
		public static const TMP_CONTINUE_ROAD_VIEW:String = "TMP_CONTINUE_ROAD_VIEW";
		public static const PULLOVER_HIT:String = "PULLOVER_HIT";
		
		public static const CONTINUE_ROAD_VIEW:String = "CONTINUE_ROAD_VIEW";
		public static const ROAD_TIMER_HIT:String = "ROAD_TIMER_HIT";
		public static const MPG_HIT:String = "MPG_HIT";
		
		// Arrivial Events
		public static const LOAD_ARRIVIAL:String = 'LOAD_ARRIVIAL';
		public static const POPUP_VISTA:String = 'POPUP_VISTA';
		public static const POPUP_SIGN:String = 'POPUP_SIGN';
		public static const POPUP_TEXT:String = 'POPUP_TEXT';
		public static const POPUP_OPTIONS:String = 'POPUP_OPTIONS';
		
		
		// Popup Events
		public static const ADVANCE_ACTIONS:String = 'ADVANCE_ACTIONS';
		public static const OVERLAY_CLICK:String = 'OVERLAY_CLICK';
		public static const BARTER_POPUP:String = 'BARTER_POPUP';
		public static const FINISH_BARTER:String = 'FINISH_BARTER';
		public static const ANNOUNCE_POPUP:String = 'ANNOUNCE_POPUP';
		public static const HIDE_OUT:String = 'HIDE_OUT';
		public static const ITEM_MOD_ALTER:String = 'ITEM_MOD_ALTER';
		
		
		// Btn Clicked
		public static const BUTTON_CLICK:String = 'BUTTON_CLICK';
		
		// Status
		public static const DISPLAY_STATUS:String = 'DISPLAY_STATUS';
		public static const STATUS_BINYAH_CLICK:String='STATUS_BINYAH_CLICK'
		public static const HIDE_STATUS_BAR:String='HIDE_STATUS_BAR'
		public static const SHOW_STATUS_BAR:String='SHOW_STATUS_BAR'
		
		
		// Town Events
		public static const INIT_TOWN:String = 'INIT_TOWN';
		public static const BACK_TO_ROAD_CLICK:String = 'BACK_TO_ROAD_CLICK';
		public static const BACK_TO_ROAD_SUCCESS:String = 'BACK_TO_ROAD_SUCCESS';
		public static const BACK_TO_ROAD_FAIL:String = 'BACK_TO_ROAD_FAIL';
		public static const TOWN_MAP_CLICK:String = 'TOWN_MAP_CLICK';
		public static const STATUS_BTN_CLICK:String = 'STATUS_BTN_CLICK';
		public static const COLLECT_BROCHURE:String = 'COLLECT_BROCHURE';
		public static const HIDE_BTN_CLICK:String = 'HIDE_BTN_CLICK';
		public static const HUNT_BTN_CLICK:String = 'HUNT_BTN_CLICK';
		public static const HUNT:String = 'HUNT';
		
		
		// Map Events
		public static const SHOW_MAP:String = 'SHOW_MAP';
		
		override public function clone():Event{
		    var ev:ModelEvent = new ModelEvent(_type, _data);
		    return ev;
		}
		


		public function ModelEvent (p_sType:String, p_xData:Object = null, p_bBubbles:Boolean = false, p_bCancellable:Boolean = false) {
			super(p_sType, p_xData, p_bBubbles, p_bCancellable);
		}
	}
}