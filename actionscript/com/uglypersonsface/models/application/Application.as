package com.uglypersonsface.models.application {
	
	import com.uglypersonsface.Controller;
	import com.uglypersonsface.globals.*;
	import com.uglypersonsface.models.Model;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.objects.*;


	public class Application extends Model {
		
		private var _xController:Controller = null;
		private var _xCurrentDestination:Destination = null;

		public var _aActionStack:Array = null;
		
		public function Application (p_xController:Controller) {
			_xController = p_xController; 
		}
		
		
		public function startGame () : void {
			dispatchEvent(new ModelEvent(ModelEvent.START_INTRO_VIEW )); 
		}
		
		
		// DISPATCHERS
		public function displayStatus():void{
			dispatchEvent(new ModelEvent(ModelEvent.DISPLAY_STATUS )); 
		}
		public function endIntro() : void{
			dispatchEvent(new ModelEvent(ModelEvent.END_INTRO_VIEW )); 
		}
		public function startRoad():void{
			dispatchEvent(new ModelEvent(ModelEvent.START_ROAD_VIEW )); 
		}
		public function freezeRoad():void{
			dispatchEvent(new ModelEvent(ModelEvent.FREEZE_ROAD_VIEW )); 
		}
		public function tmpFreezeRoad():void{
			dispatchEvent(new ModelEvent(ModelEvent.TMP_FREEZE_ROAD_VIEW )); 
		}
		
		public function continueRoad():void{
			dispatchEvent(new ModelEvent(ModelEvent.CONTINUE_ROAD_VIEW ));
		}
		public function tmpContinueRoad():void{
			dispatchEvent(new ModelEvent(ModelEvent.TMP_CONTINUE_ROAD_VIEW )); 
			dispatchEvent(new ModelEvent(ModelEvent.DISPLAY_STATUS )); 
		}
		public function roadTimerHit():void{
			dispatchEvent(new ModelEvent(ModelEvent.ROAD_TIMER_HIT ))
		}
		
		public function barterPopup(p_aGoods:Array,p_sGoodsHeader:String):void{
			dispatchEvent(new ModelEvent(ModelEvent.BARTER_POPUP, {_aGoods: p_aGoods,_sGoodsHeader: p_sGoodsHeader} )); 
		}
		public function dataChange():void{
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_DATA )); 
		}
		
		public function finishBarter():void{
			dispatchEvent(new ModelEvent(ModelEvent.FINISH_BARTER ));
		}
		public function changeSpeed():void{ 
			dispatchEvent(new ModelEvent(ModelEvent.MPG_HIT ));
		}
		
		public function backToRoadSuccess():void{
			dispatchEvent(new ModelEvent(ModelEvent.START_ROAD_VIEW )); 
			dispatchEvent(new ModelEvent(ModelEvent.DISPLAY_STATUS )); 
		}
		public function backToRoadFail():void{
			dispatchEvent(new ModelEvent(ModelEvent.POPUP_TEXT,{ _sTextString:'You need to choose a new destination!'}));
		}
		public function showMap():void{ 
			dispatchEvent(new ModelEvent(ModelEvent.SHOW_MAP));
		}
		public function announcePopup(p_xData:Object):void{
			dispatchEvent(new ModelEvent(ModelEvent.ANNOUNCE_POPUP,p_xData));
		}
		public function statusBtnClick():void{
			dispatchEvent(new ModelEvent(ModelEvent.STATUS_BTN_CLICK));
		}
		public function hideBtnClick():void{
			dispatchEvent(new ModelEvent(ModelEvent.HIDE_BTN_CLICK));
		}
		public function huntBtnClick():void{
			
			if(G._xCurrentDestination._nType == 5){
				addAnnouncement('You absolutely will not destroy the Roadside Attaction'); 
			}
			else if(G._xCurrentDestination._bIsRuined){
				addAnnouncement('Cannot destroy a destroyed city'); 
			}else{
				dispatchEvent(new ModelEvent(ModelEvent.HUNT_BTN_CLICK));
			}
			popAction();
			
		}
		
		
		// Queue Manipulators
		
		public function addAnnouncement(p_sString:String):void{
			_aActionStack.unshift( new ModelEvent(ModelEvent.POPUP_TEXT,{ _sTextString:p_sString}));
		}
		
		public function executeTownScenerio(p_xScenario:Scenario):void{
			_aActionStack = new Array();
			Data.changeData(p_xScenario._aResultData)
		}
		
		public function executeScenerio(p_xScenario:Scenario):void{
			_aActionStack = new Array();
			freezeRoad()
			// Switch on result type
			switch(p_xScenario._sResultType){ 
				case 'speak':
					// Handled later
					if(p_xScenario._bIsAilment){
						break;
					}
					// For Showing Modifications in the popup
					_aActionStack.unshift( new ModelEvent(ModelEvent.POPUP_TEXT,{ _sTextString:p_xScenario._sResultSpeak,_aData:p_xScenario._aResultData}));
					if(p_xScenario._sSpeakFollow){
						_aActionStack.unshift( new ModelEvent(ModelEvent.CHANGE_SCENERIO,{_sSlug:p_xScenario._sSpeakFollow}));
					}
				break;  
				case 'option':
					_aActionStack.unshift( new ModelEvent(ModelEvent.POPUP_OPTIONS,{ _sTextString:p_xScenario._sResultSpeak, _aOptions:p_xScenario._aResultOptions}));	

				break;
				case 'branch':
					var l_xBranch:Object = new Object; 
					switch(p_xScenario._sBranchOn){
						case 'roll':		
							var l_nRoll:int = G.roll()
							for each(l_xBranch in p_xScenario._aResultBranch){
								if(int(l_xBranch._sFactor) <= l_nRoll){
									_aActionStack.unshift( new ModelEvent(ModelEvent.CHANGE_SCENERIO,{_sSlug:l_xBranch._sFollow}));
									break;
								}
							}
						break;
						case 'charexists':
							for each(l_xBranch in p_xScenario._aResultBranch){
								trace('foreach')
								if(l_xBranch._sFactor == "exists" && G.totalCount() > 0){
									_aActionStack.unshift( new ModelEvent(ModelEvent.CHANGE_SCENERIO,{_sSlug:l_xBranch._sFollow}));
									break;
								}else if(l_xBranch._sFactor == "notexists" && G.totalCount() == 0){
									_aActionStack.unshift( new ModelEvent(ModelEvent.CHANGE_SCENERIO,{_sSlug:l_xBranch._sFollow}));
									break;
								}
							}
						break;
					}
				break;
			}
			Data.changeData(p_xScenario._aResultData)
			popAction();
		}
		
		
		public function loadArrivial(p_xDestination:Destination):void{
			dispatchEvent(new ModelEvent(ModelEvent.LOAD_ARRIVIAL )) 
			
			_xCurrentDestination = p_xDestination;
			_aActionStack = new Array();
			freezeRoad();
			
			// City Sign
			if(_xCurrentDestination._bArrivialSign){
				_aActionStack.unshift( new ModelEvent(ModelEvent.POPUP_SIGN,{_sSignString:_xCurrentDestination._sShortName.toUpperCase()}));
			}else{ 
				_aActionStack.unshift( new ModelEvent(ModelEvent.POPUP_TEXT,{ _sTextString:'You have arrived at\n'+_xCurrentDestination._sName}));
			}
			
			// Vista Screen
			if(_xCurrentDestination._sVista && !_xCurrentDestination._bNoVista){
				_aActionStack.unshift( new ModelEvent(ModelEvent.POPUP_VISTA,{_sFrame:_xCurrentDestination._sVista}));
			} 
			
			// Destination
			if(_xCurrentDestination._bTownView){ 
				_aActionStack.unshift( new ModelEvent(ModelEvent.INIT_TOWN,{_xDestination:_xCurrentDestination}));
			}
			
			popAction();
			
		}
		
		
		
		public function advanceActions():void{
			popAction();
		}
		public function popAction():Boolean{
			trace('popaction')
			if ( areMoreActions()){
				var l_xEvent:ModelEvent = _aActionStack.pop()
				dispatchEvent(l_xEvent);
				return true
			}
			else{
				 if(G._sMode==G.MODE_ROAD){
					l_xEvent = ( new ModelEvent(ModelEvent.CONTINUE_ROAD_VIEW));
					dispatchEvent(l_xEvent);
				}else if(G._sMode==G.MODE_STATUS){
					l_xEvent = ( new ModelEvent(ModelEvent.STATUS_BTN_CLICK));
					dispatchEvent(l_xEvent);
				}else if(G._sMode==G.MODE_DESTINATION){
					displayStatus()
				}
			}
			return false
			
		}
		
		public function areMoreActions():Boolean{
			return(_aActionStack && _aActionStack.length)
		}
		
		
	}
}