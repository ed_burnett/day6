package com.uglypersonsface.assets {
	import com.codeandtheory.core.assets.CoreEmbeddedAssetSwf;


	public class AssetSwf extends CoreEmbeddedAssetSwf {
		[Embed(source="day6_assets.swf")]
		private var AssetSwfClass:Class;


		public function AssetSwf () {
			super(AssetSwfClass);
		}
	}
}