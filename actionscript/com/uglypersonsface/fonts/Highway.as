package com.uglypersonsface.fonts{
	import com.uglypersonsface.Controller;
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.models.*;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.objects.*;
			[Embed(source="./highway.otf",
        		fontName = "highway",
				mimeType = "application/x-font"
       		)]
public static var highway:Class;

Font.registerFont(highway);

}