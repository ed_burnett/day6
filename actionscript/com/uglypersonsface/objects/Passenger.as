package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;

	public class Passenger{
		
	        	
		public var _sName:String;
		public var _nHealth:int;
		public var _nMaxHealth:int;
		public var _bDead:Boolean;
		public var _aAilments:Object
		public var _bIsBinyah:Boolean;
		public var _sLastDamage:String
		
		public function Passenger(p_sName:String, p_nHealth:int = 100, p_bIsBinyah:Boolean = false   ){

			_sName = p_sName;
			_nHealth = p_nHealth
			_nMaxHealth = p_nHealth;
			_bDead = false;
			_aAilments = new Array;
			_bIsBinyah = p_bIsBinyah
			
		}
		
		
		public function addAilment(ailment:String,p_sIndexOfAilment:String):void{
			
			_aAilments[p_sIndexOfAilment] = ailment
			_nHealth -= 50
			_sLastDamage = ailment
			
			trace(_aAilments)
			
		}
		public function removeAilment(p_sIndexOfAilment:String):void{
			delete _aAilments[p_sIndexOfAilment]
			_nHealth += 50
			if(_nHealth>_nMaxHealth)
				_nHealth=_nMaxHealth
		}
		
		public  function  toString():String{
			return _sName ;
		}
		
		public function starve():void{
			_nHealth-=3
			_sLastDamage = "starvation"
		}
		
		public function feedHealth():void{
			_nHealth++
			if(_nHealth>_nMaxHealth)
				_nHealth=_nMaxHealth
		}
		

	}
}