package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;

	import com.uglypersonsface.globals.Events;
	
	
	
	public class Car{
		
		public var _sSlug:String;
		public var _sName:String;
		public var _nMPG:int;
		public var _nMaxSpeed:int;
		public var _nGals:int;
		public var _nStress:int;
		public var _sDesc:String 
		public var _nCost:int
		
		public function Car(p_sSlug:String, p_sName:String, p_nMPG:int , p_nMaxSpeed:int, p_nGals:int , p_nStress:int, p_sDesc:String, p_nCost:int ){

			_sSlug = p_sSlug;
			_sName = p_sName;
			_nMPG = p_nMPG;
			_nMaxSpeed = p_nMaxSpeed;
			_nGals = p_nGals;
			_nStress = p_nStress
			_sDesc = p_sDesc
			_nCost = p_nCost
			
		}
		
		public  function  toString():void{
			
			trace( _sSlug );
			
		}

	}
}