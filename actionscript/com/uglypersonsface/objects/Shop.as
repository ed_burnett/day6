package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;
	
	
	public class Shop{
		
		public var _sName:String;
		public var _sImg:String;
		public var _sSlug:String;
		public var _sType:String;
		public var _sHeader:String;
		public var _nCharCount:int = 0;
		
		public var _aChars:Array;
		public var _aCharsQuotes:Array;
		public var _aCharsChance:Array;
		
		public var _aGoods:Array;
		public var _sGoodsHeader:String;
		
		public var _bHasBrochure:Boolean;
		
		public function Shop(p_sName:String,p_sImg:String,p_sSlug:String,p_sType:String,p_sGoodsHeader:String){
			_sName = p_sName;
			_sImg = p_sImg;
			_sSlug = p_sSlug;
			_sType = p_sType;
			_sGoodsHeader = p_sGoodsHeader;
			
			_aChars = new Array();
			_aCharsChance = new Array();
			_aCharsQuotes = new Array();
			_aGoods = new Array();
			
			_bHasBrochure=(_sType=='brochure')
				
		}
		
		public function addChar(p_sCharSlug:String, p_nCharChance:int, p_nCharQuotes:String):void{
			
			_aChars.push(p_sCharSlug);
			_aCharsChance.push(p_nCharChance);
			_aCharsQuotes.push(p_nCharQuotes); 
			_nCharCount++;
		}
		
		public function chooseChar():Array{
			
			var l_nRand:Number =  G.roll();
			var l_xReturnChar:String;
			
			for ( var i:int = 0; i < _nCharCount; i++ ){
				
				var l_nTmpChance:int =  _aCharsChance[i];
				 
				if(l_nTmpChance<=l_nRand){ 
					
					return new Array(  _aChars[i], _aCharsQuotes[i]  );
				}
				
			}
			return null; 
		}
		
		public function addGoods(p_xGoods:Goods):void{
			_aGoods.push(p_xGoods);
		}
		
		public function clone():Shop{
			var l_xClone:Shop = new Shop(_sName, _sImg,_sSlug,_sType,_sHeader)
			l_xClone._nCharCount = _nCharCount
			l_xClone._aChars = _aChars
			l_xClone._aCharsQuotes = _aCharsQuotes
			l_xClone._aCharsChance = _aCharsChance
			
			var l_aGoodsClone:Array = new Array()
			for each( var l_xGoods:Goods in _aGoods ){
				l_aGoodsClone.push(l_xGoods.clone())
			}

			l_xClone._aGoods = l_aGoodsClone
			l_xClone._sGoodsHeader = _sGoodsHeader
			l_xClone._bHasBrochure = _bHasBrochure
			return l_xClone
		}
		
		public  function  toString():void{
			
			trace( _sName , _sImg, _sType);
			
		}

	}
}