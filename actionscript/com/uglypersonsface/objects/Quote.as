package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;

	
	
	
	
	public class Quote{
		
		public var _sSlug:String;
		
		public var _nTextCount:int = 0;
		
		public var _aText:Array;
		public var _sLastText:String = ''
		
		public function Quote(p_sSlug:String){
			_sSlug = p_sSlug;
			
			_aText = new Array();
		}
		
		public function addText( p_sText:String):void{
			
			_aText.push(p_sText);
			_nTextCount++;
		}
		
		public function chooseText():String{
			
			do{
				var l_nRand:Number =  Math.random();
			
				var l_nIndex:int = Math.floor( l_nRand * _nTextCount  );
			
			}while( _aText.length != 1 && _sLastText == _aText[l_nIndex] )
			_sLastText = _aText[l_nIndex]
			return _aText[l_nIndex];
		}
		
		
		public  function  toString():void{
			
			trace( _sSlug );
			
		}

	}
}