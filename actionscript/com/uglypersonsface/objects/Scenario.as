package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;

	
	
	
	
	public class Scenario{
		
		public var _sId:String;
		public var _sType:String;
		public var _sImg:String;
		public var _sSpeakFollow:String;
		public var _sBranchOn:String;
		public var _bIsAilment:Boolean
		public var _nXmin:int;
		public var _nXmax:int;
		public var _nYmin:int;
		public var _nYmax:int;
		public var _sRegion:String;
		
		public var _sResultType:String;
		public var _sResultSpeak:String;
		public var _aResultData:Array;
		public var _aResultDataOrig:Array;
		public var _aResultOptions:Array;
		public var _aResultBranch:Array;
		
		public function Scenario(p_sId:String, p_sType:String ){
			
			_sId = p_sId			
			_sType = p_sType
			
		}
		
		public  function  toString():void{
			
			trace( _sId );
			
		}
		

	}
}