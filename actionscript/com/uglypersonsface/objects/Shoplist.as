package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;
	import com.uglypersonsface.objects.Shop;

	
	
	
	
	public class Shoplist{
		
		public var _sName:String;
		public var _aShops:Array;
		public var _aShopsChance:Array;
		public var _nShopCount:int = 0;
		public var _sQuoteOverride:Array;
		
		public function Shoplist(p_nSlug:String){
			_aShops = new Array();
			_aShopsChance = new Array();
			_sName = p_nSlug;
		}
		public function addShopData(p_sShopName:String, p_nShopChance:Number, p_sQuoteOverride:String):void{	
		//	trace('adding',p_sShopName,p_nShopChance,'to',_sName);
			
			_aShops.push(p_sShopName);
			_aShopsChance.push(p_nShopChance);
			_nShopCount++;
		}
		
		
		public function getShopData():Array{
			var l_xShopArray:Array = new Array();
			
			for( var i:int = 0; i < _nShopCount; i++ ){
				if( Math.random() < _aShopsChance[i] ){							
					l_xShopArray.push( _aShops[i]  );
				}
			}
			return l_xShopArray;
		}
		
		public function toString():void{
			
			trace( _sName );
			
		}

	}
}