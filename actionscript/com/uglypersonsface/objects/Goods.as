package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;

	
	
	
	
	public class Goods{
		
		public var _sItemType:String
		public var _sName:String
		public var _nCost:Number
		public var _sSlug:String	
		public var _nTotalCount:int
		public var _nCurrentCount:int
		public var _xMods:Object
		
		public function Goods(p_sItemType:String,p_sName:String,p_nCost:Number ,p_sSlug:String, p_nCount:int, p_xMods:Object){
			//trace("Adding Goods "+ p_sItemType, p_sName, p_nCost, p_sSlug, p_nCount)
			_sItemType = p_sItemType;
			_sName = p_sName;
			_nCost = p_nCost;
			_sSlug = p_sSlug;
			_nTotalCount = p_nCount;
			_nCurrentCount = p_nCount;
			_xMods = p_xMods
		}
		
		public function clone():Goods{
			var l_xGoods:Goods = new Goods(_sItemType, _sName,_nCost,_sSlug,_nTotalCount, _xMods)
			return l_xGoods
		}
		

		
		public  function  toString():void{
			
			trace( _sItemType );
			
		}

	}
}