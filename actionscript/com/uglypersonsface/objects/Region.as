package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;

	public class Region{
		
		public var _sName:String;
		public var _sBg:String;
		public var _sSlug:String;
		public var _sType:String;
		public var _aDescription:Array
		
		
		public function Region(p_sSlug:String,p_sName:String,p_sBg:String,p_sType:String,p_aDescription:Array){
			_sName = p_sName;
			_sBg = p_sBg;
			_sSlug = p_sSlug;
			_sType = p_sType;
			_aDescription = p_aDescription
		}
		
		public function getDescription():String{
			
			return _aDescription[Math.floor(Math.random()*_aDescription.length)]
			
		}
		
		
		public  function  toString():void{
			
			trace( _sName );
			
		}

	}
}