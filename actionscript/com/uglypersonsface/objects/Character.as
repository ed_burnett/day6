package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;

	
	
	
	
	public class Character{
		
		public var _sSlug:String;
		
		public var _nInstanceCount:int = 0;
		
		public var _aInstanceNames:Array;
		public var _aInstanceImg:Array;
		
		public function Character(p_sSlug:String){
			_sSlug = p_sSlug;
			
			_aInstanceNames = new Array();
			_aInstanceImg = new Array();
		}
		
		public function addInstance( p_sInstanceImg:String, p_sInstanceName:String):void{
			
			_aInstanceImg.push(p_sInstanceImg); 
			_aInstanceNames.push(p_sInstanceName);
			_nInstanceCount++;
		}
		
		public function chooseInstance():Array{
			
			var l_nRand:Number =  Math.random();
			
			var l_nIndex:int = Math.floor( l_nRand * _nInstanceCount  );
			
			return new Array( _aInstanceNames[l_nIndex], _aInstanceImg[l_nIndex] );
			
		}
		
		
		public  function  toString():void{
			
			trace( _sSlug );
			
		}

	}
}