package com.uglypersonsface.objects{
	
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;

	
	
	
	
	public class Destination{
		
		public var _sSlug:String;
		public var _sName:String;
		public var _sShortName:String;
		public var _nX:int;
		public var _nY:int;
		public var _nType:int;
		public var _bArrivialSign:Boolean = false; 
		public var _sVista:String = null; 
		public var _aShoplist:Array = new Array(); 
		public var _bTownView:Boolean = false;
		public var _sDescription:String = null;
		public var _nSimeonIndex:int = 0;
		public var _bOnMap:Boolean = false;
		public var _bNameKnown:Boolean = false;
		public var _aRevealed:Array = new Array()
		public var _bIsRuined:Boolean = false
		public var _sCondition:String = ''
		public var _bNoVista:Boolean = false
		public var _nRating:int = 0
		public var _nDefense:int = 0
		public var _nInitDefense:int = 0
		
		public var _aCachedShops:Array = null
		
		public function Destination(p_sSlug:String,p_sName:String, p_nX:int, p_nY:int, p_nType:int, p_nSimeonIndex:int, p_xData:Object,p_nRating:int=0){
		//	trace(p_xData._bArrivialSign);
			
			_sSlug = p_sSlug;
			_sName = p_sName;
			_nX = p_nX;
			_nY = p_nY;
			_nSimeonIndex = p_nSimeonIndex;
			_nType = p_nType;
			_nRating = p_nRating
			
			if(p_xData._sVista){
				_sShortName = p_xData._sShortName;
				_sVista = p_xData._sVista;
			}
			if(p_xData._bNoVista) 
				_bNoVista = true;
			if(p_xData._aShoplist.length){
				_bTownView = true;
				_aShoplist = p_xData._aShoplist;
			}
			if(p_xData._bArrivialSign == true)
				_bArrivialSign = true;
			if(p_xData._sDescription)
				_sDescription = p_xData._sDescription;
			if(p_xData._sDescription)
				_aRevealed = p_xData._aRevealed;
			
			
			_nDefense = ((_nType+1) * (_nRating+1) ) * 70		
			_nInitDefense = _nDefense
		}
		
		public function defense():String{
			trace("defense",_nDefense)
			if(_nDefense > 300){
				return "Very Strong"
			}else if(_nDefense > 200){
				return "Strong"
			}else if(_nDefense > 100){
				return "Weak"
			}else{
				return "Very Weak"
			}
		}
		
		public function type():String{
			var l_sType:String = ''
						
        	switch(_nType){
        		case 0:
        		l_sType= 'Rural Town';
        		break
        		case 1:
        		l_sType= 'Suburbia';
        		break
        		case 2:
        		l_sType= 'City';
        		break;
				case 3:
        		l_sType= 'Metropolis';
        		break;
				case 5:
        		l_sType= 'Roadside Attaction';
        		break;
        	}
        	return l_sType;
		}
		
		public function condition():String{
			var l_sCondition:String = ''
			
			if (_bIsRuined){
				return 'Destroyed ' 
			}
			if (_nType == 5){
				return '' 
			}
			
        	switch(_nRating){
        		case 0:
        		l_sCondition= 'Scattered ';
        		break
        		case 1:
        		l_sCondition= 'Sprawling ';
        		break
        		case 2:
        		l_sCondition= 'Dense ';
        		break;
				case 3:
        		l_sCondition= 'Very Dense ';
        		break;
        	}
        	return l_sCondition;
		}
		
		public function toString():void{
			
			trace( _sName, _nX, _nY, _bArrivialSign, _sVista, _aShoplist );
			
		}

	}
}