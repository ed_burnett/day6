package com.uglypersonsface {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.fonts.*;
	import com.uglypersonsface.globals.*;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.objects.*;
	import com.uglypersonsface.views.*;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.*; 
	
	
	public class Controller {
		// external ui elements
		private var _root:DisplayObjectContainer = null;

		// assets
		private var _assetSwf:AssetSwf = null; 

		// models
		public var _application:Application = null;

		// views
		private var _viewsContainer:Sprite = null;
		private var _roadView:RoadView = null;
		private var _introView:IntroView = null; 
		private var _popupView:PopupView = null; 
		private var _statusView:StatusView = null; 
		private var _townView:TownView = null; 
		private var _mapView:MapView = null;  
		private var _borderView:BorderView = null; 
		private var _nRand:int= G.roll(100)*G.roll(100)
		

//--------------------------------------------------------------------------------------------------
//	INITIALIZE SEQUENCE
		public function initialize (p_xRoot:DisplayObjectContainer) : void {
			trace("initialize!")
			_root = p_xRoot;

			initializeAssets();
			initializeModels();
			initializeViews();
			initializeListeners();
			loadXml();
			start();
			
			
			Events._controller = this;
			Data._controller = this;
		} 

		
		
		private function initializeAssets () : void {
			_assetSwf = new AssetSwf();
			G._assetSwf = _assetSwf;
			Events._assetSwf = _assetSwf;
			Data._assetSwf = _assetSwf;
		}
		private function initializeModels () : void {
			_application = new Application(this);
			G._application = _application;
			Events._application = _application;
			Data._application = _application;
		}
		
		

		private function initializeViews () : void {

			_introView = new IntroView(_application, _assetSwf);
			_roadView = new RoadView(_application, _assetSwf);
			_statusView = new StatusView(_application, _assetSwf);
			_townView = new TownView(_application, _assetSwf);
			_popupView = new PopupView(_application, _assetSwf);
			_mapView = new MapView(_application, _assetSwf);
			_borderView = new BorderView(_application, _assetSwf);
			
			_viewsContainer = new Sprite();
			_viewsContainer.addChild(_roadView);
			_viewsContainer.addChild(_townView);
			_viewsContainer.addChild(_mapView);
			_viewsContainer.addChild(_statusView);
			_viewsContainer.addChild(_introView); 
			_viewsContainer.addChild(_popupView);
			_viewsContainer.addChild(_borderView);
 
			_root.addChild(_viewsContainer);
		}
		
		
		private function initializeListeners () : void {
			_introView.addEventListener(ModelEvent.CHANGE_DATA, onDataChange, false, 0, true);
			_introView.addEventListener(ModelEvent.INPUT_ACCEPT, acceptIntroInput, false, 0, true);
			
			_roadView.addEventListener(ModelEvent.ROAD_TIMER_HIT, Events.advanceRoadTime, false, 0, true);
			_roadView.addEventListener(ModelEvent.MPG_HIT, mpgHit, false, 0, true);
			_roadView.addEventListener(ModelEvent.PULLOVER_HIT, pulloverHit, false, 0, true);
			
			_popupView.addEventListener(ModelEvent.ADVANCE_ACTIONS, advanceActions, false, 0, true);
			_popupView.addEventListener(ModelEvent.OVERLAY_CLICK, advanceActions, false, 0, true);
			_popupView.addEventListener(ModelEvent.FINISH_BARTER, finishBarter, false, 0, true);
			_popupView.addEventListener(ModelEvent.CHANGE_DATA, onDataChange, false, 0, true);
			_popupView.addEventListener(ModelEvent.CHANGE_SCENERIO, onScenerioChange, false, 0, true);
			_popupView.addEventListener(ModelEvent.HIDE_OUT, hideOut, false, 0, true);
			_popupView.addEventListener(ModelEvent.HUNT, hunt, false, 0, true);
			_popupView.addEventListener(ModelEvent.ITEM_MOD_ALTER,onItemModAlter, false, 0, true);
			
			_mapView.addEventListener(ModelEvent.DISPLAY_STATUS, displayStatus, false, 0, true);
			_mapView.addEventListener(ModelEvent.HIDE_STATUS_BAR, hideStatusBar, false, 0, true);
			_mapView.addEventListener(ModelEvent.SHOW_STATUS_BAR, showStatusBar, false, 0, true);
			_mapView.addEventListener(ModelEvent.BACK_TO_ROAD_CLICK, backToRoadClick, false, 0, true);
			
			_townView.addEventListener(ModelEvent.BARTER_POPUP, barterPopup, false, 0, true);
			_townView.addEventListener(ModelEvent.BACK_TO_ROAD_CLICK, backToRoadClick, false, 0, true);
			_townView.addEventListener(ModelEvent.TOWN_MAP_CLICK, townMapClick, false, 0, true);
			_townView.addEventListener(ModelEvent.ANNOUNCE_POPUP, announcePopup, false, 0, true);
			_townView.addEventListener(ModelEvent.STATUS_BTN_CLICK, statusBtnClick, false, 0, true);
			_townView.addEventListener(ModelEvent.HIDE_BTN_CLICK, hideBtnClick, false, 0, true);
			_townView.addEventListener(ModelEvent.COLLECT_BROCHURE, collectBrochure, false, 0, true);
			_townView.addEventListener(ModelEvent.HUNT_BTN_CLICK, huntBtnClick, false, 0, true);
			_townView.addEventListener(ModelEvent.HIDE_STATUS_BAR, hideStatusBar, false, 0, true);
			_townView.addEventListener(ModelEvent.SHOW_STATUS_BAR, showStatusBar, false, 0, true);
			
			_statusView.addEventListener(ModelEvent.STATUS_BINYAH_CLICK, statusBinyahClick, false, 0, true);
			
			_application.addEventListener(ModelEvent.PICK_AILMENT, Data.pickAilmentFromCall, false, 0, true);		
			_application.addEventListener(ModelEvent.CHANGE_SCENERIO,onScenerioChange, false, 0, true);
								

		}
		
		private function start () : void {
			_assetSwf.addEventListener(Event.INIT, continueStart, false, 0, true);
			_assetSwf.load();
		} 
		private function continueStart (p_xEvent:Event) : void {
			_assetSwf.removeEventListener(Event.INIT, continueStart);
			_application.startGame();
		}
		
		private function displayStatus(p_xEvent:ModelEvent):void{
			_application.displayStatus()
		}
		private function hideStatusBar(p_xEvent:ModelEvent):void{
			_statusView.visible = false
		}
		private function showStatusBar(p_xEvent:ModelEvent):void{
			_statusView.visible = true
		}
		private function announcePopup(p_xEvent:ModelEvent):void{
			_application.announcePopup(p_xEvent.data)
		}
		
		private function barterPopup(p_xEvent:ModelEvent):void{
			_application.barterPopup( p_xEvent.data._aGoods, p_xEvent.data._sGoodsHeader  );
			
		}  
		
		private function finishBarter(p_xEvent:ModelEvent):void{
			_application.finishBarter( );
		} 
		private function statusBtnClick(p_xEvent:ModelEvent):void{
			_application.statusBtnClick( );
		}
		private function hideBtnClick(p_xEvent:ModelEvent):void{
			_application.hideBtnClick( );
		}
		private function huntBtnClick(p_xEvent:ModelEvent):void{
			_application.huntBtnClick( );
		}
		private function townMapClick(p_xEvent:ModelEvent):void{
			_application.showMap( );
		}
		private function backToRoadClick(p_xEvent:ModelEvent):void{
			if(G.calcDistanceToDestination())
				_application.backToRoadSuccess( );
			else
				_application.backToRoadFail( );
		}
		
		private function onScenerioChange(p_xEvent:ModelEvent):void{
			var l_sNewScenerio:Scenario = G._aScenarios[p_xEvent.data._sSlug]
			_application.executeScenerio(l_sNewScenerio);
		}
		
		
		private function onItemModAlter(p_xEvent:ModelEvent):void{
			var l_xGoods:Goods = p_xEvent.data._xGoods
			var l_nCount:int = p_xEvent.data._nCount
			
			if(l_xGoods._xMods.quality){
				trace(G._nCurrentFood)
				trace(G._nCurrentFoodQuality)
				var l_nTotal:int = G._nCurrentFood * G._nCurrentFoodQuality
				trace(l_nTotal)
				l_nTotal += l_nCount * l_xGoods._xMods.quality
				trace(l_nCount)
				trace(l_xGoods._xMods.quality)
				trace(l_nTotal)
				G._nCurrentFoodQuality = l_nTotal / (l_nCount + G._nCurrentFood)
				trace(G._nCurrentFoodQuality)
			}
			if(l_xGoods._xMods.health){
				G.HEALTH += l_nCount * l_xGoods._xMods.health
			}
		}
		
		
		private function onDataChange(p_xEvent:ModelEvent) : void{
			var l_sNewCar:String = p_xEvent.data._sNewCar; 
			var l_nDollarChange:Number = p_xEvent.data._nDollarChange; 
			var l_nGasChange:int = p_xEvent.data._nGasChange;  
			var l_nFoodChange:int = p_xEvent.data._nFoodChange; 
			
			if(l_sNewCar)
				G._sCurrentCar = l_sNewCar; 
			if(l_nDollarChange)
				G._nCurrentDollars += l_nDollarChange; 
			if(l_nGasChange){
				G._nCurrentGallons += l_nGasChange;
				if(G._nCurrentGallons > G._nMaxGallons)
					G._nCurrentGallons = G._nMaxGallons
				if(G._nCurrentGallons <= 0)
					G._nCurrentGallons = 0
			}
			if(l_nFoodChange){
				G._nCurrentFood += l_nFoodChange;
			}
			
		}
		
		
		private function acceptIntroInput(p_xEvent:ModelEvent):void{
			G._sMode = G.MODE_ROAD;
			_application.endIntro();
			_application.startRoad();
		}
		
		
		//
		// HUNT & HIDE
		//
		
		private function hunt(p_xEvent:ModelEvent):void{
			var l_nDamage:int = G.huntDamage() 
			var l_nAttack:int = G.huntAttack()
			G.HEALTH = G.HEALTH - l_nDamage 
			_application.addAnnouncement('You lose '+l_nDamage+' health and do '+l_nAttack+' damage')
			
			if(0&& G.HEALTH <= 0){  
				_application.addAnnouncement('You died!');
			}else if(l_nAttack < G._xCurrentDestination._nDefense){
				_application.addAnnouncement('The city defense has weakened')
				G._xCurrentDestination._nDefense = G._xCurrentDestination._nDefense - l_nAttack
			}else{
				var l_nPlunder:int = G.huntPlunder()
				_application.addAnnouncement('Hunt Successful! You plunder '+l_nPlunder+'$!');  
				G._nCurrentDollars += l_nPlunder
				
				if(G._xCurrentDestination._bArrivialSign && !G._xCurrentDestination._bIsRuined){
					G._nCurrentCitiesDestroyed += 1
					_application.addAnnouncement('You destory the city and plunder '+G.CITY_BANK_PLUNDER+'$ more!');
					G._nCurrentDollars += G.CITY_BANK_PLUNDER
				}
				G._xCurrentDestination._bIsRuined = true
				_townView.addTownImg('destroyed.jpg')
				
				if(G.roll(2)>0){
					_application.addAnnouncement('The destruction attracted the police and someone got a look at your plates.\n\nAlert +1');
					G._nCurrentAlert += 1
				}
				
			}
			_application.popAction()
			_townView.updateSupportText()
		}
		
		private function hideOut(p_xEvent:ModelEvent):void{
			G._nCurrentDay += 1
			Events.feed()
			
			_application.displayStatus()
			
			G.reduceAlarm()
			G.heal()
			G.reduceRage()
			
			if(!_application.popAction()){
				_application.addAnnouncement("The day passes without incident.");
				_application.popAction()
			}
		}
		
        
		
		
		// ROAD STUFF
		
		private function pulloverHit(p_xEvent:ModelEvent):void{
			
			if(!G._sMode == G.MODE_ROAD)
			return;
			
			var l_aShoplist:Array = new Array();
			l_aShoplist.push( G._sCurrentRegion );
			
			var l_xTmpObject:Object = { 
				_aShoplist: l_aShoplist,
				_sDescription: G._aRegions[G._sCurrentRegion].getDescription(),
				_sVista: "region_"+G._sCurrentRegion+".jpg",
				_bNoVista: true
			};
			
			var l_sName:String = G._aPrefix[Math.floor(Math.random()*G._aPrefix.length)]
			l_sName += G._aMiddle[Math.floor(Math.random()*G._aMiddle.length)]
			l_sName += G._aSuffix[Math.floor(Math.random()*G._aSuffix.length)]
			
			var l_nRating:int = G.roll(3)
			
			var l_xTmpDestination:Destination = new Destination('', l_sName, 0, 0, G._aRegions[G._sCurrentRegion]._sType, -1, l_xTmpObject, l_nRating);
			
			_application.loadArrivial(l_xTmpDestination);
			
		}
		
		private function mpgHit(p_xEvent:ModelEvent):void{
			
			var l_nMpgChange:int = p_xEvent.data._nCount;
			var l_nMaxMpg:int = G._aCars[G._sCurrentCar]._nMaxSpeed;
			var l_nCurrentSpeed:int = G._nCurrentMPH + l_nMpgChange;
			if( l_nCurrentSpeed > l_nMaxMpg)
				l_nCurrentSpeed = l_nMaxMpg
			if(l_nCurrentSpeed <= 0){
				l_nCurrentSpeed = 0
				_application.tmpFreezeRoad();
			}
			if(G._nCurrentMPH == 0 && l_nCurrentSpeed != 0)
				_application.tmpContinueRoad();
			G._nCurrentMPH = l_nCurrentSpeed
			_application.changeSpeed();
			
		}
		
		private function collectBrochure(p_xEvent:ModelEvent):void{
			_application.executeScenerio(G._aScenarios['brochure-1']);
		}
		
		// Every step:
		// Advance x/y
		// Consume Gas
		// Check Distance, else move x/y
		public function moveCar():void{
			var l_xDest:Destination = G._aCities[G._sCurrentDestination];
			var l_nTotalDistance:Number = G.calcDistanceToDestination();
			var l_nDistanceTraveled:Number = (G._nCurrentMPH  / 6)/G.MILES_TO_PIXEL;
			var l_nRatio:Number = l_nTotalDistance/l_nDistanceTraveled;
			var l_bAtDestination:Boolean = (l_nRatio  < 1)?true:false;
			
			// Advance location
			var l_nNewX:Number =   ( l_xDest._nX - G._nX ) / l_nRatio;
			var l_nNewY:Number = ( l_xDest._nY - G._nY ) / l_nRatio;
			
			// Consume Gas
			var l_nMPG:int = G._aCars[G._sCurrentCar]._nMPG ; 
			var l_nGasConsumed:Number = l_nDistanceTraveled * G.MILES_TO_PIXEL / l_nMPG  ;
			G._nCurrentGallons -= l_nGasConsumed ;
			if(G._nCurrentGallons < 0 ){
				G._nCurrentGallons = 0
			}
			
			//Check Destination
			if(l_bAtDestination){
				G._nY = l_xDest._nY ;
				G._nX = l_xDest._nX ;
				G._nDistanceToDestination = 0;
			}
			
			// Else advance car
			else{
				G._nY = G._nY + l_nNewY ;
				G._nX = G._nX + l_nNewX ;
				G._nDistanceToDestination = (l_nTotalDistance-l_nDistanceTraveled)* G.MILES_TO_PIXEL;
			}
		}
		
		
		// Internal
		private function advanceActions(p_xEvent:ModelEvent):void{
			_application.advanceActions();
		}
		
		// When Clicking Binyah Button
		private function statusBinyahClick(p_xEvent:ModelEvent):void{
			if(G._xBinyah._bDead)
				_application.executeScenerio(G._aScenarios['binyah-300']);
			else if(Events.BINYAH_HUNGER)
				_application.executeScenerio(G._aScenarios['binyah-100']);
			else
				_application.executeScenerio(G._aScenarios['binyah-200']);
		}
		
		
		
		/*****************
		 * 
		 * 	XML
		 * 
		 ****************/

		private function loadXml():void{
			initializeDestinations();
			initializeShoplists();
			initializeShops();
			initializeChars();
			initializeQuotes();
			initializeCars(); 
			initializeRegions();
			initializeScenarios();
		}
		
		
		
		private function initializeCars() : void{
			var _loader:URLLoader = new URLLoader(new URLRequest('swf/xml/cars.xml?v='+_nRand));
			_loader.addEventListener(Event.COMPLETE, loadCars);
		} 
		private function loadCars(e:Event):void{
			e.target.removeEventListener(Event.COMPLETE, loadCars);
			var l_xml:XML = XML(e.target.data);
			
			var l_aTmpArray:Array = new Array();
			
			for each(var xml_car:XML in l_xml.car){
				var l_xTmpCar:Car = new Car(xml_car.slug,xml_car.name,xml_car.mpg,xml_car.maxspeed,xml_car.gals, xml_car.stress, xml_car.desc,xml_car.cost ); 
				l_aTmpArray[xml_car.slug] = l_xTmpCar;
			}
			G._aCars = l_aTmpArray;
		}
		
		
		
		
		private function initializeQuotes() : void{
			var _loader:URLLoader = new URLLoader(new URLRequest('swf/xml/quotes.xml?v='+_nRand));
			_loader.addEventListener(Event.COMPLETE, loadQuotes);
		} 
		private function loadQuotes(e:Event):void{
			e.target.removeEventListener(Event.COMPLETE, loadQuotes);
			var l_xml:XML = XML(e.target.data);
			
			var l_aTmpArray:Array = new Array();
			
			for each(var xml_quote:XML in l_xml.quote){
				
				var l_xTmpQuote:Quote = new Quote(xml_quote.slug); 
				
				for each(var xml_text:XML in xml_quote.text){
					l_xTmpQuote.addText( xml_text );
				}
				
				l_aTmpArray[xml_quote.slug] = l_xTmpQuote;
				
			}
			G._aQuotes = l_aTmpArray;
		}
		
		
		
		
		private function initializeChars() : void{
			var _loader:URLLoader = new URLLoader(new URLRequest('swf/xml/chars.xml?v='+_nRand));
			_loader.addEventListener(Event.COMPLETE, loadChars);
		} 
		private function loadChars(e:Event):void{
			e.target.removeEventListener(Event.COMPLETE, loadChars);
			var l_xml:XML = XML(e.target.data);
			
			var l_aTmpArray:Array = new Array();
			
			for each(var xml_char:XML in l_xml.char){
				
				var l_xTmpChar:Character = new Character(xml_char.slug);
				
				for each(var xml_instance:XML in xml_char.instance){
					l_xTmpChar.addInstance( xml_instance.name, xml_instance.image  );
				} 
				
				l_aTmpArray[xml_char.slug] = l_xTmpChar;
				
			}
			G._aChars = l_aTmpArray;
		}
		
		private function initializeShops() : void{
			var _loader:URLLoader = new URLLoader(new URLRequest('swf/xml/shops.xml?v='+_nRand));
			_loader.addEventListener(Event.COMPLETE, loadShops);
		} 
		
		private function loadShops(e:Event):void{
			
			e.target.removeEventListener(Event.COMPLETE, loadShops);
			var l_xml:XML = XML(e.target.data);
			
			var l_aTmpArray:Array = new Array();
			var l_xMods:Object
			for each(var xml_shop:XML in l_xml.shop){
				var l_xTmpShop:Shop = new Shop(xml_shop.name,xml_shop.image,xml_shop.slug,xml_shop.type,xml_shop.goodsheader);
				if(xml_shop.type=='brochure')
						G._nTotalBrochures++; 
				for each(var xml_char:XML in xml_shop.char){
					l_xTmpShop.addChar(xml_char,xml_char.attribute('chance'),xml_char.attribute('quotes'));
				}
				
				for each(var xg:XML in xml_shop.goods){
					l_xMods = new Object()
					if (xg.health){
						l_xMods.health = xg.health
					}
					if (xg.quality){
						l_xMods.quality = xg.quality
					}
					l_xTmpShop.addGoods( new Goods(xg.itemtype, xg.name, xg.cost, xg.slug, xg.count, l_xMods));
				}
				l_aTmpArray[xml_shop.slug] = l_xTmpShop;
				
			}
			G._aShops = l_aTmpArray;
		}
		
		
		
		
		private function initializeShoplists() : void{
			var _loader:URLLoader = new URLLoader(new URLRequest('swf/xml/shoplists.xml?v='+_nRand));
			_loader.addEventListener(Event.COMPLETE, loadShoplists);
		}
		private function loadShoplists(e:Event):void{
			e.target.removeEventListener(Event.COMPLETE, loadShoplists);
			var l_xml:XML = XML(e.target.data);
			
			var l_aTmpArray:Array = new Array();
			
			for each(var xml_shoplist:XML in l_xml.shoplist){
				
				var l_xTmpShoplist:Shoplist = new Shoplist(xml_shoplist.attribute('slug'));
				
				for each(var xml_shop:XML in xml_shoplist.shop){
					l_xTmpShoplist.addShopData(xml_shop,xml_shop.attribute('chance'),xml_shop.attribute('quoteoverride'));
				}
				
				l_aTmpArray[xml_shoplist.attribute('slug')] = l_xTmpShoplist;
				
			}
			G._aShoplists = l_aTmpArray; 
		 
		}
		
		
		private function initializeRegions() : void{
			var _loader:URLLoader = new URLLoader(new URLRequest('swf/xml/regions.xml?v='+_nRand));
			_loader.addEventListener(Event.COMPLETE, loadRegions);
		}
		private function loadRegions(e:Event):void{
			e.target.removeEventListener(Event.COMPLETE, loadRegions);
			var l_xml:XML = XML(e.target.data);
			
			var l_aTmpArray:Array = new Array();
			
			for each(var xml_region:XML in l_xml.region){
				
				var l_aDescription:Array = new Array();
				
				for each(var xml_description:XML in xml_region.description){
					l_aDescription.push( xml_description );
				}
				
				var l_xTmpRegion:Region = new Region(xml_region.slug,xml_region.name,xml_region.bg,xml_region.type,l_aDescription);
				l_aTmpArray[xml_region.slug] = l_xTmpRegion;
			}
			G._aRegions = l_aTmpArray;
		 
		}
		
		
		
		private function initializeDestinations() : void{
			var _loader:URLLoader = new URLLoader(new URLRequest('swf/xml/destinations.xml?v='+_nRand));
			_loader.addEventListener(Event.COMPLETE, loadDestinations);
		}
		
		private function loadDestinations(e:Event):void{
			e.target.removeEventListener(Event.COMPLETE, loadDestinations);
			var l_xml:XML = XML(e.target.data);
			
			var l_aTmpArray:Array = new Array();
			
			for each(var xml_city:XML in l_xml.city){
				
				var l_aShoplist:Array = new Array();
				
				for each(var xml_shoplist:XML in xml_city.shoplist.shops){
					l_aShoplist.push( xml_shoplist );
				}
				
				var l_aRevealed:Array = new Array()
				
				for each(var xml_destination:XML in xml_city.revealed){
					
					l_aRevealed.push( {_sSlug:String(xml_destination),_bKnown: int(xml_destination.attribute('known'))} );
				}
				
				var l_sSlugString:String = xml_city.slug ;
				
				var l_nSimeonIndex:int = G.SIMEON_CITIES.indexOf( l_sSlugString);
				
				var l_xTmpObject:Object = { 
					_sVista: xml_city.vista,
					_aShoplist: l_aShoplist,
					_bArrivialSign : xml_city.attribute('arrivialsign'),
					_sDescription: xml_city.description,
					_aRevealed : l_aRevealed,
					_sShortName : xml_city.shortname
				};
				
				l_aTmpArray[xml_city.slug] = new Destination(xml_city.slug, xml_city.name, xml_city.x, xml_city.y, xml_city.type, l_nSimeonIndex, l_xTmpObject, xml_city.rating );
				
				G._nTotalCities++;
				
			}
			
			G._aCities = l_aTmpArray;
			
			G._aCities[G.START_DESTINATION]._bOnMap = true;
			G._aCities[G.START_DESTINATION]._bNameKnown = true;
			
		}
		
		
		
		private function initializeScenarios() : void{
			var _loader:URLLoader = new URLLoader(new URLRequest('swf/xml/scenarios.xml?v='+_nRand));
			_loader.addEventListener(Event.COMPLETE, loadScenarios);
		}
		private function loadScenarios(e:Event):void{
			e.target.removeEventListener(Event.COMPLETE, loadScenarios);
			var l_xml:XML = XML(e.target.data);
			
			var l_nIndex:int;  
			var l_nChance:int;
			var l_sType:String;
			var l_sResultType:String
			var l_sImg:String
			var l_bAilment:Boolean
			var l_nAilmentCount:int = 0
			
			G._aCoordsScenarios = new Array();
			G._aGoodIndexScenario = new Array();
			G._aBinyahRoadIndexScenario = new Array();  
			G._aCharIndexScenario = new Array();  
			G._aStressIndexScenario = new Array();
			G._aRoadStartIndexScenario = new Array();
			G._aRoadStartBinyahIndexScenario = new Array();
			G._aEtcIndexScenario = new Array();
			G._aScenarios = new Array();
			var xml_data:XML
			var l_aResultData:Array = null;
			var l_aResultDataOrig:Array = null;
			var l_aResultBranch:Array = null
			var l_aResultOptions:Array = null;
			var l_xDataPiece:Object = null;
			var l_xDataPieceOrig:Object = null
			var l_sAilmentType:String = null;
			var i:int
			
			for each(var xml_scenario:XML in l_xml.scenario){
				l_bAilment = (xml_scenario.attribute('ailment') == 1)
				l_sType = String(xml_scenario.attribute('type'))
				l_sAilmentType = String(xml_scenario.attribute('ailmenttype'))  
				l_nIndex = int(xml_scenario.attribute('id')) ;
				l_nChance = int(xml_scenario.attribute('chance'))
				l_sResultType = String(xml_scenario.result.attribute('type'))
				l_sImg = String(xml_scenario.result.attribute('img'))
				
				
				// construct scenerio
				var l_xTmpScenario:Scenario = new Scenario(l_sType+'-'+l_nIndex, l_sType);
				l_xTmpScenario._sResultType = l_sResultType
				l_xTmpScenario._sImg= l_sImg
				
				// create scenerio result
				l_aResultData = new Array();
				l_aResultDataOrig = new Array();
				l_aResultOptions = new Array();
				l_aResultBranch = new Array();
				
				// grab <speak> 
				l_xTmpScenario._sResultSpeak =  String(xml_scenario.result.speak)
				
				// grab follow
				l_xTmpScenario._sSpeakFollow =  String(xml_scenario.result.attribute('follow'))
				
				// grab branchon
				l_xTmpScenario._sBranchOn =  String(xml_scenario.result.attribute('branchon'))
				
				// grab <chance>
				for each(xml_data in xml_scenario.result.chance){
					l_xDataPiece = new Object();
					l_xDataPiece._sFactor = String(xml_data.attribute('factor'))
					l_xDataPiece._sFollow = String(xml_data.attribute('follow'))
					l_aResultBranch.push( l_xDataPiece );
				}
				l_xTmpScenario._aResultBranch = l_aResultBranch;
				
				// grab <data>
				for each(xml_data in xml_scenario.result.data){
					l_xDataPiece = new Object();
					l_xDataPiece._sUnit = String(xml_data.attribute('unit'))
					l_xDataPiece._sChange = String(xml_data.attribute('change'))
					l_xDataPiece._sKey = String(xml_data.attribute('key'))
					l_xDataPiece._sVar= String(xml_data)
					l_aResultData.push( l_xDataPiece );
					
					
					l_xDataPieceOrig = new Object();
					l_xDataPieceOrig._sUnit = String(xml_data.attribute('unit'))
					l_xDataPieceOrig._sChange = String(xml_data.attribute('change'))
					l_xDataPieceOrig._sKey = String(xml_data.attribute('key'))
					l_xDataPieceOrig._sVar= String(xml_data)
					l_aResultDataOrig.push( l_xDataPieceOrig );
					
				}
				l_xTmpScenario._aResultData = l_aResultData;
				l_xTmpScenario._aResultDataOrig = l_aResultDataOrig;
				
				// grab <option>
				for each(xml_data in xml_scenario.result.option){
					l_xDataPiece = new Object();
					l_xDataPiece._sResult = String(xml_data.attribute('result'))
					l_xDataPiece._sCheck = String(xml_data.attribute('check'))
					l_xDataPiece._nMin = Number(xml_data.attribute('min'))
					l_xDataPiece._sIs = String(xml_data.attribute('is'))
					l_xDataPiece._sText= String(xml_data)
					l_aResultOptions.push( l_xDataPiece ); 
				}
				l_xTmpScenario._aResultOptions = l_aResultOptions;
				
				
				// distribute scenerios to arrays
				switch(l_sType){
					
					case 'coords':
						l_xTmpScenario._nXmin = xml_scenario.attribute('xmin')
						l_xTmpScenario._nYmin = xml_scenario.attribute('ymin')
						l_xTmpScenario._nXmax = xml_scenario.attribute('xmax')
						l_xTmpScenario._nYmax = xml_scenario.attribute('ymax')
						l_xTmpScenario._sRegion = xml_scenario.attribute('region')
						if(l_xTmpScenario._sRegion)
							G._aCoordsScenarios[l_nIndex] = l_xTmpScenario
						G._aScenarios['coords-'+l_nIndex] = l_xTmpScenario
					break;
					
					case 'good':
						for(i = 0; i<l_nChance; i++)
							G._aGoodIndexScenario.push(l_nIndex); 
						G._aScenarios['good-'+l_nIndex] = l_xTmpScenario
					break;
					
					case 'binyahroad':
						for(i = 0; i<l_nChance; i++)
							G._aBinyahRoadIndexScenario.push(l_nIndex); 
						G._aScenarios['binyahroad-'+l_nIndex] = l_xTmpScenario
					break;
					
					case 'char':
						for(i = 0; i<l_nChance; i++)
							G._aCharIndexScenario.push(l_nIndex); 
						G._aScenarios['char-'+l_nIndex] = l_xTmpScenario
					break;
					
					case 'etc':
						for(i = 0; i<l_nChance; i++)
							G._aEtcIndexScenario.push(l_nIndex); 
						G._aScenarios['etc-'+l_nIndex] = l_xTmpScenario
					break;
					
					case 'speed':
						G._aScenarios['speed-'+l_nIndex] = l_xTmpScenario
					break;
					
					case 'binyah':
						G._aScenarios['binyah-'+l_nIndex] = l_xTmpScenario
					break;
					case 'brochure':
						G._aScenarios['brochure-'+l_nIndex] = l_xTmpScenario
					break;
					case 'stress':
						G._aScenarios['stress-'+l_nIndex] = l_xTmpScenario
						for(i = 0; i<l_nChance; i++)
							G._aStressIndexScenario.push(l_nIndex); 
					break;
					case 'roadstartbinyah':
						G._aScenarios['roadstartbinyah-'+l_nIndex] = l_xTmpScenario
						for(i = 0; i<l_nChance; i++)
							G._aRoadStartBinyahIndexScenario.push(l_nIndex); 
					break;
					case 'roadstart':
						G._aScenarios['roadstart-'+l_nIndex] = l_xTmpScenario
						for(i = 0; i<l_nChance; i++)
							G._aRoadStartIndexScenario.push(l_nIndex); 
					break;
					case 'instruction':
						G._aScenarios['instruction-'+l_nIndex] = l_xTmpScenario
					break;
					
				}
				
				// Ailments
				if(l_bAilment){
					trace("l_sAilmentType1",l_sAilmentType)
					l_xTmpScenario._bIsAilment = true
					if(!G._aAilments[l_sAilmentType])G._aAilments[l_sAilmentType]=new Array()
					G._aAilments[l_sAilmentType][G._aAilments[l_sAilmentType].length] = l_xTmpScenario
				}
				
				
			}
		 
		}
		
		


//--------------------------------------------------------------------------------------------------
//	SINGLETON
		private static const _instance:Controller = new Controller(SingletonEnforcer);
		public static function get instance () : Controller {
			return _instance;
		}
		public function Controller (p_xEnforcer:Class) {
			if (p_xEnforcer != SingletonEnforcer) {
				throw new Error("singleton");
			}
		}
	}
}
class SingletonEnforcer {}