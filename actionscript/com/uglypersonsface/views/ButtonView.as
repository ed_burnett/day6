package com.uglypersonsface.views {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.models.application.Application;
	
	import flash.events.MouseEvent;

	public class ButtonView extends View {
		
		private var e_callback:ModelEvent;
		private var _sButtonType:String;
		private var _bDead:Boolean;
		private var _fOnFunction:Function
		private var _fOutFunction:Function
		private var _bHover:Boolean 
		

		public function ButtonView (p_xModel:Application, p_xAssetSwf:AssetSwf, p_sButtonFrame:String,p_bDead:Boolean = false,p_fOn:Function = null, p_fOut:Function = null, p_bHover:Boolean=false) {
			_sButtonType = p_sButtonFrame;
			_bDead=p_bDead
			_fOnFunction = p_fOn
			_fOutFunction = p_fOut
			_bHover = p_bHover
			super(p_xModel, p_xAssetSwf, "mc_Button");
		}
		
		public function setTitle(p_sS:String):void{ 
			_view.txt_title.text = p_sS  
		}

		
		public function setOptionTxt(p_sS:String):void{ 
			_view.txt_option_txt.text = p_sS  
		}
		
		public function setPriceTxt(p_sS:String):void{
			p_sS = (p_sS)? p_sS:""
			_view.txt_price_txt.text = p_sS
			
		}

//--------------------------------------------------------------------------------------------------
//	HOOK

		override protected function buildViewHook () : void {
			_view.stop();
			_view.gotoAndStop('f_'+_sButtonType+((_bDead)?'_dead':''));
			if(!_bDead){
				if(_bHover){
					_view.gotoAndStop('f_'+_sButtonType+'_hover');
				}
				setHover();
				_view.buttonMode = true;
			}
		}


//--------------------------------------------------------------------------------------------------
//	LISTENERS
		
		private function setHover():void{
			
			addEventListener(MouseEvent.ROLL_OVER,rollOver);
			addEventListener(MouseEvent.ROLL_OUT,rollOut);
			
		}
		
		

//--------------------------------------------------------------------------------------------------
//	HELPERS
		
		public function setCallback(p_e:ModelEvent):void{
			e_callback = p_e;
			_view.addEventListener(MouseEvent.CLICK,dispatchCallback) ;
		}
		
		private function dispatchCallback(p_eEvent:MouseEvent):void{
			dispatchEvent(e_callback);
		}
		
		private function rollOver(e:MouseEvent):void{
			_view.gotoAndStop('f_'+_sButtonType+'_hover');
			if (_fOnFunction != null)
				_fOnFunction(e_callback)
		}
		private function rollOut(e:MouseEvent):void{
			if(!_bHover){
				_view.gotoAndStop('f_'+_sButtonType);
			}
			if (_fOutFunction != null)
				_fOutFunction(e_callback)
		}
		
		
	}
}