package com.uglypersonsface.views {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.globals.Events;
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.objects.*;
	
	import flash.display.Graphics;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.text.*;
	
	public class TownView extends View {
		private var _nCharOptionsCount:int = 0;
		private var _aShops:Array;
		
		private var _xLoader:Loader = new Loader();
		
		private var _sSimeonText:String = null;
		private var _sCharName:String = null;
		
		private var _xTextFormat:TextFormat = new TextFormat();
		
		private var _xCharOptionsFormat:TextFormat = new TextFormat();
		
		private var _xShopLoader:Loader = new Loader();
		private var _xCharLoader:Loader = new Loader();
		
		private var _xDestination:Destination = null;
		
		
		private var txt_CharName:TextField = new TextField();
		private var _xCharNameFormat:TextFormat = new TextFormat();
		
		private var txt_CharQuote:TextField = new TextField();
		private var _xCharQuoteFormat:TextFormat = new TextFormat();
		
		private var _bDragging:Boolean = false; 
		
		private var rec_bounds:Rectangle
		
		public function TownView (p_xModel:Application, p_xAssetSwf:AssetSwf) {
			super(p_xModel, p_xAssetSwf, "mc_TownView");
		}
		
		
		private var _xCharStyle:StyleSheet = new StyleSheet;
		
		private var l_nMapX:int = 65
		private var l_nMapY:int = 230 
		
		private var l_nHideX:int = 75   
		private var l_nHideY:int = 410
		
		private var l_nHuntX:int = 550 
		private var l_nHuntY:int = 260 
		 
		private var l_nStatusX:int = 650
		private var l_nStatusY:int = 400
		 
		private var l_nRoadX:int = 20 
		private var l_nRoadY:int = 100 
		
		private var l_nExploreX:int = 610
		private var l_nExploreY:int = 80
 
//--------------------------------------------------------------------------------------------------
//	HOOK

		override protected function buildViewHook () : void {
	//		_view.stop();
			 
			hideClips(); 
			stopClips(); 
			
			
			_model.addEventListener(ModelEvent.INIT_TOWN, initTown, false, 0, true);
			_model.addEventListener(ModelEvent.START_ROAD_VIEW, endTown, false, 0, true);
			
			_view.mc_scrollup.addEventListener(MouseEvent.MOUSE_DOWN, upScroll);
			_view.mc_scrolldown.addEventListener(MouseEvent.MOUSE_DOWN, downScroll);
			
			_view.mc_gps.mc_x.addEventListener(MouseEvent.MOUSE_DOWN, removeGps);
			
			
			_view.mc_shoptext.txt_shoptext.width = G.TOTAL_WIDTH;
			_view.mc_shoptext.txt_shoptext.type = TextFieldType.DYNAMIC
			_view.mc_shoptext.txt_shoptext.autoSize = TextFieldAutoSize.LEFT;
			_view.mc_shoptext.txt_shoptext.background = true;
			_view.mc_shoptext.txt_shoptext.backgroundColor = 0x000000; 
			_view.mc_shoptext.txt_shoptext.x = 10;
			_view.mc_shoptext.txt_shoptext.y = 10;
			
			_xCharNameFormat.align = TextFormatAlign.RIGHT;
			_xCharNameFormat.font = 'Arial';
			_xCharNameFormat.size = 25;
			_xCharNameFormat.color = 0xFFFFFF;
			_xCharNameFormat.leftMargin = 8;
			_xCharNameFormat.rightMargin = 8;
			
			txt_CharName.width = G.TOTAL_WIDTH-10;
			txt_CharName.x = 0;
			txt_CharName.y = G.TOTAL_HEIGHT - G.STATUS_HEIGHT - 45;
			txt_CharName.type = TextFieldType.DYNAMIC
			txt_CharName.defaultTextFormat = _xCharNameFormat;
			txt_CharName.autoSize = TextFieldAutoSize.RIGHT;
			txt_CharName.background = true;
			txt_CharName.backgroundColor = 0x000000;
			
			
			
			var t:Object = new Object();
			t.fontSize = 19;
			
			var p:Object = new Object();
			p.lineHeight = 10;
			
			_xCharStyle.setStyle(".name", t);
			_xCharStyle.setStyle(".char", p);
			
			_view.mc_charquote.txt_char.styleSheet = _xCharStyle 

			_view.mc_charquote.txt_char.width = G.TOTAL_WIDTH - 400; 
			_view.mc_charquote.txt_char.x = 10;
			_view.mc_charquote.txt_char.y = 0;  
			_view.mc_charquote.txt_char.background = true;
			_view.mc_charquote.txt_char.backgroundColor = 0x000000;
			_view.mc_charquote.txt_char.wordWrap = true;
			
			_view.txt_TownTitle.width = G.TOTAL_WIDTH;
			_view.txt_TownTitle.x = 0;
			_view.txt_TownTitle.y = 10; 
			_view.txt_TownTitle.type = TextFieldType.DYNAMIC
			_view.txt_TownTitle.autoSize = TextFieldAutoSize.CENTER;
			_view.txt_TownTitle.selectable = false;
			
			_view.txt_description.x = 190;
			_view.txt_description.y = 360;
			_view.txt_description.width = 380;
			_view.txt_description.height = 150;
			
			_view.mc_scrollup.x = 563
			_view.mc_scrollup.y = 341
			
			_view.mc_scrolldown.x = 576
			_view.mc_scrolldown.y = 528
			
			
			_view.txt_description.wordWrap =true;
			_view.txt_description.multiline =true;
			
			_view.mc_chartext.addChild(txt_CharName);
			
			_view.mc_grid.x = 480; 
			_view.mc_grid.y = 300;
			
			_view.mc_shopbuttons.x = 0;
			_view.mc_shopbuttons.y = G.TOTAL_HEIGHT;
			
			//addBg(0x003300);  
			addShopBg(0x000000); 
			
		} 
 

//--------------------------------------------------------------------------------------------------
//	LISTENERS
		
		
		public function addTownImg(p_sText:String):void{
			trace('addTownImg',p_sText)
			_xLoader.load(new URLRequest( 'swf/image/'+p_sText ));
			_xLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadTownImg, false,0,true);
			
		}
		
		private function loadTownImg(p_xEvent:Event):void{
			_view.mc_vistaborder.graphics.clear()
			clearMovieClip(_view.mc_vistaholder);
			_view.mc_vistaholder.visible = true;
			_view.txt_description.visible = true;
			_view.mc_vistaholder.addChild(p_xEvent.target.content);
			p_xEvent.target.removeEventListener(Event.COMPLETE, loadTownImg);
			 
			var l_nMaxWidth:int = 300
			var l_nMaxHeight:int = 240
			
			var l_nWidth:int = _view.mc_vistaholder.width;
			var l_nHeight:int = _view.mc_vistaholder.height;
			
			var l_nRatio:Number = l_nWidth / l_nHeight;
			
			var l_nMaxRatio:Number = l_nMaxWidth / l_nMaxHeight
			
			var l_nNewHeight:int
			var l_nNewWidth:int
			
			
			if(l_nRatio==l_nMaxRatio){
				l_nNewHeight = l_nMaxHeight
				l_nNewWidth = l_nMaxWidth
			}if(l_nRatio > l_nMaxRatio){
				l_nNewWidth = l_nMaxWidth
				l_nNewHeight = l_nNewWidth/l_nRatio
				
			}else{
				l_nNewHeight = l_nMaxHeight 
				l_nNewWidth = l_nNewHeight*l_nRatio
				
			}
			_view.mc_vistaholder.width = l_nNewWidth
			_view.mc_vistaholder.height = l_nNewHeight
			
			_view.mc_vistaholder.x = (G.TOTAL_WIDTH - _view.mc_vistaholder.width)/2 ;
			_view.mc_vistaholder.y = 100 ;
			
			_view.mc_vistaborder.x = _view.mc_vistaholder.x
			_view.mc_vistaborder.y = _view.mc_vistaholder.y
			
			_view.mc_vistaborder.graphics.lineStyle( 1, 0x000000 );
			_view.mc_vistaborder.graphics.drawRect( -1, -1, _view.mc_vistaholder.width+1, _view.mc_vistaholder.height+1 );
			_view.mc_vistaborder.graphics.endFill();
			
			//trace('vistaborder',_view.mc_vistaborder.width, _view.mc_vistaborder.height)
		}
		
		private function initTown(p_xEvent:ModelEvent):void{
			G._sMode=G.MODE_DESTINATION
			_xDestination = p_xEvent.data._xDestination;
			G._xCurrentDestination = _xDestination
			_xDestination._bNameKnown = true
			_view.txt_TownTitle.text = _xDestination._sName.toUpperCase();
			showClips();  
			
			initShops();
			showShops();
			
			trace('support',_xDestination._nType)
			
			if(_xDestination._sVista)
				addTownImg(_xDestination._sVista)
			
			makeBackToRoadBtn();
			makeMapBtn();
			makeStatusBtn();
			makeHuntBtn();
			makeHideBtn();
			makeExploreBtn();
			supportText();
			
			var l_bShowNewPopup:Boolean=updateDestinations(_xDestination._aRevealed)
			

			if(l_bShowNewPopup){
				var l_sOutput:String = "You have arrived at "+_xDestination._sName+"<br>"
				for(var i:int=0; i<_xDestination._aRevealed.length;i++){
					if(G._aCities[_xDestination._aRevealed[i]._sSlug]._bNameKnown)
						l_sOutput += '<br>+'+G._aCities[_xDestination._aRevealed[i]._sSlug]._sName;
					else
						l_sOutput += '<br>+???';
				}
				dispatchEvent(new ModelEvent(ModelEvent.ANNOUNCE_POPUP,{_sText:l_sOutput} ));
			}
			
			if(!G._bTownInstrctions){
				G._application.executeScenerio(G._aScenarios['instruction-2']);
				G._bTownInstrctions = true
			}
		}
		
		
		
		
		private function endTown(p_xEvent:ModelEvent):void{
			hideClips();
			emptyClips();
			_view.mc_vistaborder.graphics.clear()
			clearMovieClip(_view.mc_vistaholder);
		}
		
		private function backToRoadClick(p_xEvent:ModelEvent):void{
			dispatchEvent(new ModelEvent(ModelEvent.BACK_TO_ROAD_CLICK ));
		}
		private function townMapClick(p_xEvent:ModelEvent):void{
			dispatchEvent(new ModelEvent(ModelEvent.TOWN_MAP_CLICK ));
		}
		private function statusBtnClick(p_xEvent:ModelEvent):void{
			dispatchEvent(new ModelEvent(ModelEvent.STATUS_BTN_CLICK))
		}
		private function huntBtnClick(p_xEvent:ModelEvent):void{
			dispatchEvent(new ModelEvent(ModelEvent.HUNT_BTN_CLICK))
		}
		private function hideBtnClick(p_xEvent:ModelEvent):void{
			dispatchEvent(new ModelEvent(ModelEvent.HIDE_BTN_CLICK))
		}
		
//--------------------------------------------------------------------------------------------------
//	HELPERS
		
		private function initShops():void{ 
			if (_xDestination._aCachedShops){
				_aShops = _xDestination._aCachedShops
				return;
			}
			var l_aTmpShopArray:Array = new Array();
			
			for each(var l_sShoplistSlug:String in _xDestination._aShoplist){
				 var l_sShopList:Shoplist = G._aShoplists[l_sShoplistSlug];
				 
				 var l_aShopArray:Array = l_sShopList.getShopData();
				 
				 for each(var l_sShop:String in l_aShopArray){
					 l_aTmpShopArray.push( l_sShop  );
					 if(l_aTmpShopArray.length == G.MAX_SHOPS_TOWN)  break;
				}
			}
			
			_aShops = new Array();
			
			var l_xShop:Shop
			while (l_aTmpShopArray.length > 0){
				l_xShop = G._aShops[l_aTmpShopArray.splice(0,1)]
				l_xShop = l_xShop.clone()
				_aShops.push( l_xShop );  
			}
		}
		
		private function makeBackToRoadBtn():void{
			
			var l_funCallback:Function = backToRoadClick;
			
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, 'backtoroad');
			
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,{});
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
			l_xButton.setCallback(l_e);
			
			
			l_xButton.y= l_nRoadY;
			l_xButton.x= l_nRoadX;
			
			_view.mc_backtoroad.addChild(l_xButton);
			_view.mc_backtoroad.visible = true;
		}
		
		public function updateSupportText():void{
			if(_xDestination._bIsRuined || _xDestination._nType == 5){
				_view.txt_description.htmlText = "<font size='14'>"+_xDestination.condition()+_xDestination.type()+"</font>\n\n"+  
			 _xDestination._sDescription;
			}else{
				_view.txt_description.htmlText = "<font size='14'>"+_xDestination.condition()+_xDestination.type()+"</font>\n<font size='14'>"+_xDestination.defense()+" Defense</font>\n\n"+  
			 _xDestination._sDescription;
			}
			
		}
		
		private function supportText():void{
			updateSupportText();  
			_view.mc_scrolldown.buttonMode = true
			_view.mc_scrollup.buttonMode = true
			if(_view.txt_description.maxScrollV>1){
				_view.mc_scrolldown.visible = true
				_view.mc_scrollup.visible = true
			}else{
				_view.mc_scrolldown.visible = false
				_view.mc_scrollup.visible = false
			}
			rec_bounds = new Rectangle(_view.mc_scroll.x, _view.mc_scroll.y, 0,50);
		}
		
		
		private function upScroll(event:MouseEvent):void
		{
			_view.txt_description.scrollV -= 2;
			textScrolled()
		}
		private function downScroll(event:MouseEvent):void
		{
			_view.txt_description.scrollV += 2;
			textScrolled()
		}
		
		private function textScrolled():void
		{
			_view.mc_scroll.y = rec_bounds.y + ((_view.txt_description.scrollV-1) * 50/(_view.txt_description.maxScrollV));
		}
		
		private function makeMapBtn():void{ 
			
			var l_funCallback:Function = townMapClick;
			
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, 'townmap');
			
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,{});
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
			l_xButton.setCallback(l_e);
			
			l_xButton.y= l_nMapY;
			l_xButton.x= l_nMapX;
			
			_view.mc_map.addChild(l_xButton); 
			_view.mc_map.visible = true;
			
		}
		
		private function makeExploreBtn():void{
			
			var l_funCallback:Function = exploreBtnClick
			
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, 'explore');
			
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,{});
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
			l_xButton.setCallback(l_e);
			
			l_xButton.y= l_nExploreY;
			l_xButton.x= l_nExploreX;
			
			_view.mc_map.addChild(l_xButton); 
			
		}
		private function makeHideBtn():void{
			
			var l_funCallback:Function = hideBtnClick
			
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, 'hideout');
			
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,{});
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
			l_xButton.setCallback(l_e);
			
			l_xButton.y= l_nHideY;
			l_xButton.x= l_nHideX;
			
			_view.mc_map.addChild(l_xButton); 
			
		}
		private function makeHuntBtn():void{
			
			var l_funCallback:Function = huntBtnClick
			
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, 'huntbtn');
			
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,{});
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
			l_xButton.setCallback(l_e);
			
			l_xButton.y= l_nHuntY;
			l_xButton.x= l_nHuntX;
			
			_view.mc_map.addChild(l_xButton); 
			
		}
		private function makeStatusBtn():void{
			
			var l_funCallback:Function = statusBtnClick
			
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, 'statusbtn');
			
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,{});
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
			l_xButton.setCallback(l_e);
			
			
			l_xButton.y= l_nStatusY;
			l_xButton.x= l_nStatusX;
			
			_view.mc_map.addChild(l_xButton); 
			
		}
		
		private function getSimeonQuotes(p_nCount:int):String{
			
			var l_nSimeonIndex:int = _xDestination._nSimeonIndex;
			
			if( l_nSimeonIndex == -1 ){
				return 'nosimeon';
			}else{
				return 'clue_'+G.SIMEON_CITIES[l_nSimeonIndex+1]+'_'+p_nCount;
			}
			
		}
		
		private function showShops():void{
			clearMovieClip(_view.mc_gps.mc_grid);
			var mc_shopholder:MovieClip = new MovieClip;
			var l_nCount:int = 0;
			
			for each( var l_xShop:Shop in _aShops ){
				
				var l_funCallback:Function = clickShop
				var l_sBtnType:String = ''; 
				/* 
				switch(l_xShop._sType){
					case 'food': l_sBtnType='shop_food';  break; 
					case 'brochure': l_sBtnType='shop_brochure'; break;  
					case 'landmark': l_sBtnType='shop_landmark'; break;  
					default: l_sBtnType='shop_generic'; break;  
				}  
				*/
				l_sBtnType = 'shop_btn';  
				
				var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, l_sBtnType); 
				
				var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,{_nCount:l_nCount});
				l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
				l_xButton.setCallback(l_e);
				l_xButton.setTitle(l_xShop._sName.toUpperCase())  
				
				l_xButton.y= l_nCount*45 
				l_xButton.x= -30
				l_xButton.buttonMode = true
				mc_shopholder.addChild( l_xButton );
				
				l_nCount++;
			}
			 
			_view.mc_gps.mc_grid.addChild( mc_shopholder );
			
			
		}
		
		private function exploreBtnClick(p_xEvent:ModelEvent):void{
			//_view.mc_grid.visible = true;
			_view.mc_gps.visible = true;
		} 
		
		private function removeGps(e:Event):void{
			_view.mc_gps.visible=false;
		}
		
		private function clickShop(p_xEvent:ModelEvent):void{
			
			var l_nCount:int = p_xEvent.data._nCount
			var l_xShopClicked:Shop = _aShops[l_nCount];
			
			var l_sSimeonQuote:String = getSimeonQuotes(l_nCount);
			initShop(l_xShopClicked,l_sSimeonQuote);
			removeGps(null)
		}
		
		private function initShop(p_xShop:Shop, p_sSimeonQuotes:String):void{
			_view.mc_shopbg.visible = true;
			
			addShopImg(p_xShop._sImg);
			addShopText(p_xShop._sName);
			
			addTotalChar(p_xShop);
			var l_xSimeonQuotes:Quote = G._aQuotes[p_sSimeonQuotes];
			_sSimeonText =  l_xSimeonQuotes.chooseText();
			
			addShopButtons(p_xShop);
			
			if(p_xShop._bHasBrochure){
				p_xShop._bHasBrochure = false
				G._nCurrentBrochures++;
				dispatchEvent(new ModelEvent(ModelEvent.COLLECT_BROCHURE));
			} 
			dispatchEvent(new ModelEvent(ModelEvent.HIDE_STATUS_BAR));
			
			if(!G._bShopInstrctions){
				G._application.executeScenerio(G._aScenarios['instruction-3']);
				G._bShopInstrctions = true
			}
		}
		
		private function clickBackToTown(p_xEvent:ModelEvent):void{
			
			
			_view.mc_shopbg.visible = false;
			clearMovieClip(_view.mc_shopbuttons);
			clearMovieClip(_view.mc_shopholder);
			_view.mc_shoptext.visible = false;
			_view.mc_charholder.visible = false;
			_view.mc_chartext.visible = false;
			_view.mc_charquote.visible = false;
			
			dispatchEvent(new ModelEvent(ModelEvent.SHOW_STATUS_BAR));
		}
		
		
		private function reloadTotalChar(p_xEvent:ModelEvent):void{
			var l_xShop:Shop = p_xEvent.data._xShop;
			addTotalChar(l_xShop);
		}
		
		private function addTotalChar(p_xShop:Shop):void{
			
			
			var l_aReturnValues:Array = p_xShop.chooseChar() ;
			
			var l_sCharSlug:String = l_aReturnValues[0];
			var l_sCharQuotes:String = l_aReturnValues[1];
			
			trace(l_sCharSlug)
			trace(l_sCharQuotes)
			var l_xQuote:Quote = G._aQuotes[l_sCharQuotes];
			var l_sActualQuote:String = l_xQuote.chooseText();
			
			
			var l_xChar:Character = G._aChars[l_sCharSlug];
			l_aReturnValues = l_xChar.chooseInstance();
			
			var l_sCharImg:String = l_aReturnValues[0];
			var l_sCharName:String = l_aReturnValues[1];
			trace(l_sCharImg)
			trace(l_sCharName)
			trace(l_sActualQuote)
			addCharImg(l_sCharImg);
			_sCharName = l_sCharName
			//addCharText(l_sCharName);
			addCharQuote(l_sActualQuote)
			
		}
		
		private function addCharOptionButton( mc_btnholder:MovieClip, l_sBtnText:String, l_xData:Object = null, p_nX:int = 0, p_nY:int = 0):void{
			
			var l_nTextY:int =  mc_btnholder.height + (_nCharOptionsCount?10:0);
			
			var l_funCallback:Function = l_xData._funCallback;
				
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, l_xData._sButtonFrame);
			
			l_xButton.y = - l_nTextY; 
			
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,l_xData);
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
			l_xButton.setCallback(l_e);
			
			if(p_nX && p_nY){
				l_xButton.x = p_nX
				l_xButton.y = p_nY
			}
			
			
			mc_btnholder.addChild(l_xButton);
			
			_nCharOptionsCount++;
			
		}
		
		
		private function addShopButtons(p_xShop:Shop ):void{
			var mc_btnholder:MovieClip = new MovieClip;
			var l_nCount:int = 0; 
			
			addCharOptionButton(mc_btnholder,'clear',{_sButtonFrame:'exitmap', _funCallback:clickBackToTown}, 50, 540);
			 
			addCharOptionButton(mc_btnholder,'simeon',{_sButtonFrame:'simeon', _funCallback:clickSimeonAsk},250,540);
			addCharOptionButton(mc_btnholder,'reload',{_sButtonFrame:'char_reload_btn', _funCallback:reloadTotalChar, _xShop:p_xShop},700,540); 
			 
			if(p_xShop._aGoods.length){
				addCharOptionButton(mc_btnholder,'buy',{_sButtonFrame:'buy', _funCallback:fieldBarterClick,_aGoods: p_xShop._aGoods, _sGoodsHeader: p_xShop._sGoodsHeader},490,540);
			}
			 
			mc_btnholder.x=0
			mc_btnholder.y=0
			_view.mc_shopbuttons.x = 0
			_view.mc_shopbuttons.y = 0
			_view.mc_shopbuttons.addChild( mc_btnholder );
			_view.mc_shopbuttons.visible = true;
			
		}
		
		
		private function fieldBarterClick(p_xEvent:ModelEvent):void{
			dispatchEvent(new ModelEvent(ModelEvent.BARTER_POPUP , {_aGoods: p_xEvent.data._aGoods, _sGoodsHeader: p_xEvent.data._sGoodsHeader}));
			_model.addEventListener(ModelEvent.FINISH_BARTER, finishBarter);
			
		}
		
		private function finishBarter(e:ModelEvent):void{
			//Not necessary yet
		}
		
		private function clickSimeonAsk(e:Event):void{
			addCharQuote(_sSimeonText);
		}
		
		private function updateDestinations(p_aRevealed:Array):Boolean{
			var l_bNewPlaces:Boolean = false
			for each(var l_xDestination:Object in p_aRevealed){ 
				if(!G._aCities[l_xDestination._sSlug]._bOnMap){
					l_bNewPlaces = true;
					G._aCities[l_xDestination._sSlug]._bOnMap = true
					if(l_xDestination._bKnown)
						G._aCities[l_xDestination._sSlug]._bNameKnown = true
				}
			}
			return l_bNewPlaces
		}
		
		
		private function addCharQuote(p_sText:String):void{
			
			_view.mc_charquote.visible = true;
			
			//txt_CharQuote.text = p_sText;
			
			//txt_CharQuote.height = txt_CharQuote.numLines * 25; 
			
			//txt_CharQuote.y = G.TOTAL_HEIGHT - txt_CharQuote.height - G.STATUS_HEIGHT-10;
			
			_view.mc_charquote.txt_char.htmlText = "<span class='char'><span class='name'>"+_sCharName+"</span> - "+p_sText+"</span>"
			_view.mc_charquote.txt_char.height = _view.mc_charquote.txt_char.numLines * 25; 
			_view.mc_charquote.txt_char.y = G.TOTAL_HEIGHT - _view.mc_charquote.txt_char.height - G.STATUS_HEIGHT-10;
			
		}
		
		
		private function addShopText(p_sText:String):void{
			
			_view.mc_shoptext.visible = true;
			_view.mc_shoptext.txt_shoptext.text = p_sText
		}
		
		private function addCharText(p_sText:String):void{
			
			txt_CharName.autoSize  = TextFieldAutoSize.LEFT;
			_view.mc_shoptext.txt_shoptext.autoSize = TextFieldAutoSize.LEFT;
			
			txt_CharName.text = p_sText;
			_view.mc_chartext.visible = true;
			
			var l_nCharName:int = txt_CharName.width
			
			txt_CharName.autoSize  = TextFieldAutoSize.NONE;
			
			txt_CharName.x =  G.TOTAL_WIDTH - txt_CharName.width  - 25
			
	//		if(l_nCharName < l_nShopWidth)
	//			 txt_CharName.width = l_nShopWidth;
	//		else
	//			txt_ShopTitleDrill.width = l_nCharName;
		}
		
		
		
		private function addCharImg(p_sText:String):void{
			_xCharLoader.load(new URLRequest( 'swf/image/'+p_sText ));
			_xCharLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadCharImg, false,0,true);
		}
		
		private function loadCharImg(p_xEvent:Event):void{
			_view.mc_charholder.visible = true;
			
			clearMovieClip(_view.mc_charholder)
			
			_view.mc_charholder.addChild(p_xEvent.target.content);
			p_xEvent.target.removeEventListener(Event.COMPLETE, loadCharImg);
			
			_view.mc_charholder.x = (G.TOTAL_WIDTH - _view.mc_charholder.width + 10) ;
			
			_view.mc_charholder.y = (G.TOTAL_HEIGHT - G.STATUS_HEIGHT - _view.mc_charholder.height)*3/4 ;
			
			_view.mc_charholder.visible = true;
		}
		
		
		
		private function addShopImg(p_sText:String):void{
			_xShopLoader.load(new URLRequest( 'swf/image/'+p_sText ));
			_xShopLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadShopImg, false,0,true);
		}
		
		private function loadShopImg(p_xEvent:Event):void{
			_view.mc_shopholder.visible = true;
			clearMovieClip(_view.mc_shopholder)
			_view.mc_shopholder.addChild(p_xEvent.target.content);
			p_xEvent.target.removeEventListener(Event.COMPLETE, loadShopImg);
			
			_view.mc_shopholder.x = (G.TOTAL_WIDTH - _view.mc_shopholder.width)/4 ;
			
			_view.mc_shopholder.y = (G.TOTAL_HEIGHT - G.STATUS_HEIGHT- _view.mc_shopholder.height)/2 ;
			
		}
		
		
		
		private function addShopBg(p_sColor:int):void{
			
			var s_bg:Sprite = new Sprite();
			_view.mc_shopbg.addChild(s_bg);
			
			s_bg.x = s_bg.y = 0;
			var g:Graphics = s_bg.graphics; 
			g.beginFill(p_sColor);
			g.drawRect(0, 0, G.TOTAL_WIDTH , G.TOTAL_HEIGHT);
		}
		
		private function addBg(p_sColor:int):void{
			
			var s_bg:Sprite = new Sprite();
			_view.mc_bg.addChild(s_bg);
			
			s_bg.x = s_bg.y = 0;
			var g:Graphics = s_bg.graphics; 
			g.beginFill(p_sColor);
			//g.drawRect(0, 0, G.TOTAL_WIDTH , G.TOTAL_HEIGHT );
		}
		
		private function stopClips():void{
			
		}
		
		private function emptyClips():void{
			
			if(_xDestination){
				_xDestination._aCachedShops = _aShops
			}
			clearMovieClip(_view.mc_shopholder);
			clearMovieClip(_view.mc_shopbuttons);
			clearMovieClip(_view.mc_grid);
			clearMovieClip(_view.mc_backtoroad);
			clearMovieClip(_view.mc_map);
			clearMovieClip(_view.mc_vistaholder);
			clearMovieClip(_view.mc_townimg);
			
		}
		
		private function hideClips():void{
			_view.mc_grid.visible = false; 
			_view.mc_bg.visible = false;
			_view.mc_text.visible = false; 
			_view.mc_shopbg.visible = false;
			_view.mc_shopholder.visible = false;
			_view.mc_charholder.visible = false;
			_view.mc_shoptext.visible = false;
			_view.mc_chartext.visible = false;
			_view.mc_charquote.visible = false;
			_view.mc_shopbuttons.visible = false;
			_view.mc_backtoroad.visible = false;
			_view.txt_TownTitle.visible = false
			_view.mc_scroll.visible = false
			_view.mc_scrollup.visible = false
			_view.mc_scrolldown.visible = false
			_view.mc_scrolltray.visible = false
			
			_view.mc_townimg.visible = false
			_view.mc_gps.visible = false
			_view.mc_vistaholder.visible = false
			_view.mc_vistaborder.visible = false
			_view.txt_description.visible = false
		}
		private function showClips():void{
			//_view.mc_grid.visible = true; 
			_view.mc_bg.visible = true;
			_view.mc_text.visible = true;
			_view.txt_TownTitle.visible = true
			_view.mc_scroll.visible = true
			_view.mc_scrollup.visible = true
			_view.mc_scrolldown.visible = true
			_view.mc_townimg.visible = true
			_view.mc_scrolltray.visible = true
			_view.txt_description.visible = true
			_view.mc_vistaborder.visible = true
		}
	}
}