package com.uglypersonsface.views {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.models.application.Application;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;


	public class View extends Sprite {
		protected var _model:Application = null;
		protected var _assetSwf:AssetSwf = null;
		protected var _symbolClassName:String = null;
		public var _view:MovieClip = null;


		public function View (p_xModel:Application, p_xAssetSwf:AssetSwf = null, p_sSymbolClassName:String = null) {
			_model = p_xModel;
			_assetSwf = p_xAssetSwf;
			_symbolClassName = p_sSymbolClassName;

			if (_assetSwf != null) {
				if (_assetSwf.loaded) {
					buildView();
				} else {
					_assetSwf.addEventListener(Event.INIT, buildView, false, 0, true);
				}
			} else {
				buildView();
			}
		}
		
		public function clearMovieClip(p_mc:MovieClip):MovieClip{
			
			while(p_mc.numChildren){
				p_mc.removeChildAt(0);
			}
			return p_mc;
		}
		
		
		protected function buildView (p_xEvent:Event = null) : void {
			// get the view
			_view = _assetSwf.newSymbolInstance(_symbolClassName);

			// allow implementation specific view build
			buildViewHook();

			// add the view so we can see it
			if (_view == null) {
				_view = new MovieClip();
			}
			this.addChild(_view);
		}
		

		protected function buildViewHook () : void {
			// optional override
		}
	}
}