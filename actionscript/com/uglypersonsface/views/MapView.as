package com.uglypersonsface.views {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.objects.Destination;
	
	import flash.display.MovieClip;
	import flash.text.*;

	public class MapView extends View {
		
		private var _xMapFormat:TextFormat = new TextFormat();
		private var _xLegendFormat:TextFormat = new TextFormat();
		private var _xStatusFormat:TextFormat = new TextFormat();
		
		public function MapView (p_xModel:Application, p_xAssetSwf:AssetSwf) {
			super(p_xModel, p_xAssetSwf, "mc_MapView"); 
		}


//--------------------------------------------------------------------------------------------------
//	HOOK

		override protected function buildViewHook () : void {
			_view.stop();
			_view.visible = false;
			
			_model.addEventListener(ModelEvent.SHOW_MAP, showMap, false, 0, true);
			
			_xMapFormat.align = TextFormatAlign.LEFT;
			_xMapFormat.font = 'Arial';
			_xMapFormat.size = 12;
			_xMapFormat.color = 0x111111;
			_xMapFormat.bold = true;
			
			
		}
		
		
//--------------------------------------------------------------------------------------------------
//	LISTENERS
		private function showMap(p_xEvent:ModelEvent = null):void{
			_view.mc_status.x = 0
			_view.mc_status.y = G.TOTAL_HEIGHT-G.STATUS_HEIGHT 
			_view.mc_status.graphics.beginFill(0x000000,1 );
			_view.mc_status.graphics.drawRect( 0, 0, G.TOTAL_WIDTH, G.STATUS_HEIGHT );
			_view.mc_status.graphics.endFill();
			
			G.addCoords()
			var l_aCoords:Array = G._aCoords
			_view.mc_car.x = G._nX 
			_view.mc_car.y = G._nY
			_view.visible = true;
			
			dispatchEvent(new ModelEvent(ModelEvent.HIDE_STATUS_BAR));
			
			var mc_lines:MovieClip = new MovieClip();
			_view.mc_lines.addChild(mc_lines)
			mc_lines.graphics.lineStyle(2,0x990000,9);   
			for(var i:int=0; i<G._aCoords.length;i++){  
				var l_xObj:Object = G._aCoords[i]
				if(!i)
					mc_lines.graphics.moveTo(l_xObj.x,l_xObj.y);
				else
					mc_lines.graphics.lineTo(l_xObj.x,l_xObj.y);
					
			}
		    
			var l_funCallback:Function = closeMap;
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, 'exitmap');  
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,{});
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
			l_xButton.setCallback(l_e);
			l_xButton.x = 45
			l_xButton.y = 538
			_view.mc_cityholder.addChild(l_xButton); 
			
			if(G._sCurrentDestination){
				makeLegendText(G._sCurrentDestination)
			}
			var l_sButtonType:String
			for each(var l_xDestination:Destination in G._aCities){
				if(l_xDestination._bOnMap){ 
					
					l_funCallback = cityClick;
					if(l_xDestination._bArrivialSign){
						l_sButtonType = l_xDestination._bIsRuined? 'map_cityruined' : 'map_city'
					}
					else{
						l_sButtonType = 'map_attraction'
					}
					l_xButton = new ButtonView(G._application, G._assetSwf, l_sButtonType,false,overBtn,outBtn,G._sCurrentDestination == l_xDestination._sSlug);
					l_e = new ModelEvent(ModelEvent.BUTTON_CLICK ,{_sDestination: l_xDestination._sSlug});
					l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
					l_xButton.setCallback(l_e);
					l_xButton.x = l_xDestination._nX
					l_xButton.y = l_xDestination._nY
					_view.mc_cityholder.addChild(l_xButton); 
					
					var l_sName:String = (l_xDestination._bNameKnown)?l_xDestination._sName:'???';
					var txt_cityname:TextField = getCityText( l_sName );
					txt_cityname.x = l_xDestination._nX + 10
					txt_cityname.y = l_xDestination._nY - 8
					
				}
			}
			shouldShowRoadBtn();
			
		}
		
		private function makeLegendText(p_sSlug:String):void{
			clearMovieClip(_view.mc_textholder)
			var l_sName:String = (G._aCities[p_sSlug]._bNameKnown)?G._aCities[p_sSlug]._sName:'??????'
			var txt_status:String = l_sName+' - '+(int)(G.calcDistanceToDestination(p_sSlug)* G.MILES_TO_PIXEL)+' miles'
			_view.txt_citystatus.text = txt_status
		}
		
		private function overBtn(e:ModelEvent):void{
			makeLegendText (e.data._sDestination)
		}
		private function outBtn(e:ModelEvent):void{
			makeLegendText (G._sCurrentDestination)
		}
		
		private function closeMap(p_xEvent:ModelEvent):void{
			_view.visible = false; 
			clearMovieClip(_view.mc_cityholder)
			clearMovieClip(_view.mc_textholder) 
			dispatchEvent(new ModelEvent(ModelEvent.DISPLAY_STATUS ));
			
			dispatchEvent(new ModelEvent(ModelEvent.SHOW_STATUS_BAR));
		} 
		
				
		private function getCityText(p_sText:String ):TextField{
			
			var txt_options:TextField = new TextField();
			txt_options.defaultTextFormat =  _xMapFormat;
			txt_options.text = p_sText;
			txt_options.width=200;
			txt_options.selectable = false;
			
			txt_options.type = TextFieldType.DYNAMIC;
			txt_options.wordWrap = true;
			txt_options.autoSize = TextFieldAutoSize.CENTER;
			
			return txt_options;
			
		}
		
		private function resetMap():void{
			
			clearMovieClip(_view.mc_cityholder)
			clearMovieClip(_view.mc_textholder)
			shouldShowRoadBtn();
		}
		
		
		private function cityClick(p_xEvent:ModelEvent):void{
			var l_sSlug:String = p_xEvent.data._sDestination;
			G._sCurrentDestination = l_sSlug
			G._nDistanceToDestination = G.calcDistanceToDestination()*G.MILES_TO_PIXEL
			resetMap();
			showMap();
		}
		
		private function shouldShowRoadBtn():void{
			if (G._nDistanceToDestination){
				var l_funCallback:Function = jumpToRoad; 
				var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf, 'to_road');  
				var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK ,{});
				l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
				l_xButton.setCallback(l_e);
				l_xButton.x = 700  
				l_xButton.y = 538
				_view.mc_cityholder.addChild(l_xButton); 
			}
			
		}
		private function jumpToRoad(p_xEvent:ModelEvent):void{
			closeMap(p_xEvent)
			dispatchEvent(new ModelEvent(ModelEvent.BACK_TO_ROAD_CLICK ));	
		}
	}
}