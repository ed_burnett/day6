package com.uglypersonsface.views {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.objects.*;
	
	import flash.display.Graphics;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.*;
	
		
	public class PopupView extends View { 
		
		private var g_sSetType:String;
		private var g_xSet:Object;
		
		private var _xTextFormat:TextFormat = new TextFormat();
		private var _xOptionsFormat:TextFormat = new TextFormat();
		private var _xStyle:StyleSheet = new StyleSheet;
		
		private var _xLoader:Loader = new Loader();
		
		private var _bSpaceClosePopup: Boolean = false
		private var _bSpaceStartHunt: Boolean = false
		private var _bSpaceStartHide: Boolean = false
		private var _bSpaceClickVista: Boolean = false
		
		private var _bEtcCloseHide: Boolean = false
		private var _bEtcCloseHunt: Boolean = false
		
		private var _nOptionCount:int = 0;
		
		public function PopupView (p_xModel:Application, p_xAssetSwf:AssetSwf) {
			super(p_xModel, p_xAssetSwf, "mc_PopupView");
		}


//--------------------------------------------------------------------------------------------------
//	HOOK

		override protected function buildViewHook () : void {
			_view.stop();
			
			hideClips();
			stopClips();
			
			_model.addEventListener(ModelEvent.POPUP_OPTIONS, popupOptions, false, 0, true);
			_model.addEventListener(ModelEvent.POPUP_VISTA, popupVista, false, 0, true);
			_model.addEventListener(ModelEvent.POPUP_TEXT, popupText, false, 0, true);
			_model.addEventListener(ModelEvent.POPUP_SIGN, popupSign, false, 0, true);
			_model.addEventListener(ModelEvent.BARTER_POPUP, barterPopup, false, 0, true);
			_model.addEventListener(ModelEvent.ANNOUNCE_POPUP, announcePopup, false, 0, true);
			_model.addEventListener(ModelEvent.HIDE_BTN_CLICK, popupHide, false, 0, true);
			_model.addEventListener(ModelEvent.HUNT_BTN_CLICK, popupHunt, false, 0, true);
			
			stage.addEventListener( KeyboardEvent.KEY_DOWN, keyDownHandler );
			
			var t:Object = new Object();
			t.fontSize = 19;
			t.color = "#000000";
			
			
			var p:Object = new Object();
			p.fontSize = 18;
			p.color = "#000000";

			_xStyle.setStyle("p", p);
			_xStyle.setStyle(".text", t);
			
			_xOptionsFormat.align = TextFormatAlign.LEFT;
			_xOptionsFormat.font = 'Arial'; 
			_xOptionsFormat.size = 18;
			_xOptionsFormat.color = 0x000000; 
			
			_xTextFormat.align = TextFormatAlign.LEFT;
			_xTextFormat.font = 'Arial';
			_xTextFormat.leftMargin = 5;
			_xTextFormat.rightMargin = 5;  
			_xTextFormat.size = 15;
			_xTextFormat.color = 0x00000; 
			
			_view.mc_clickoverlay.alpha = 0;
			_view.mc_clickoverlay.width = G.TOTAL_WIDTH;
			_view.mc_clickoverlay.height = G.TOTAL_HEIGHT;
			_view.mc_clickoverlay.buttonMode = true;
			
			_view.mc_clickablebg.alpha = 0;
			_view.mc_clickablebg.width = G.TOTAL_WIDTH+G.TOTAL_WIDTH;
			_view.mc_clickablebg.height = G.TOTAL_HEIGHT+G.TOTAL_HEIGHT;
			_view.mc_clickablebg.buttonMode = true;
			
			
			_view.mc_hide.x=G.TOTAL_WIDTH/2 - _view.mc_hide.width/2 ;
			_view.mc_hide.y=G.TOTAL_HEIGHT/2 - _view.mc_hide.height/2 ;
			
			_view.mc_hunt.x=G.TOTAL_WIDTH/2 - _view.mc_hunt.width/2 ;
			_view.mc_hunt.y=G.TOTAL_HEIGHT/2 - _view.mc_hunt.height/2 ;
			
			/*
			var mc_shop:MovieClip = new MovieClip;
			mc_shop.graphics.beginFill(0xFF0000);
			mc_shop.graphics.drawRect(0,0,50,50);
			mc_shop.graphics.endFill();
			mc_shop.y = l_nCount*100;
			mc_shop._nCount = l_nCount;
			mc_shop.addEventListener(MouseEvent.CLICK, clickShop);
			*/
		}
 

//--------------------------------------------------------------------------------------------------
//	LISTENERS
		
		private function resetKeyHandlers():void{
			
			 _bSpaceClosePopup = false
			 _bSpaceStartHunt = false
			 _bSpaceStartHide = false
			 _bSpaceClickVista = false

		
			 _bEtcCloseHide = false
			 _bEtcCloseHunt = false
		}
		
		private function keyDownHandler(p_xEvent:KeyboardEvent):void{
			if(G._bKeyOverride){
				G._bKeyOverride = false
				return 
			}
			if (p_xEvent.keyCode==32 && _bSpaceClosePopup) {
				trace('space _bSpaceClosePopup')
				_bSpaceClosePopup=false
				clickedOverlay(p_xEvent);
			}
			if (p_xEvent.keyCode==32 && _bSpaceStartHunt) {
				trace('space _bSpaceStartHunt')
				_bSpaceStartHunt=false
				huntOk();
			}
			if (p_xEvent.keyCode==32 && _bSpaceStartHide) {
				trace('space _bSpaceStartHide')
				_bSpaceStartHide=false
				hideOk();
			}
			if (p_xEvent.keyCode==27 && _bEtcCloseHide) {
				trace('space _bEtcCloseHide')
				_bEtcCloseHide=false
				clickedOverlay();
			}
			if (p_xEvent.keyCode==27 && _bEtcCloseHunt) {
				trace('space _bEtcCloseHunt')
				_bEtcCloseHunt=false
				clickedOverlay();
			}
			if (p_xEvent.keyCode==32 && _bSpaceClickVista) {
				trace('space _bSpaceClickVista')
				_bSpaceClickVista=false 
				clickedOverlay();
			}
			
			
			
		}
		
		private function popupVista(p_xEvent:ModelEvent):void{
			var l_sFrame:String = p_xEvent.data._sFrame;
			addCenterImg(l_sFrame);
			addBlackUnClickableBg();
			_bSpaceClickVista=true
		}
		
		private function popupText(p_xEvent:ModelEvent):void{
			trace('popuptxt')
			addText(p_xEvent.data._sTextString, p_xEvent.data._aData);
		}
		
		
		private function popupHide(p_xEvent:ModelEvent):void{	
			addHide();
		}
		
		
		private function popupHunt(p_xEvent:ModelEvent):void{	
			addHunt();
		}
		
		private function clickedOverlay(p_xEvent:Event=null):void{
			_view.mc_announcementoverlay.visible = false;
			_view.mc_announcementoverlay.removeEventListener(MouseEvent.CLICK, clickedOverlay);
			_view.mc_options.removeEventListener(MouseEvent.CLICK, clickedOverlay);
			_view.mc_clickoverlay.removeEventListener(MouseEvent.CLICK, clickedOverlay);
			stage.removeEventListener( KeyboardEvent.KEY_DOWN, clickedOverlay );
			hideClips();
			
			dispatchEvent(new ModelEvent(ModelEvent.OVERLAY_CLICK ));
			
		}
		
		
		private function barterPopup(p_xEvent:ModelEvent):void{
			
			var p_aGoods:Array = p_xEvent.data._aGoods;
			var p_sGoodsHeader:String = p_xEvent.data._sGoodsHeader;
			addClickableBg(finishBarter); 
			
			setOption('barter', {_aGoods:p_aGoods, _sGoodsHeader:p_sGoodsHeader} );
			
		}
		private function popupOptions(p_xEvent:ModelEvent):void{
			setOption('optionchoose', p_xEvent.data );
		}
		
		
		private function finishBarter(p_xEvent:MouseEvent):void{
			hideClips();
			dispatchEvent(new ModelEvent(ModelEvent.FINISH_BARTER ));
		}
		
//--------------------------------------------------------------------------------------------------
//	HELPERS
		
		
		private function addCenterImg(p_sText:String):void{
			
			_xLoader.load(new URLRequest( 'swf/image/'+p_sText ));
			_xLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadCenterImg, false,0,true);
			
		}
		
		private function loadCenterImg(p_xEvent:Event):void{
			_view.mc_vistaholder.visible = true;
			_view.mc_vistaholder.addChild(p_xEvent.target.content);
			p_xEvent.target.removeEventListener(Event.COMPLETE, loadCenterImg);
			
			_view.mc_vistaholder.x = (G.TOTAL_WIDTH - _view.mc_vistaholder.width)/2 ;
			_view.mc_vistaholder.y = (G.TOTAL_HEIGHT - _view.mc_vistaholder.height)/2 ;
			createClickOverlay();
		}
		
		private function announcePopup(p_xEvent:ModelEvent):void{
			
			var l_sText:String = p_xEvent.data._sText;
			setOption('announcement', {_sText:l_sText} );
			
		}
		
		private function removeHide(e:MouseEvent):void{
			_view.mc_hide.visible = false
			_view.mc_clickablebg.visible = false;  
			_view.mc_bg.visible = false;
		}
		
		private function addHide():void{
			
			 _view.mc_hide.txt_alert.text = G.alertStars()
			 _view.mc_hide.txt_health.text = G.HEALTH  
			 _view.mc_hide.txt_daysoffood.text = G.daysOfFood()
			_view.mc_hide.visible = true
			
			_view.mc_hide.mc_ok.addEventListener(MouseEvent.CLICK, hideOk, false, 0, true);
			_view.mc_hide.mc_x.addEventListener(MouseEvent.CLICK, removeHide, false, 0, true);
			
			addClickableBg(removeHide);
			_bSpaceStartHide=true 
			_bEtcCloseHide=true
		}
		
		private function addHunt():void{
			
			
			addClickableBg(clickedOverlay); 
			_view.mc_hunt.visible = true
			
			_view.mc_hunt.mc_ok.addEventListener(MouseEvent.CLICK, huntOk, false, 0, true);
			_view.mc_hunt.buttonMode = false;   
			_view.mc_hunt.mc_x.addEventListener(MouseEvent.CLICK, clickedOverlay, false, 0, true);
			
			var l_sStress:String = G.STRESS.toString()
			 _view.mc_hunt.txt_alert.text = G.alertStars()
			 _view.mc_hunt.txt_stress.text = G.calculatedStressNumber()
			 _view.mc_hunt.txt_outcome.text = G.getHuntVictoryOutcome()
			 _view.mc_hunt.txt_hp.text = G._xCurrentDestination._nDefense+"/"+G._xCurrentDestination._nInitDefense
			 _view.mc_hunt.txt_desc.text = "Attacking "+G._xCurrentDestination._sName+", a "+G._xCurrentDestination.condition()+G._xCurrentDestination.type()
			 _view.mc_hunt.txt_health.text = G.HEALTH 
			_bSpaceStartHunt=true
			_bEtcCloseHunt=true
		}
		
		private function huntOk(e:MouseEvent=null):void{
			_view.mc_hunt.visible = false
			dispatchEvent(new ModelEvent(ModelEvent.HUNT ));
		}
		
		private function hideOk(e:MouseEvent=null):void{
			dispatchEvent(new ModelEvent(ModelEvent.HIDE_OUT));
			removeHide(null);
			trace('hideOk')
		}
		
		private function addText(p_sText:String, p_aData:Array):void{
			setOption('announcement',{_sText:p_sText, _aData:p_aData});
			
		}
		
		private function getButtonText(p_sText:String, p_sTextType:String ):TextField{
			var txt_options:TextField = new TextField();
			txt_options.defaultTextFormat =  _xOptionsFormat;
			txt_options.styleSheet = _xStyle;
			txt_options.text = '<p class="button">'+p_sText+'</p>';
			txt_options.selectable = false;
			
			txt_options.type = TextFieldType.DYNAMIC;
			txt_options.wordWrap = true;
			txt_options.autoSize = TextFieldAutoSize.CENTER;
			return txt_options;
			
		}
		
		private function addOptionLine( mc_btnholder:MovieClip, l_sBtnText:String, l_xData:Object = null,p_sTextType:String=null):void{
			var l_nAdditionalHeight:int = 7;
			var l_nTextX:int = 0;
			var l_nTextY:int =  mc_btnholder.height + (_nOptionCount?l_nAdditionalHeight:0);
			
			if(l_xData){ 
				var l_bDead:Boolean = l_xData._bDead;
				var l_funCallback:Function = l_xData._funCallback;
				var l_sButtonFrame:String = l_xData._sButtonFrame;
				var l_nItemCount:int = l_xData._nItemCount;
				var l_sPrice:String = l_xData._sPrice;
				
				var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf,l_xData._sButtonFrame,l_bDead);
				var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK, l_xData );
				
				l_nTextX = G.BUTTON_WIDTHS[l_xData._sButtonFrame];
				
				if(!l_bDead){
					l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, l_funCallback, false, 0, true);
					l_xButton.setCallback(l_e);
				}
				l_xButton.y =  l_nTextY+3;  
				l_xButton.x =  10; 
				
				l_xButton.setOptionTxt( l_sBtnText )
				l_xButton.setPriceTxt( l_sPrice )
				
				mc_btnholder.addChild(l_xButton);
			}
			else{
				var txt_options:TextField = getButtonText( l_sBtnText,p_sTextType );
				txt_options.width = G.OPTIONS_MAX_WIDTH - l_nTextX - 40 ;
				txt_options.y = l_nTextY + 3;
				txt_options.x = l_nTextX + 0;
				txt_options.selectable = false;
				
				mc_btnholder.addChild(txt_options);
			}
			_nOptionCount++;
			
			
		}
		
		private function refreshPopup():void{ setOption( g_sSetType, g_xSet ) }
		
		private function setOption(p_sSetType:String, p_xSet:Object):void{
			
			g_sSetType = p_sSetType;
			g_xSet = p_xSet;
			
			_view.mc_options.visible = true;
			//_view.mc_options.buttonMode = true
			_view.mc_options.gotoAndStop('f_'+p_sSetType);
			
			_nOptionCount = 0;
			var l_nCost:Number = 0;
			var l_aData:Array
			var l_sBtnText:String = '';
			var l_sText:String = ''
			var l_fCallback:Function = null;
			var l_sButtonFrame:String = ''
			var l_bDead:Boolean = false;
			var mc_btnholder:MovieClip
			
			if(p_xSet._sTextString && p_xSet._sTextString.indexOf('%name')>=0){
				r = /\%name/;
				p_xSet._sTextString = p_xSet._sTextString.replace(r,  G.randomChar()._sName); 
			}
			
			switch(p_sSetType){    
				
				case 'optionchoose': 
					addDarkUnClickableBg();
					_view.mc_announcementoverlay.visible = false;
					
					mc_btnholder = _view.mc_options.mc_btnholder;
					clearMovieClip(mc_btnholder);
					
					l_sButtonFrame = 'square';
					var r:RegExp
					var l_aOptions:Array = p_xSet._aOptions; 
					var l_sTextHeader:String= p_xSet._sTextString
					//_xStyle
					
					_view.mc_options.txt_choicetxt.styleSheet = _xStyle 
					_view.mc_options.txt_choicetxt.autoSize = TextFieldAutoSize.LEFT;
					_view.mc_options.txt_choicetxt.htmlText = '<span class="text">'+l_sTextHeader+'</span>'; 
					
					//_view.mc_options.txt_choicetxt.selectable = false; 
					for each(var _xOption:Object in l_aOptions){ 
						
						l_fCallback = choiceChoose;
						l_sBtnText =  _xOption._sText; 
						
						if(l_sBtnText.indexOf('%fine')>=0){
							r = /\%fine/;
							l_sBtnText = l_sBtnText.replace(r,  G.calcFine()); 
							l_bDead = (G._nCurrentDollars<G.calcFine());
						}else if(l_sBtnText.indexOf('%binyah')>=0){
							r = /\%binyah/;
							l_sBtnText = l_sBtnText.replace(r,  G.calcBinyah()); 
							l_bDead = (G._nCurrentFood<G.calcBinyah());
						}
						else
							l_bDead = false
						
						switch(_xOption._sCheck){
							case 'food':
								if(_xOption._nMin){
									l_bDead = (G._nCurrentFood<_xOption._nMin)
								}
							break;
							case 'dollars':
								if(_xOption._nMin){
									l_bDead = (G._nCurrentDollars<_xOption._nMin)
								}
							break;
							case 'car':
								if(_xOption._sIs){
									l_bDead = (G._sCurrentCar != _xOption._sIs)
								}
							break;
						}
						
						
						addOptionLine( mc_btnholder, l_sBtnText, {_bDead:l_bDead,_sButtonFrame:l_sButtonFrame, _funCallback:l_fCallback, _sResult:_xOption._sResult} );
					}
					
					_view.mc_options.mc_btnholder.y = _view.mc_options.txt_choicetxt.height+40;
					
					_view.mc_options.mc_options_bg.height = _view.mc_options.txt_choicetxt.height + _view.mc_options.mc_btnholder.height+70;
					
				break;
				
				case 'announcement': 
					
					addClickableBg(clickedOverlay); 
					l_aData = p_xSet._aData
					l_sText = p_xSet._sText ;
					var l_xChar:Passanger = G.randomChar()
					if(l_xChar)
						l_sText = l_sText.replace(/%name/,l_xChar._sName)
					l_sText += addTextWithAttributes(l_aData)
					_view.mc_options.txt_announcement.autoSize = TextFieldAutoSize.LEFT;
					_view.mc_options.txt_announcement.htmlText = '<span class="text">'+l_sText+'</span>'; ; 
					_view.mc_options.txt_announcement.styleSheet = _xStyle 
					_view.mc_options.txt_announcement.selectable =true 
					_view.mc_options.txt_announcement.mouseEnabled  = false
					_view.mc_options.mc_options_bg.height = _view.mc_options.txt_announcement.height+35;
					
					_view.mc_options.mc_x.addEventListener(MouseEvent.CLICK, clickedOverlay,false,0,true);
					_view.mc_options.mc_x.buttonMode = true
					
					_bSpaceClosePopup = true
			
				break;
				
				case 'barter': 
				
					mc_btnholder = _view.mc_options.mc_btnholder;
					clearMovieClip(mc_btnholder);
					_view.mc_options.buttonMode = false
					
					l_sButtonFrame = 'square'; 
					
					var l_aGoods:Array = p_xSet._aGoods; 
					var l_sGoodsHeader:String= p_xSet._sGoodsHeader;
					_view.mc_options.txt_goodsheader.text = l_sGoodsHeader;
					
					_view.mc_options.mc_x.addEventListener(MouseEvent.CLICK, clickedOverlay,false,0,true);
					_view.mc_options.mc_x.buttonMode = true
					
					l_sBtnText =  "<font size='14'>Cash: $"+G._nCurrentDollars
					l_sBtnText +=  "\nFood: "+G._nCurrentFood+' meals'
					l_sBtnText +=  '\nDays of food: '+G.daysOfFood()
					l_sBtnText += '\nGas: '+G.getTankStatus()+'</font>';
					addOptionLine( mc_btnholder, l_sBtnText);
					
					var l_xCar:Car
					var l_xCurrentCar:Car = G._aCars[G._sCurrentCar]
					var l_nCount:int
					var l_nTotalCount:int
					var l_sPrice:String
					for each(var l_xGoods:Goods in l_aGoods){ 
						
						l_nCost = makeCost(l_xGoods._nCost);
						l_nCount = l_xGoods._nCurrentCount
						l_nTotalCount = l_xGoods._nTotalCount
						trace(l_xGoods._sItemType)
						switch(l_xGoods._sItemType){
							
							case 'item':
								// Items Left
								l_sBtnText = " <font size='12'>"
								if (l_xGoods._xMods.health >0){
									l_sBtnText += "+"+l_xGoods._xMods.health+" Health - " 
								}
								l_sBtnText += l_nCount+" Items left</font>"
								addOptionLine( mc_btnholder, l_sBtnText);
								
								
								
								if(l_nTotalCount && !l_nCount){ 
									l_bDead = true
								}
								l_sBtnText = l_xGoods._sName
								l_sPrice = '-$'+l_nCost;
								l_fCallback = buyItem;
								addOptionLine( mc_btnholder, l_sBtnText, {_sPrice: l_sPrice,_bDead:l_bDead,_sButtonFrame:l_sButtonFrame, _nCost:l_nCost, _funCallback:l_fCallback, _nItemCount:1, _xGoods:l_xGoods} );
								
								break;
							
							case 'car':
								l_fCallback = buyCar;
								
								l_xCar = G._aCars[l_xGoods._sName]
								
								l_sBtnText =  "<font size='20'>"+l_xCar._sName+'</font>';
								addOptionLine( mc_btnholder, l_sBtnText);
								
								l_sBtnText =  l_xCar._sDesc;
								addOptionLine( mc_btnholder, l_sBtnText);
								
								l_bDead = true
								var l_nTrade:int = 0
								if(l_xGoods._sName != l_xCurrentCar._sSlug){
									l_bDead = false
									l_nTrade = l_xCar._nCost - (l_xCurrentCar._nCost/2)
								}
								l_sBtnText = l_xCar._sName
								l_sPrice = '-$'+l_nTrade;
								addOptionLine( mc_btnholder, l_sBtnText, {_sPrice: l_sPrice, _bDead:l_bDead,_sButtonFrame:l_sButtonFrame, _nCost:l_nTrade, _funCallback:l_fCallback, _xGoods:l_xCar} );
								
								break;
							case 'food':
								l_fCallback = buyFood;
								
								l_sBtnText = "<font size='12'>"+G.foodStringFromInt(l_xGoods._xMods.quality)+" Quality Meal</font>"
								if(l_nTotalCount){
									l_sBtnText += "<font size='12'> - "+l_nCount+" Meals left</font>"
								}
								
								addOptionLine( mc_btnholder, l_sBtnText);
								
								if (l_xGoods._xMods.health >0 || l_xGoods._xMods.health <0){
									addOptionLine( mc_btnholder, "Health: "+((l_xGoods._xMods.health>0)?"+":"")+ l_xGoods._xMods.health ,null);
								}
								
								// 10x
								var l_nItemCount:int = 10;
								l_bDead = (G._nCurrentDollars<l_nCost*l_nItemCount);
								if(l_nTotalCount && l_nCount<l_nItemCount){
									l_bDead = true
								}
								l_sBtnText = l_xGoods._sName+' x'+l_nItemCount;
								
								l_sPrice = "-$"+l_nCost*l_nItemCount
								addOptionLine( mc_btnholder, l_sBtnText, {_sPrice: l_sPrice, _bDead:l_bDead, _sButtonFrame:l_sButtonFrame, _nCost:l_nCost*l_nItemCount, _funCallback:l_fCallback, _nItemCount:l_nItemCount, _xGoods:l_xGoods} );
								
								
								// 1x
								l_sPrice = "-$"+l_nCost
								l_sBtnText = l_xGoods._sName;
								l_bDead = (G._nCurrentDollars<l_nCost); 
								if(l_nTotalCount && !l_nCount){
									l_bDead = true
								}
								addOptionLine( mc_btnholder, l_sBtnText, {_sPrice: l_sPrice, _bDead:l_bDead,_sButtonFrame:l_sButtonFrame, _nCost:l_nCost, _funCallback:l_fCallback, _nItemCount:1, _xGoods:l_xGoods} );
								
								break;
							case 'weapon':
								//l_fCallback = buyWeapon;
								
								l_sBtnText = l_nCost+' - '+ l_xGoods._sName;
								
								break;
							case 'gas':
								l_fCallback = buyGas;
								
								var l_bIsFull:Boolean = G._nMaxGallons == G._nCurrentGallons
								var l_nGallonsLeft:int = G._nMaxGallons-Math.floor(G._nCurrentGallons); 
								
								l_sBtnText = "<font size='12'>Gasoline</font>"
								addOptionLine( mc_btnholder, l_sBtnText);
																
								l_bDead = (G._nCurrentDollars<l_nCost*l_nGallonsLeft || l_bIsFull);
								l_sBtnText =  'Fill Er Up!' ;
								l_sPrice = '$'+l_nCost*l_nGallonsLeft
								addOptionLine( mc_btnholder, l_sBtnText, {_sPrice: l_sPrice, _bDead:l_bDead, _sButtonFrame:l_sButtonFrame, _nCost:l_nCost*l_nGallonsLeft, _funCallback:buyGas, _nItemCount:l_nGallonsLeft} );

								l_bDead = (G._nCurrentDollars<l_nCost || l_bIsFull);
								l_sBtnText = '1 gallon'; 
								l_sPrice = '$'+l_nCost
								addOptionLine( mc_btnholder, l_sBtnText, {_sPrice: l_sPrice, _bDead:l_bDead, _sButtonFrame:l_sButtonFrame, _nCost:l_nCost, _funCallback:buyGas, _nItemCount:1} );
							break;
							
						}
						
					}
					_view.mc_options.mc_options_bg.height = _view.mc_options.mc_btnholder.height + 95; 
					
					
				break;
					
			}
			
			_view.mc_announcementoverlay.x = _view.mc_options.x = ( G.TOTAL_WIDTH - _view.mc_options.width )/2 
			_view.mc_announcementoverlay.y = _view.mc_options.y = ( G.TOTAL_HEIGHT - _view.mc_options.height)/2 
			_view.mc_announcementoverlay.height = _view.mc_options.height
			_view.mc_announcementoverlay.width = _view.mc_options.width
			
			
		}
		
		private function buyGas(p_xEvent:ModelEvent):void{
			var l_xData:Object = p_xEvent.data;
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_DATA,{_nDollarChange: -l_xData._nCost, _nGasChange:l_xData._nItemCount} ));
			refreshPopup();
		}
		
		private function buyFood(p_xEvent:ModelEvent):void{
			var l_xData:Object = p_xEvent.data;
			var l_xGoods:Goods = l_xData._xGoods
			if(l_xGoods._nTotalCount){
				l_xGoods._nCurrentCount -= l_xData._nItemCount
			}
			dispatchEvent(new ModelEvent(ModelEvent.ITEM_MOD_ALTER, {_xGoods:l_xGoods, _nCount:l_xData._nItemCount}))
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_DATA,{_nDollarChange: -l_xData._nCost} ));
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_DATA,{_nFoodChange:l_xData._nItemCount} ));
			refreshPopup();
		}
		
		private function buyCar(p_xEvent:ModelEvent):void{
			var l_xData:Object = p_xEvent.data
			var l_xCar:Car = l_xData._xGoods
			G._sCurrentCar = l_xCar._sSlug
			G._nMaxGallons = G._aCars[l_xCar._sSlug]._nGals
			G._nCurrentGallons = G._aCars[l_xCar._sSlug]._nGals
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_DATA,{_nDollarChange: -l_xData._nCost} ));
			refreshPopup();
		}
		
		private function buyItem(p_xEvent:ModelEvent):void{
			var l_xData:Object = p_xEvent.data;
			var l_xGoods:Goods = l_xData._xGoods
			if(l_xGoods._nTotalCount){
				l_xGoods._nCurrentCount -= l_xData._nItemCount
			}
			dispatchEvent(new ModelEvent(ModelEvent.ITEM_MOD_ALTER, {_xGoods:l_xGoods, _nCount:l_xData._nItemCount}))
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_DATA,{_nDollarChange: -l_xData._nCost} ));
			refreshPopup();
		}
		
		private function choiceChoose(p_xEvent:ModelEvent):void{
			var l_sNewScenerio:String = p_xEvent.data._sResult
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_SCENERIO,{_sSlug:l_sNewScenerio} ));
			
		}
		
		
		
		private function addBg(p_sColor:int, p_nAlpha:Number = 1):void{
			_view.mc_bg.visible = true;
			
			_view.mc_bg = clearMovieClip(_view.mc_bg);
			
			var s_bg:Sprite = new Sprite();
			_view.mc_bg.addChild(s_bg);
			_view.mc_bg.alpha = p_nAlpha;
			s_bg.x = s_bg.y = 0;
			var g:Graphics = s_bg.graphics; 
			g.beginFill(p_sColor);
			g.drawRect(0, 0, G.TOTAL_WIDTH , G.TOTAL_HEIGHT);
			
			
		}
		
		private function addBlackUnClickableBg():void{
			addBg(0x000000, 1);
		}
		
		private function addDarkUnClickableBg():void{
			addBg(0x000000, G.BGTRANS);
		}
		
		private function addClearUnClickableBg():void{
			addBg(0x000000, 0);
		}
		
		private function addClickableBg(p_fCallback:Function):void{
			addBg(0x000000, G.BGTRANS);
			_view.mc_clickablebg.addEventListener(MouseEvent.CLICK, p_fCallback,false,0,true);
			_view.mc_clickablebg.visible = true; 
			_view.mc_clickablebg.buttonMode = true	
		}
		
		private function makeCost(p_nCost:Number):Number{
			return (p_nCost);
			
		}
		
		
		
		private function popupSign(p_xEvent:ModelEvent):void{
			trace("popup sign")
			var l_sText:String = p_xEvent.data._sSignString;
			trace(l_sText)
			_view.mc_citysign.txt_cityname.text = l_sText; 
			_view.mc_citysign.visible = true;
			_view.visible = true;
			_view.mc_citysign.x = (G.TOTAL_WIDTH - _view.mc_citysign.width)/2; 
			_view.mc_citysign.y = (G.POPUP_CENTER_VERTICAL -  _view.mc_citysign.height)/2;
			
			createClickOverlay();
			_bSpaceClickVista=true
		}
		
		
		
		private function createClickOverlay():void{
			
			stage.addEventListener( KeyboardEvent.KEY_DOWN, clickedOverlay );
			_view.mc_clickoverlay.addEventListener(MouseEvent.CLICK, clickedOverlay,false,0,true);
			_view.mc_clickoverlay.visible = true;
			
		}
		
		private function addTextWithAttributes(p_aData:Array):String{
			var _sString:String = ""
			if (!p_aData)
				return _sString
			for each (var item:Object in p_aData){
				if (item._sKey == 'purify'){
					_sString += "+Food Purification<br>"
				}
				
				switch(item._sUnit){
					case 'stress':
						if (item._sChange == 'add'
							|| item._sKey == 'half-increase'
							|| item._sKey == 'max'){
							_sString += "+"+item._sVar+" Rage<br>"
						}
						if (item._sChange == 'sub'
							|| item._sKey == 'zero'){
							_sString += "-"+item._sVar+" Rage<br>"
						}
					break;
					case 'health':
						if (item._sChange == 'add'
							|| item._sKey == 'half-increase'
							|| item._sKey == 'max'){
							_sString += "+"+item._sVar+" Health<br>"
						}
						if (item._sChange == 'sub'){
							_sString += "-"+item._sVar+" Health<br>"
						}
					break;
					case 'dollars':
						if (item._sChange == 'add'){
							_sString += "+"+item._sVar+"$<br>"
						}
						if (item._sChange == 'sub'){
							_sString += "-"+item._sVar+"$<br>"
						}
					break;
					case 'food':
						if (item._sChange == 'add'){
							_sString += "+"+item._sVar+" Meals of Food<br>"
						}
						if (item._sChange == 'sub'){
							_sString += "-"+item._sVar+" Meals of Food<br>"
						}
					break;
					case 'strength':
						if (item._sChange == 'add'){
							_sString += "+"+item._sVar+" Strength<br>"
						}
						if (item._sChange == 'sub'){
							_sString += "-"+item._sVar+" Strength<br>"
						}
					break;
					case 'alert':
						if (item._sChange == 'add'){
							_sString += "+Alert "
						}
						if (item._sChange == 'sub'){
							_sString += "-Alert "
						}
					break;
					case 'location':
						var l_xDestination:Destination = G._aCities[item._sVar]
						if(item._sChange!='unknown')
							_sString += '+'+l_xDestination._sName+"<br>";
						else
							_sString += '+??? <br>';
					break;
				}
			} 
			
			if(_sString != ''){
				_sString = "<br><br>"+_sString
			}
			return _sString
		}
		
		
		private function stopClips():void{
			_view.mc_vistaholder.stop();
			_view.mc_hide.stop();
		}
		private function hideClips():void{
			resetKeyHandlers()
			_view.mc_hunt.visible = false;
			_view.mc_hide.visible = false;
			_view.mc_citysign.visible = false;
			_view.mc_clickoverlay.visible = false;
			_view.mc_vistaholder.visible = false;
			clearMovieClip(_view.mc_vistaholder)
			_view.mc_bg.visible = false; 
			_view.mc_text.visible = false; 
			_view.mc_clickablebg.visible = false;
			_view.mc_options.visible = false;
			
		}
	}
}