package com.uglypersonsface.views {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.objects.Car;
	
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.*;


	public class StatusView extends View {
		
		public function StatusView (p_xModel:Application, p_xAssetSwf:AssetSwf) {
			super(p_xModel, p_xAssetSwf, "mc_StatusView"); 
		}

		private var _bSpaceCloseStatus: Boolean = false

//--------------------------------------------------------------------------------------------------
//	HOOK

		override protected function buildViewHook () : void {
			_view.stop(); 
			_view.x = 0;
			_view.y = G.TOTAL_HEIGHT - G.STATUS_HEIGHT; 
			
			_view.mc_statusbar.visible = false; 
			_view.mc_instructionsbar.visible = false; 
			_view.mc_totalstatus.visible = false;
			
			
			_model.addEventListener(ModelEvent.STATUS_BTN_CLICK, displayTotalStatus, false, 0, true);
			_model.addEventListener(ModelEvent.CONTINUE_ROAD_VIEW, displayStatus, false, 0, true);
			_model.addEventListener(ModelEvent.DISPLAY_STATUS, displayStatus, false, 0, true);
			_model.addEventListener(ModelEvent.POPUP_TEXT, continueText, false, 0, true);
			_model.addEventListener(ModelEvent.POPUP_SIGN, continueText, false, 0, true);
			_model.addEventListener(ModelEvent.POPUP_VISTA, continueText, false, 0, true);
			_model.addEventListener(ModelEvent.INIT_TOWN, displayStatus, false, 0, true);
			_model.addEventListener(ModelEvent.ROAD_TIMER_HIT, roadTimerHit, false, 0, true);
			_model.addEventListener(ModelEvent.START_ROAD_VIEW, displayStatus, false, 0, true);
			_model.addEventListener(ModelEvent.CHANGE_DATA, displayStatus, false, 0, true);
			_model.addEventListener(ModelEvent.SHOW_MAP, chooseMapText, false, 0, true);
			
		//	displayStatus();
		//	displayInstructions('click to continue');     
		 
			stage.addEventListener( KeyboardEvent.KEY_DOWN, keyDownHandler );
			
			_view.mc_totalstatus.mc_bg_click.addEventListener(MouseEvent.CLICK, closeTotalStatus, false, 0, true);
			_view.mc_totalstatus.mc_x.addEventListener(MouseEvent.CLICK, closeTotalStatus, false, 0, true);
			_view.mc_totalstatus.mc_bg_click.buttonMode=true
			_view.mc_totalstatus.mc_bg.buttonMode=false
			_view.mc_totalstatus.mc_binyah.addEventListener(MouseEvent.CLICK, binyahClick, false, 0, true);
			
		}
		
		
//--------------------------------------------------------------------------------------------------
//	LISTENERS

		private function keyDownHandler(p_xEvent:KeyboardEvent):void{
			
			if (p_xEvent.keyCode==32 && _bSpaceCloseStatus) {
				_bSpaceCloseStatus=false
				closeTotalStatus();
			}
			
			
		}
	
		private function chooseMapText(p_xEvent:ModelEvent):void{
			displayInstructions('Choose a Destination');  
			
		}
		private function continueText(p_xEvent:ModelEvent):void{
			displayInstructions('Click or Space to Continue');
			
		}
		private function roadTimerHit(p_xEvent:ModelEvent):void{
			displayStatus(p_xEvent)
			
		}
		private function binyahClick(p_xEvent:MouseEvent):void{
			dispatchEvent(new ModelEvent(ModelEvent.STATUS_BINYAH_CLICK) )
		}
		
		private function closeTotalStatus(p_xEvent:MouseEvent=null):void{
			_view.mc_totalstatus.visible = false ;
			
			_bSpaceCloseStatus=false
		}
		
		private function displayTotalStatus(p_xEvent:ModelEvent):void{
			_view.mc_totalstatus.visible = true;
			
			_view.mc_totalstatus.txt_brochures.text = G._nCurrentBrochures+ ' / '+G._nTotalBrochures; 
			
			
			_view.mc_totalstatus.txt_days.text = G._nCurrentDay
			
			_view.mc_totalstatus.txt_stressbase.text = G.STRESS
			_view.mc_totalstatus.txt_ailmentmod.text = "+"+G.ailmentMod()
			_view.mc_totalstatus.txt_foodqualitymod.text = "+"+G.foodQualityMod()
			_view.mc_totalstatus.txt_foodperday.text = G.foodPerDay()+" meals";
			_view.mc_totalstatus.txt_daysoffood.text = G.daysOfFood() +" days";
			
			_view.mc_totalstatus.txt_carmod.text = "+"+G._aCars[G._sCurrentCar]._nStress
			_view.mc_totalstatus.txt_strength.text = G.STRENGTH
			_view.mc_totalstatus.txt_money.text = G._nCurrentDollars+"$";
			_view.mc_totalstatus.txt_health.text = G.HEALTH 
			_view.mc_totalstatus.txt_totalstress.text = G.calculatedStressNumber()
			
			_view.mc_totalstatus.txt_alert.text = G.alertStars()

			_view.mc_totalstatus.txt_gas.text = G.getTankStatus()
			
			_view.mc_totalstatus.txt_foodlbs.text = G._nCurrentFood+" meals";
			
			_view.mc_totalstatus.txt_foodquality.text = G.foodString()
		    
			_view.mc_totalstatus.txt_cities.text = G._nCurrentCitiesDestroyed +' / '+G._nTotalCities	
			
			var l_xCar:Car = G._aCars[G._sCurrentCar]
			_view.mc_totalstatus.txt_carname.text = l_xCar._sName
			_view.mc_totalstatus.txt_mpg.text = l_xCar._nMPG
			_view.mc_totalstatus.txt_mph.text = l_xCar._nMaxSpeed
			
			_view.mc_totalstatus.txt_char_1_name.text = G._aCarChars[0]._sName 
			_view.mc_totalstatus.txt_char_1_ailments.text = G.makeAilmentString(G._aCarChars[0])
			
			_view.mc_totalstatus.txt_char_2_name.text = G._aCarChars[1]._sName
			_view.mc_totalstatus.txt_char_2_ailments.text = G.makeAilmentString(G._aCarChars[1])
			
			_view.mc_totalstatus.txt_char_3_name.text = G._aCarChars[2]._sName
			_view.mc_totalstatus.txt_char_3_ailments.text = G.makeAilmentString(G._aCarChars[2])
			
			_view.mc_totalstatus.txt_char_4_name.text = G._aCarChars[3]._sName
			_view.mc_totalstatus.txt_char_4_ailments.text = G.makeAilmentString(G._aCarChars[3])
			
			_view.mc_totalstatus.txt_binyahstatus.text = G.getBinyahStatus()
			_view.mc_totalstatus.txt_binyah_ailments.text = G.makeAilmentString( G._xBinyah )
			
			var l_nTotalWidth:int = 100
			var l_nTotalHeight:int = 87
			
			
			displayStatus(p_xEvent)
			_bSpaceCloseStatus=true
			 
		}
		
		
		private function displayStatus(p_xEvent:ModelEvent = null):void{ 
			_view.mc_statusbar.visible = true;
			_view.mc_instructionsbar.visible = false;
			_view.mc_statusbar.txt_dollars.text = G._nCurrentDollars+"$";
			_view.mc_statusbar.txt_food.text = G._nCurrentFood+" meals";
			_view.mc_statusbar.txt_fuel.text = G.getTankStatus()
			_view.mc_statusbar.txt_stress.text = G.getStressStatus()
			//_view.mc_statusbar.txt_fuel.text = G._nCurrentGallons;
			_view.mc_statusbar.txt_health.text = G.HEALTH 
			
			var destination:String 
			if (int(G._nDistanceToDestination) == 0){
				destination = "Arrived at "+G._aCities[G._sCurrentDestination]._sName
			}else{
				destination = int(G._nDistanceToDestination)+" miles to "+G._aCities[G._sCurrentDestination]._sName
			}
			_view.mc_statusbar.txt_fulldestination.text = destination
			
			var timestamp:String = "Day "+String(G._nCurrentDay)+", "+String(G._sCurrentTimeString) 
			_view.mc_statusbar.txt_timestamp.text = timestamp
			
			if(G._nCurrentAlert > 0){
				var l_sAlert:String = ""
				for(var i:int = 0; i<G._nCurrentAlert;i++){
					l_sAlert += "*"
				}
				_view.mc_statusbar.txt_alert.text = l_sAlert
			}
			else{
				_view.mc_statusbar.txt_alert.text = "~"
				
			}
			
		} 
		
		
		
		private function displayInstructions(p_sText:String):void{
			
			_view.mc_statusbar.visible = false;
			_view.mc_instructionsbar.visible = true;
			_view.mc_instructionsbar.txt_instructions.text = p_sText;
			
		}
		
		
		
	}
}