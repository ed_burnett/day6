package com.uglypersonsface.views {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.views.View;
	import com.uglypersonsface.views.ButtonView;
	import flash.events.KeyboardEvent;
	import com.uglypersonsface.objects.Car;
	import flash.text.*;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;


	public class RoadView extends View {
		
		public var _xRoadTimer:Timer;
		private var _txt_mpg:TextField;
		
		public function RoadView (p_xModel:Application, p_xAssetSwf:AssetSwf) {
			super(p_xModel, p_xAssetSwf, "mc_RoadView");
		}


		private var _bSpacePullOver: Boolean = false
//--------------------------------------------------------------------------------------------------
//	HOOK

		override protected function buildViewHook () : void {
			
			_view.stop();
			
			_view.mc_carholder.stop();
			_view.mc_skyholder.stop();
			_view.mc_roadholder.stop();
			_view.mc_bgholder.stop();
			
			_view.mc_carholder.x = G.TOTAL_WIDTH;
			_view.mc_carholder.y = G.ROAD_CAR_HEIGHT;
			
			_view.mc_skyholder.x = G.TOTAL_WIDTH;
			_view.mc_skyholder.y = G.ROAD_SKY_HEIGHT;
			
			_view.mc_roadholder.x = G.TOTAL_WIDTH;
			_view.mc_roadholder.y = G.ROAD_ROAD_HEIGHT;
			
			_view.mc_bgholder.x = G.TOTAL_WIDTH;
			_view.mc_bgholder.y = G.ROAD_BG_HEIGHT;
			
			_view.mc_mpgholder.x = G.MPG_HOLDER_X; 
			_view.mc_mpgholder.y = G.MPG_HOLDER_Y;
			
			_view.mc_pulloverholder.x = G.PULLOVER_HOLDER_X 
			_view.mc_pulloverholder.y = G.PULLOVER_HOLDER_Y
			
			_xRoadTimer = new Timer(G.TICK);
			
			_model.addEventListener(ModelEvent.START_ROAD_VIEW, startRoadView, false, 0, true);
			_model.addEventListener(ModelEvent.END_ROAD_VIEW, endRoadView, false, 0, true);
			_model.addEventListener(ModelEvent.FREEZE_ROAD_VIEW, freezeRoadView, false, 0, true);
			_model.addEventListener(ModelEvent.CONTINUE_ROAD_VIEW, continueRoadView, false, 0, true);
			_model.addEventListener(ModelEvent.TMP_FREEZE_ROAD_VIEW, tmpFreezeRoadView, false, 0, true);
			_model.addEventListener(ModelEvent.TMP_CONTINUE_ROAD_VIEW, tmpContinueRoadView, false, 0, true);
			_model.addEventListener(ModelEvent.MPG_HIT, updateMpg, false, 0, true);
			
			stage.addEventListener( KeyboardEvent.KEY_DOWN, keyDownHandler );
			_xRoadTimer.addEventListener(TimerEvent.TIMER, roadTimerHit,false,0,true);
		}


//--------------------------------------------------------------------------------------------------
//	LISTENERS
		
		
		private function keyDownHandler(p_xEvent:KeyboardEvent):void{
			if (p_xEvent.keyCode==32 && _bSpacePullOver) {
				_bSpacePullOver=false
				G._bKeyOverride=true
				pulloverHit();
			}
		}
		private function startRoadView(p_xEvent:ModelEvent):void{
			_view.visable = true;
			constructView();
			_xRoadTimer.start();
			continueView();
			Events.checkRoadStartEvent()
		}
		
		private function endRoadView(p_xEvent:ModelEvent):void{
			_bSpacePullOver=false
			_view.visable = false;
			clearView(); 
		}
		
		private function freezeRoadView(p_xEvent:ModelEvent):void{
			_bSpacePullOver=false
			freezeView();
			_xRoadTimer.stop();
		}
		
		private function continueRoadView(p_xEvent:ModelEvent):void{
			continueView();
			_xRoadTimer.start();
		}
		private function tmpFreezeRoadView(p_xEvent:ModelEvent):void{
			_bSpacePullOver=false
			freezeView();
		}
		
		private function tmpContinueRoadView(p_xEvent:ModelEvent):void{
			continueView();
		}
		
		private function roadTimerHit(p_xEvent:TimerEvent):void{
			dispatchEvent(new ModelEvent(ModelEvent.ROAD_TIMER_HIT ));
		}
		
		private function mpgHit(p_xEvent:ModelEvent):void{
			dispatchEvent(new ModelEvent(ModelEvent.MPG_HIT , p_xEvent.data));
		}
		private function updateMpg(p_xEvent:ModelEvent):void{
			showMpgInt();
		}
		private function pulloverHit(p_xEvent:ModelEvent=null):void{
			dispatchEvent(new ModelEvent(ModelEvent.PULLOVER_HIT ));
		}
		

//--------------------------------------------------------------------------------------------------
//	HELPERS
		
		private function constructView():void{
			
			_view.mc_carholder.gotoAndStop('f_'+G._sCurrentCar); 
			_view.mc_roadholder.gotoAndStop(G._sCurrentRoad);
			_view.mc_bgholder.gotoAndStop('f_'+G._aRegions[G._sCurrentRegion]._sBg);
			_view.mc_skyholder.gotoAndStop(G._sCurrentSky);
			
			addMpgButtons(); 
			
			addPulloverButton();
		}
		
		
		private function clearView():void{
			_view.mc_carholder.gotoAndStop(G.BLANK);
			_view.mc_roadholder.gotoAndStop(G.BLANK); 
			_view.mc_bgholder.gotoAndStop(G.BLANK);
			_view.mc_skyholder.gotoAndStop(G.BLANK);
		}
		
		private function freezeView():void{
			_view.mc_roadholder.road.stop();
			_view.mc_carholder.car.stop();
			_view.mc_bgholder.bg.stop();
			_view.mc_skyholder.sky.stop();
			_view.mc_roadholder.road.stop();
		}
		
		private function continueView():void{
			G._sMode=G.MODE_ROAD
			_view.mc_carholder.gotoAndStop('f_'+G._sCurrentCar); 
			_view.mc_roadholder.gotoAndStop(G._sCurrentRoad);
			_view.mc_bgholder.gotoAndStop('f_'+G._aRegions[G._sCurrentRegion]._sBg);
			_view.mc_skyholder.gotoAndStop(G._sCurrentSky);
			_view.mc_carholder.car.play();
			_view.mc_roadholder.road.play();
			_view.mc_bgholder.bg.play();
			_view.mc_skyholder.sky.play();
			
			_bSpacePullOver=true
		}
		
		private function addPulloverButton():void{
			
			clearMovieClip(_view.mc_pulloverholder);
			
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf,'pullover');
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK, {} );
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, pulloverHit, false, 0, true);
			l_xButton.setCallback(l_e);
			_view.mc_pulloverholder.addChild(l_xButton);
			
		}
		
		private function addMpgButtons():void{  
			//clearMovieClip(_view.mc_mpgholder); 
			
			var l_xButton:ButtonView = new ButtonView(G._application, G._assetSwf,'arrow_up');
			var l_e:ModelEvent = new ModelEvent(ModelEvent.BUTTON_CLICK, {_nCount:5} );
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, mpgHit, false, 0, true);
			l_xButton.setCallback(l_e);
			l_xButton.y= -5;
			l_xButton.x= 120;  
			_view.mc_mpgholder.addChild(l_xButton);
			
			l_xButton = new ButtonView(G._application, G._assetSwf,'arrow_down');
			l_e = new ModelEvent(ModelEvent.BUTTON_CLICK, {_nCount:-5} );
			l_xButton.addEventListener(ModelEvent.BUTTON_CLICK, mpgHit, false, 0, true);
			l_xButton.setCallback(l_e);
			l_xButton.y= -5;
			l_xButton.x= 23; 
			_view.mc_mpgholder.addChild(l_xButton);
			
			
			showMpgInt();
		}
		private function showMpgInt():void{
			_view.mc_mpgholder.txt_mpg.text = G._nCurrentMPH.toString()+" mph";
		}
		
		
	}
}