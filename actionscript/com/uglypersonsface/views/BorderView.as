package com.uglypersonsface.views {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.models.Model;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.views.View;
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	


	public class BorderView extends View {
		

		public function BorderView (p_xModel:Application, p_xAssetSwf:AssetSwf) {
			super(p_xModel, p_xAssetSwf, "mc_BorderView");
		}


//--------------------------------------------------------------------------------------------------
//	HOOK

		override protected function buildViewHook () : void {
			
			_view.stop();
			
			_view.mc_border.graphics.lineStyle( 1, 0xaaaaaa );
			_view.mc_border.graphics.drawRect( 0, 0, G.TOTAL_WIDTH, G.TOTAL_HEIGHT );
			_view.mc_border.graphics.endFill();
		
		}


//--------------------------------------------------------------------------------------------------
//	HELPERS
		
		
		
	}
}