package com.uglypersonsface.views {
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.models.Model;
	import com.uglypersonsface.models.ModelEvent;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.views.View;
	import com.uglypersonsface.globals.G;
	import com.uglypersonsface.globals.Events;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	


	public class IntroView extends View {
		

		public function IntroView (p_xModel:Application, p_xAssetSwf:AssetSwf) {
			super(p_xModel, p_xAssetSwf, "mc_IntroView"); 
		}


//--------------------------------------------------------------------------------------------------
//	HOOK

		override protected function buildViewHook () : void {
			
			_view.stop();
			
			_model.addEventListener(ModelEvent.START_INTRO_VIEW, startIntroView, false, 0, true);
			_model.addEventListener(ModelEvent.END_INTRO_VIEW, endIntroView, false, 0, true);
			
		
		}


//--------------------------------------------------------------------------------------------------
//	LISTENERS
		
		private function endIntroView(p_xEvent:ModelEvent):void{ 
			trace('endIntroView')
 			_view.car_screen.btn_button1.removeEventListener(MouseEvent.CLICK, button1click);
 			_view.car_screen.btn_button2.removeEventListener(MouseEvent.CLICK, button2click);
			
			_view.visible = false; 
		}
		 
		private function startIntroView(p_xEvent:ModelEvent):void{
			trace('startIntroView')
			_view.visible = true;
			
 			_view.intro_screen.buttonMode = true;
 			_view.mid_screen.buttonMode = true;
 			
 			_view.car_screen.btn_button1.txt_button.selectable = false; 
 			_view.car_screen.btn_button1.txt_button.text  = 'Chevy Suburban'; 
 			_view.car_screen.btn_button1.buttonMode = true;
 			_view.car_screen.btn_button1.mouseChildren  = false;
 			_view.car_screen.btn_button1.addEventListener(MouseEvent.MOUSE_OVER,overr);
			_view.car_screen.btn_button1.addEventListener(MouseEvent.MOUSE_OUT,outt);
 			
 			_view.car_screen.btn_button2.txt_button.selectable = false;
 			_view.car_screen.btn_button2.txt_button.text  = 'Reliant Robin';
 			_view.car_screen.btn_button2.buttonMode = true;  
 			_view.car_screen.btn_button2.mouseChildren  = false; 
 			_view.car_screen.btn_button2.addEventListener(MouseEvent.MOUSE_OVER,overr);
			_view.car_screen.btn_button2.addEventListener(MouseEvent.MOUSE_OUT,outt);
 			
 			_view.car_screen.btn_button3.txt_button.selectable = false;
 			_view.car_screen.btn_button3.txt_button.text  = 'Toyota Prius';
 			_view.car_screen.btn_button3.buttonMode = true;
 			_view.car_screen.btn_button3.mouseChildren  = false; 
 			_view.car_screen.btn_button3.addEventListener(MouseEvent.MOUSE_OVER,overr);
			_view.car_screen.btn_button3.addEventListener(MouseEvent.MOUSE_OUT,outt);
 			
 			_view.car_screen.btn_button1.addEventListener(MouseEvent.CLICK, button1click, false,0,true);
 			_view.car_screen.btn_button2.addEventListener(MouseEvent.CLICK, button2click, false,0,true);
 			_view.car_screen.btn_button3.addEventListener(MouseEvent.CLICK, button3click, false,0,true); 
 			_view.car_screen.btn_button1.gotoAndStop(2);
 			_view.car_screen.btn_button2.gotoAndStop(2);
 			_view.car_screen.btn_button3.gotoAndStop(2);
 			
 			_view.intro_screen.addEventListener(MouseEvent.CLICK, introScreenClick, false,0,true);
 			_view.mid_screen.addEventListener(MouseEvent.CLICK, midScreenClick, false,0,true);

			
		}
		
		private function introScreenClick(p_xEvent:Event):void{  
			trace('introScreenClick')
			_view.intro_screen.removeEventListener(MouseEvent.CLICK,introScreenClick);
			_view.intro_screen.visible = false;
		}
		
		private function midScreenClick(p_xEvent:Event):void{
			trace('midScreenClick')
			_view.mid_screen.removeEventListener(MouseEvent.CLICK,midScreenClick);
			_view.mid_screen.visible = false; 
		}
		
		private function button1click(p_xEvent:Event):void{
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_DATA,{_sNewCar: G.CAR_SUV} ));
			G._nCurrentGallons = G._aCars[G.CAR_SUV]._nGals 
			G._nMaxGallons = G._aCars[G.CAR_SUV]._nGals
			dispatchEvent(new ModelEvent(ModelEvent.INPUT_ACCEPT ));
			
		}  
		private function button2click(p_xEvent:Event):void{
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_DATA,{_sNewCar: G.CAR_3WHEEL} ));
			G._nCurrentGallons = G._aCars[G.CAR_3WHEEL]._nGals
			G._nMaxGallons = G._aCars[G.CAR_3WHEEL]._nGals
			dispatchEvent(new ModelEvent(ModelEvent.INPUT_ACCEPT ));
		}
		private function button3click(p_xEvent:Event):void{
			dispatchEvent(new ModelEvent(ModelEvent.CHANGE_DATA,{_sNewCar: G.CAR_PRIUS} ));
			G._nCurrentGallons = G._aCars[G.CAR_PRIUS]._nGals
			G._nMaxGallons = G._aCars[G.CAR_PRIUS]._nGals
			dispatchEvent(new ModelEvent(ModelEvent.INPUT_ACCEPT ));
		}
		private function overr(e:MouseEvent):void {
			e.currentTarget.gotoAndStop(1);
		}
		private function outt(e:MouseEvent):void {
			e.currentTarget.gotoAndStop(2);
		}

//--------------------------------------------------------------------------------------------------
//	HELPERS
		
		
		
	}
}