package com.uglypersonsface.globals{
	import com.uglypersonsface.Controller;
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.models.*;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.objects.*;
	
	
	public class Events {
		
		
        // Scenerio Specs
        
        public static const MAX_SPEED:int = 70;
        public static const SPEED_INTERVAL:int = 3;
        public static const SPEED_CHANCE:int = 10;
        public static var SPEED_COUNT:int = 0;
        
        public static var BINYAH_HUNGER:int = 0;
        public static var BINYAH_COUNT:int = 0;
        public static var BINYAH_CHANCE:int = 5;
        public static const BINYAH_INTERVAL:int = 10;
        
        public static var CHANCE_COUNT:int = 0;        
        public static const CHANCE_INTERVAL:int = 1;
        
        public static const GOOD_CHANCE:int = 3
        public static const BINYAHROAD_CHANCE:int = 3
        public static const CHAR_CHANCE:int = 3
        public static const ETC_CHANCE:int = 3
        
        public static const COORDS_INTERVAL:int = 3;
        public static var COORDS_COUNT:int = 0;
        
        public static const STRESS_CHANCE:int = 100;
        
        
        // Applications
        public static var _application:Application;
        public static var _assetSwf:AssetSwf;
        public static var _controller:Controller;
		
		// In order:
		// Destination?		*
		// New Region?		COORDS_INTERVAL
		// Speeding?		SPEED_INTERVAL
		// Binyah Hungry?	BINYAH_INTERVAL
		// Chance Event?	CHANCE_INTERVAL
		public static function detectRoadEvent():void{
			var l_xS:Scenario;
			if(G._nCurrentHour == 11 && G._nCurrentMin < 1 && G._nCurrentDay == 1){
				return
			}
			if(!G._bRoadInstrctions){
				_application.executeScenerio(G._aScenarios['instruction-1']);
				G._bRoadInstrctions = true
			}
			if( !G._nDistanceToDestination ){
				_application.loadArrivial(G._aCities[G._sCurrentDestination]);
				return;
			}
			l_xS = checkCoordsScenerio()
			if (l_xS) {
				_application.executeScenerio(l_xS);
				return;
			}
			l_xS = checkSpeedScenerio()
			if (l_xS) {
				_application.executeScenerio(l_xS);
				return;
			}
			l_xS = checkBinyahHungerScenerio()
			if (l_xS) {
				_application.executeScenerio(l_xS);
				return;
			}
			l_xS = checkChanceScenerio()
			if (l_xS) {
				_application.executeScenerio(l_xS);
				return;
			}
		}
		
		
		
		// Every BINYAH_INTERVAL ticks, roll against BINYAH_CHANCE
		private static function checkBinyahHungerScenerio():Scenario{
			if((!G._xBinyah._bDead) && Events.BINYAH_INTERVAL == ++Events.BINYAH_COUNT){
				Events.BINYAH_COUNT = 0;
				if(Events.BINYAH_CHANCE > G.roll()){
					Events.BINYAH_HUNGER++
					return G._aScenarios['binyah-100']
				}
			}
			return null;
		}
		
		// Order of checks every CHANCE_INTERVAL:  GOOD, BINYAHROAD, CHAR, ETC
		private static function checkChanceScenerio():Scenario{
			if(Events.CHANCE_INTERVAL == ++Events.CHANCE_COUNT){
				Events.CHANCE_COUNT = 0;
				var l_nRoll:int = 0;
				if(Events.GOOD_CHANCE > G.roll()){
					trace("good")
					l_nRoll = G._aGoodIndexScenario[G.roll(G._aGoodIndexScenario.length)]
					return G._aScenarios['good-'+l_nRoll]
				}
				if((!G._xBinyah._bDead)&&Events.BINYAHROAD_CHANCE > G.roll()){
					trace("binyah")
					l_nRoll = G._aBinyahRoadIndexScenario[G.roll(G._aBinyahRoadIndexScenario.length)]
					return G._aScenarios['binyahroad-'+l_nRoll]
				}
				// TODO Fix for chars. This doesnt seem to make sense.
				if(Events.CHAR_CHANCE > G.roll() && !!G.randomChar()){
					trace("char")
					l_nRoll = G._aCharIndexScenario[G.roll(G._aCharIndexScenario.length)]
					return G._aScenarios['char-'+l_nRoll]
				}
				if(Events.ETC_CHANCE > G.roll()){
					trace("etc")
					l_nRoll = G._aEtcIndexScenario[G.roll(G._aEtcIndexScenario.length)]
					return G._aScenarios['etc-'+l_nRoll]
				}
				if(Events.STRESS_CHANCE > G.roll()){
					if(G.calculatedStressNumber() > 90){	
						G.STRESS = 0
						trace("stress")
						l_nRoll = G._aStressIndexScenario[G.roll(G._aStressIndexScenario.length)]
						return G._aScenarios['stress-'+l_nRoll]
					}
				}
			}
			return null;
		}
		
		private static function checkOutsideCurrentCoords():Boolean{
			var l_xScenario:Scenario;
			var l_xTmpScenario:Scenario;
			var l_sKey:String 
			
			for (l_sKey in G._aCoordsScenarios){
				l_xTmpScenario = G._aCoordsScenarios[l_sKey]
				if(l_xTmpScenario._sRegion == G._sCurrentRegion){
					l_xScenario = l_xTmpScenario
				}
			}
			
			if(
				G._nX < l_xScenario._nXmin ||
				G._nX > l_xScenario._nXmax ||
				G._nY < l_xScenario._nYmin ||
				G._nY > l_xScenario._nYmax
			){
				return true;
			  }else{
			  	return false;
			  }
		}
		
		
		// Every COORDS_INTERVAL, check x/y against all 
		private static function checkCoordsScenerio():Scenario{

			if(Events.COORDS_INTERVAL == ++Events.COORDS_COUNT){
				Events.COORDS_COUNT = 0;
				
				// Hack for first switch
				if(!G._aCities['atlanta']._bOnMap){
					return null;
				}
				
				if(!checkOutsideCurrentCoords()){
					return null;
				}
				
				Events.COORDS_COUNT = 0
				var l_xScenario:Scenario;
				var l_sKey:String 
				for (l_sKey in G._aCoordsScenarios){
					l_xScenario = G._aCoordsScenarios[l_sKey]
					
					if(
						G._nX >= l_xScenario._nXmin &&
						G._nX <= l_xScenario._nXmax &&
						G._nY >= l_xScenario._nYmin &&
						G._nY <= l_xScenario._nYmax
					){
						return l_xScenario
					}
				}
			}
			return null;
		}
		
		// Roll on difference between MPG and MAX_SPEED. Min of SPEED_CHANCE
		private static function checkSpeedScenerio():Scenario{
			if(Events.SPEED_INTERVAL == ++Events.SPEED_COUNT){
				Events.SPEED_COUNT = 0;
				if(Events.MAX_SPEED < G._nCurrentMPH){
					var l_nDifference:int = G._nCurrentMPH - Events.MAX_SPEED  
					if(l_nDifference > Events.SPEED_CHANCE) 
						l_nDifference = Events.SPEED_CHANCE
					if (G.roll() < l_nDifference){
						return G._aScenarios ['speed-1']
					} 
				}
			}
			return null;
		}
		
		
        public static const ROAD_START_BINYAH_CHANCE:int = 50;
        public static const ROAD_START_CHANCE:int = 10;
        
        public static var ROAD_START_CHANCE_INIT:Boolean = true;
        
        
		public static function checkRoadStartEvent():void{
			if(ROAD_START_CHANCE_INIT){
				ROAD_START_CHANCE_INIT = false
				return
			}
			var l_nRoll:int = 0;
			if(BINYAH_HUNGER > 2 && G._sCurrentCar != '3wheel'){
				if(ROAD_START_BINYAH_CHANCE > G.roll()){
					BINYAH_HUNGER = 0
					G._sCurrentCar = '3wheel'
					trace("roadstartbinyah")
					l_nRoll = G._aRoadStartBinyahIndexScenario[G.roll(G._aRoadStartBinyahIndexScenario.length)]
					return _application.executeScenerio(G._aScenarios['roadstartbinyah-'+l_nRoll])
				}
			}
			
			if(ROAD_START_CHANCE > G.roll()){
				trace("roadstart")
				l_nRoll = G._aRoadStartIndexScenario[G.roll(G._aRoadStartIndexScenario.length)]
				return _application.executeScenerio(G._aScenarios['roadstart-'+l_nRoll])
			}
			
		}
		
		
		// Every step,
		// Advance 10min,
		// Move Car,
		// Update View,
		// Check Road events
		public static function advanceRoadTime(p_xEvent:ModelEvent):void{			
			if(G._sMode == G.MODE_ROAD){
				G._nCurrentMin ++;
				G.fixTime();
				_controller.moveCar();
				_application.roadTimerHit();
				checkDailyFeed()
				detectRoadEvent();
			}
		}
		
		
		//////// FEED STUFF
		
		
		public static function checkDailyFeed():void{
			if(G._nCurrentMin==0 && G._nCurrentHour==0){
				Events.feed()
			}
		}
		
        public static function feed():void{
        	
			var l_nFoodPerDay:int = G.foodPerDay()
			if(l_nFoodPerDay < G._nCurrentFood){
				// everyone gets fed
				G._nCurrentFood -= l_nFoodPerDay
				Data.removeRandomAilment(G.randomChar())
				
			}else if(G._nCurrentFood != 0){
				// out of food
				_application.addAnnouncement("You have run out of food!");
				Data.addStarveAilment(G.randomChar())
				G._nCurrentFood = 0 
			}else{
				// starve someone
				G._nCurrentFood = 0
				Data.addStarveAilment(G.randomChar())
				Data.addStarveAilment(G.randomChar())
			}
        	
        }
        
		
		
	}
	
	
}