package com.uglypersonsface.globals{
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.objects.*;
	import com.uglypersonsface.models.*;
	import com.uglypersonsface.Controller;
	
	
	public class Data {
		

        // Applications
        public static var _application:Application;
        public static var _assetSwf:AssetSwf;
        public static var _controller:Controller;
		

		// Etc
		
		public static function changeData(p_aData:Array):void{
			
			// unit change var
			var l_xData:Object;
			
			for each (l_xData in p_aData){
				if(l_xData._sKey){
					switch(l_xData._sKey){
						case 'fine':
							l_xData._sVar = G.calcFine();
						break;
						case 'binyah':
							l_xData._sVar = G.calcBinyah();
						break;
						
						case 'region':break;
						
						case 'purify':
							G._nCurrentFoodQuality = 100
						break;
						case 'char':break;
						
						default:
							l_xData._sVar = Number(l_xData._sVar);
						break;
					}
				}
				
				switch(l_xData._sUnit){
					case 'foodquality':
						switch(l_xData._sChange){
							case 'add':
								
								//G._nCurrentFoodQuality = Number(l_xData._sVar * /(G._nCurrentFood);
							break;
							case 'sub':
								//G._nCurrentFoodQuality -= int(l_xData._sVar);
								//if(G._nCurrentFoodQuality<0)G._nCurrentFoodQuality=0
							break;
						}
					break;
					case 'location':
						var l_xDestination:Destination = G._aCities[l_xData._sVar]						
						l_xDestination._bOnMap = true
						if(l_xData._sChange!='unknown')
							l_xDestination._bNameKnown = true
					break;
					case 'time':
						switch(l_xData._sChange){
							case 'add':
								G.fixTime();
								G._nCurrentMin += int(l_xData._sVar);
							break;
						}
					break;
					case 'alert':
						switch(l_xData._sChange){
							case 'add':
								G._nCurrentAlert += int(l_xData._sVar);
							break;
						}
					break;
					 
					case 'ailment':
						switch (l_xData._sChange){
							case 'add':
								var l_xChar:Passenger = G.randomChar()
								if (l_xChar)
									_application.dispatchEvent(new ModelEvent(ModelEvent.PICK_AILMENT,{_xChar:l_xChar,_aAilments:l_xChar._aAilments,_sType:l_xData._sKey,_sName:l_xChar._sName} )); 		
							break;
						}
					break;
					
					case 'strength':
						switch(l_xData._sChange){
							case 'add':
								G.STRENGTH += int(l_xData._sVar);
							break;
							case 'sub':
								G.STRENGTH -= int(l_xData._sVar);
								if(G.STRENGTH<0)G.STRENGTH=0
							break;
						}	
					break;
					case 'stress':
						if(l_xData._sKey){
							switch(l_xData._sKey){
								case 'zero':
									G.STRESS = 0;
								break;
							}
						}else{
							switch(l_xData._sChange){
							case 'add':
								G.STRESS += int(l_xData._sVar);
							break;
							case 'sub':
								G.STRESS -= int(l_xData._sVar);
								if(G.STRESS<0)G.STRESS=0
							break;
							}
						}
					break;
					
					case 'region':
						G._sCurrentRegion = l_xData._sVar;
					break;
					case 'dollars': 
						switch(l_xData._sChange){
							case 'sub':
								G._nCurrentDollars -= int(l_xData._sVar);
							break;
							case 'add':
								G._nCurrentDollars += int(l_xData._sVar);
							break;
						}
					break;
					case 'food': 
						switch(l_xData._sChange){
							case 'sub':
								G._nCurrentFood -= int(l_xData._sVar);
							break;
							case 'add':
								G._nCurrentFood += int(l_xData._sVar);
							break;
						}
					break;
					case 'binyah': 
						switch(l_xData._sChange){
							case 'reset':
								Events.BINYAH_HUNGER = 0;
							break;
						}
					break;
				}
			}
		}
		
		
        // CHAR STUFF
        
        public static function killChar(p_xChar:Passenger):void{
			_application.addAnnouncement(p_xChar._sName+' has died'); 
			p_xChar._bDead=true
        }
		
		
		public static function removeAilment(p_xPassenger:Passenger,p_sType:String):void{
			
			var l_aPassangerAilments:Object = p_xPassenger._aAilments
			var l_aTypeAilmentScenerioList:Array = G._aAilments[p_sType]
		
			var l_aScramble:Array = new Array()
			if(l_aTypeAilmentScenerioList){
				for(var i:int=0; i<l_aTypeAilmentScenerioList.length;i++){
					if(l_aTypeAilmentScenerioList[i])
						l_aScramble.push(i)
				}
				var l_nRoll:int
				while(l_aScramble.length){
					l_nRoll = G.roll(l_aScramble.length)
					i = l_aScramble[l_nRoll]
					l_aScramble.splice(l_nRoll,1)
					if( l_aPassangerAilments[p_sType+i]){
						removeAilmentFromChar(l_aTypeAilmentScenerioList[p_sType+i],p_sType+i,p_xPassenger);
						break;
					}
				}
			}
		}
		
		public static function pickAilmentFromCall(p_xEvent:ModelEvent):void{
			var l_aTmp:Array = G._aAilments
			pickAilment(p_xEvent.data._xChar,p_xEvent.data._sType)
		}
		
		public static function pickAilment(p_xPassenger:Passenger,p_sType:String):void{
			if(!p_xPassenger) return;
			trace("pickailment: "+p_sType)
			
			var l_aTypeAilmentScenerioList:Array = G._aAilments[p_sType]
			var l_aAilments:Object = p_xPassenger._aAilments
			var l_aScramble:Array = new Array()
			var l_sName:String = p_xPassenger._sName
			
			for(var i:int=0; i<l_aTypeAilmentScenerioList.length;i++){
				if(l_aTypeAilmentScenerioList[i])
					l_aScramble.push(i)
			}
 
			var indexOfAilment:int
			var targetScenerio:Scenario
			var l_nRoll:int
			// Cuts out all types of ailments in temp array one by one in order to skip dupes.
			while(l_aScramble.length){
				l_nRoll = G.roll(l_aScramble.length)
				indexOfAilment = l_aScramble[l_nRoll]
				if(! l_aAilments[p_sType+indexOfAilment]){
					targetScenerio = l_aTypeAilmentScenerioList[indexOfAilment]
					return addAilmentToChar(targetScenerio,p_sType+indexOfAilment,p_xPassenger);
				}
				l_aScramble.splice(l_nRoll,1)
			}
			//In case theres a dupe just choose 1
			
			for(i=0; i<l_aTypeAilmentScenerioList.length;i++){
				if(l_aTypeAilmentScenerioList[i])
					l_aScramble.push(i)
			}
			
			l_nRoll = G.roll(l_aScramble.length)
			indexOfAilment = l_aScramble[l_nRoll]
			targetScenerio = l_aTypeAilmentScenerioList[indexOfAilment]
			return addAilmentToChar(targetScenerio,p_sType+indexOfAilment,p_xPassenger);
			
			
		}
		public static function removeAilmentFromChar(p_xScenario:Scenario,p_sIndex:String,p_xPassenger:Passenger):void{
			var ailmentString:String = p_xScenario._aResultData[0]._sVar
			p_xPassenger.removeAilment(p_sIndex)
			_application.addAnnouncement(p_xPassenger._sName+' no longer has '+ailmentString);
			checkPassengerHealth(p_xPassenger)
		}
		
		public static function addAilmentToChar(p_xScenario:Scenario,p_sIndex:String,p_xPassenger:Passenger):void{
			var ailmentString:String = p_xScenario._aResultDataOrig[0]._sVar
			p_xPassenger.addAilment(ailmentString,p_sIndex)
			_application.addAnnouncement(p_xScenario._sResultSpeak.replace(/%name/,p_xPassenger._sName));
			checkPassengerHealth(p_xPassenger)
		}
		
		public static function checkPassengerHealth(p_xPassenger:Passenger):Boolean{
			if(p_xPassenger._nHealth > p_xPassenger._nMaxHealth)
				p_xPassenger._nHealth = p_xPassenger._nMaxHealth
			if(p_xPassenger._nHealth<=0){
				killChar(p_xPassenger)
				return true
			}else{
				return false
			}
		}
		
		public static function removeRandomAilment(p_xPassenger:Passenger):void{
			if(!p_xPassenger)
				return 
			var key:String;
			var ailmentArray:Array = new Array
			for (key in p_xPassenger._aAilments){
				ailmentArray.push(key)
			}
			if(!ailmentArray.length) return
			var l_sAilmentKey:String =  ailmentArray[G.roll(ailmentArray.length)]
			var l_sAilmentString:String = p_xPassenger._aAilments[l_sAilmentKey]
			p_xPassenger.removeAilment(l_sAilmentKey)
			_application.addAnnouncement(p_xPassenger._sName+' no longer has '+l_sAilmentString);
			checkPassengerHealth(p_xPassenger)
		}
		
		
		
        public static function addStarveAilment(p_xChar:Passenger):void{
			Data.pickAilment(p_xChar,'hunger');
        }
        public static function removeStarveAilment(p_xChar:Passenger):void{
			Data.removeAilment(p_xChar,'hunger');
        }
        
		
	}
}
		