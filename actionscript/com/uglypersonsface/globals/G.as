package com.uglypersonsface.globals{
	import com.uglypersonsface.assets.AssetSwf;
	import com.uglypersonsface.models.application.Application;
	import com.uglypersonsface.objects.Destination;
	import com.uglypersonsface.objects.Passenger;
	
	
	public class G {
		
		public static const TICK:int=1000
		
		// Instructions
		public static var _bRoadInstrctions:Boolean = false
		public static var _bTownInstrctions:Boolean = false
		public static var _bShopInstrctions:Boolean = false
		
		
        //Starting Values
        public static const START_DESTINATION:String = DEST_START;
        public static const START_ROAD:String = ROAD_STREET;
        public static const START_SKY:String = SKY_CLOUDS;
        public static const START_X:Number = 680;
        public static const START_Y:Number = 365; 
        public static const START_MPH:int = 50;  
        public static const START_HOUR:int = 11;
        public static const START_MIN:int = 0;
        public static const START_TIMESTRING:String = '11:00am';
        public static const START_ISDAY:Boolean = true;
        public static const START_DOLLARS:Number = 500;
        public static const START_FOOD:Number = 20;
        public static const START_GALLONS:Number = 95; 
        public static const BASE_STRENGTH:int = 100; 
        public static const START_REGION:String = REGION_SOUTHEAST_BEACH;
        
        
		// Design Specs 
		public static const TOTAL_HEIGHT:int = 600;
		public static const TOTAL_WIDTH:int = 800;  
		 
		public static const ROAD_BG_HEIGHT:int = 400;
		public static const ROAD_SKY_HEIGHT:int = -35; 
		public static const ROAD_CAR_HEIGHT:int = 450;
		public static const ROAD_ROAD_HEIGHT:int = 400;
		public static const MPG_HOLDER_X:int = 300; 
		public static const MPG_HOLDER_Y:int = 530;
		public static const PULLOVER_HOLDER_X:int = 670; 
		public static const PULLOVER_HOLDER_Y:int = 450; 
		
        public static const STATUS_HEIGHT:int = 60;
        
        // Popup variables
        public static const POPUP_MAX_WIDTH:int = 500;
        public static const POPUP_CENTER_VERTICAL:int = 200;
        public static const OPTIONS_MAX_WIDTH:int = 450;
        
        
        // Button Widths
        public static const BUTTON_WIDTHS:Array = new Array();
        public static const BUTTON_HEIGHTS:Array = new Array();
        	BUTTON_WIDTHS['square']= 45;
        	BUTTON_HEIGHTS['square']= 45;
        	
        
		
		// etc
		public static const BLANK:String = 'f_blank';
		public static const BGTRANS:Number = .4
        
        //modes
        public static const MODE_INTRO:String = "MODE_INTRO";
        public static const MODE_ROAD:String = "MODE_ROAD";
        public static const MODE_ARRIVIAL:String = "MODE_ARRIVIAL";
        public static const MODE_VISTA:String = "MODE_ARRIVIAL";
        public static const MODE_DESTINATION:String = "MODE_DESTINATION";
        public static const MODE_BARTER:String = "MODE_BARTER";
        public static const MODE_STATUS:String = "MODE_STATUS";
        
        
        
        
        
        //Destinations
        public static const DEST_START:String = 'charleston'; 
        
        
        public static const SIMEON_CITIES:Array = new Array('charleston', 'jacksonville', 'atlanta', 'beta' );
        
        // hack
        public static var _bKeyOverride:Boolean = false
        
        
        // Xml Lists
        
        
        public static var _aChars:Array;
        public static var _aCities:Array;
        public static var _aShoplists:Array;
        public static var _aShops:Array; 
        public static var _aQuotes:Array;
        public static var _aCars:Array;
        public static var _aRegions:Array;
        
        // Applications
        public static var _application:Application;
        public static var _assetSwf:AssetSwf;
        
        // Town View Stuff
        public static const MAX_SHOPS_TOWN:int = 6;
        
        /*
        //BGs
        public static const BG_KUDZU:String = 'f_kudzu';
        public static const BG_ROCKIES:String = 'f_rockies';
        public static const BG_BEACH:String = 'f_beach';
        public static const BG_HILLS:String = 'f_hills';
        public static const BG_REFINERY:String = 'f_refinery';
        */
        
		// Health and Stress and Strength
        public static var HEALTH:int = 100; 
		
		public static var STRESS:int = 0 
		
		public static var STRENGTH:int = 100
		
        //Sky
        public static const SKY_CLOUDS:String = 'f_clouds';
        
        //Road
        public static const ROAD_STREET:String = 'f_street';
        
		//cars
        public static const CAR_3WHEEL:String = "3wheel";
        public static const CAR_BOXCHEVY:String = "boxchevy";
        public static const CAR_PRIUS:String = "prius";
        public static const CAR_SUV:String = "suv";
        
        
        //Night Hours
        public static const START_NIGHT:int = 20;
        public static const END_NIGHT:int = 8;
        
        
        //Regions
        public static const REGION_SOUTHEAST:String= "southeast";
        public static const REGION_SOUTHEAST_BEACH:String= "southeast_beach";
        
        public static const MILES_TO_PIXEL:Number = 4.593;
        
        public static var _nCurrentFoodQuality:Number = 50
        public static const MAX_FOOD_QUALITY:Number = 100
        
        
        public static var _aCoordsScenarios:Array;
        public static var _aScenarios:Array;
        public static var _aGoodIndexScenario:Array;
        public static var _aBinyahRoadIndexScenario:Array;
        public static var _aEtcIndexScenario:Array;
        public static var _aCharIndexScenario:Array;
        public static var _aStressIndexScenario:Array;
        public static var _aRoadStartIndexScenario:Array;
        public static var _aRoadStartBinyahIndexScenario:Array;
        	
        	
        
        // Currents
        public static var _sMode:String = MODE_INTRO;
        public static var _bHideSpeak:Boolean = false
        
        public static var _sCurrentCar:String = null;
        public static var _sCurrentSky:String = G.START_SKY; 
        public static var _sCurrentRoad:String = G.START_ROAD;
        public static var _sCurrentDestination:String = G.START_DESTINATION;
        public static var _nCurrentDollars:Number= G.START_DOLLARS;
        public static var _nCurrentAlert:int=2
        public static var _nX:Number = G.START_X;
        public static var _nY:Number = G.START_Y;
        public static var _nCurrentMPH:int = G.START_MPH;
        public static var _nCurrentIsDay:Boolean = G.START_ISDAY;
        public static var _nDistanceToDestination:Number;
        
		public static var _sCurrentTimeString:String = START_TIMESTRING;
		public static var _nCurrentHour:int = START_HOUR;
		public static var _nCurrentMin:int = START_MIN;
        public static var _nCurrentDay:int = 1;
        public static var _nCurrentGallons:Number = START_GALLONS;  
        public static var _nCurrentFood:int = START_FOOD;
        public static var _nCurrentWeight:int = 0;
        public static var _nCurrentBrochures:int = 0;
        public static var _nTotalBrochures:int = 0;
        public static var _nCurrentCitiesDestroyed:int = 0;
        public static var _nTotalCities:int = 0;
        public static var _sCurrentRegion:String = START_REGION;
        public static var _nMaxGallons:int = 100;
        
        
        
        public static var _aCoords:Array = new Array({x:START_X,y:START_Y});
        
		
		public static function fixTime():void{
			
			if(G._nCurrentMin == 6){
				G._nCurrentHour++;
				G._nCurrentMin = 0;
			}
			
			if(G._nCurrentHour == 24){
				G._nCurrentHour = 0;
				G._nCurrentDay++;
			}
			if(G._nCurrentHour == 0)
				G._sCurrentTimeString = '12';
			else if(G._nCurrentHour > 12)
				G._sCurrentTimeString = String(G._nCurrentHour - 12);
			else
				G._sCurrentTimeString = String(G._nCurrentHour);
				
			G._sCurrentTimeString += ':'+String(G._nCurrentMin)+'0'; 
			G._sCurrentTimeString += (G._nCurrentHour > 11)?'pm':'am';
			
		}
		
        public static function addCoords():void{
			if(G._aCoords[G._aCoords.length-1].x!=G._nX && G._aCoords[G._aCoords.length-1].y!=G._nY)
        		G._aCoords.push({x: int(G._nX),y: int(G._nY)})
        	
        	
        }
        
        public static function getBinyahStatus():String{
        	
			if(G._xBinyah._bDead)
				return "Dead"
        	if(Events.BINYAH_HUNGER>2)
        		return 'Very Hungry';
        	if(Events.BINYAH_HUNGER)
        		return 'Hungry';
        	else
        		return 'Not Hungry';
        }
        
        public static function getTankStatus():String{
        	var l_nGallons:int = _nCurrentGallons
        	return l_nGallons.toString()+" / "+_nMaxGallons+" gal"
        }
        
        public static function getStressStatus():String{
        	var l_nStress:int = calculatedStressNumber()
        	if(l_nStress > 90) return 'BORDERLINE';
        	if(l_nStress > 65) return 'High';
        	if(l_nStress > 40) return 'Medium';
        	return 'Low'; 
        }
        
        public static function calculatedStressNumber():int{
        	var l_iStress:int = STRESS
        	l_iStress += foodQualityMod()
        	l_iStress += ailmentMod()
        	l_iStress += G._aCars[G._sCurrentCar]._nStress
        	return l_iStress
        }
        
        public static function ailmentMod():int{
        	var l_nAilmentCount:int = 0
        	var l_sAilment:String
        	for(var i:int=0; i< G._aCarChars.length; i++){
        		var l_xChar:Passenger = G._aCarChars[i]
        		if(l_xChar._bDead) continue;
    	    	for each( l_sAilment in l_xChar._aAilments){
        			l_nAilmentCount++
	        	}
        	}
        	if(!G._xBinyah._bDead){
        		for each( l_sAilment in G._xBinyah._aAilments){
        			l_nAilmentCount++
	        	}
        	}
        	return l_nAilmentCount*5
        }
        
        // Foood
        
        public static function foodString():String{
        	return foodStringFromInt(_nCurrentFoodQuality)
        }
        
        public static function foodQualityMod():int{
        	if(_nCurrentFoodQuality > 66){
        		return 0
        	}else if(_nCurrentFoodQuality > 33){
        		return 20
        	}else{
        		return 40
        	}
        }
        public static function foodStringFromInt(p_sVal:int):String{
        	if(p_sVal > 66){
        		return "High"
        	}else if(p_sVal > 33){
        		return "Medium"
        	}else{
        		return "Low"	
        	}
        }
        
        public static var _xCurrentDestination:Destination;
        
        public static function alertStars():String{
			if(G._nCurrentAlert > 0){
				var l_sAlert:String = ""
				for(var i:int = 0; i<G._nCurrentAlert; i++){
					l_sAlert += "*"
				}
				return l_sAlert
							}
			else{
				return "~"
			}
        }
        
        
        //
        // HUNT
        //
        
        public static const CITY_BANK_PLUNDER:int = 2000;
        
        public static function getHuntVictoryOutcome():String{
        	if(G._xCurrentDestination._nType == 5){
        		return "None"
        	}
        	var l_nNewHealth:int = G.HEALTH - G.huntDamage() 
        	trace('l_nNewHealth '+l_nNewHealth)
        	if (l_nNewHealth < 0){
        		return "Defeat"
        	}
    		else if(G._xCurrentDestination._nDefense <= huntAttack()){
        		return "Destruction and Plunder"
    		}
        	else if(l_nNewHealth <= 25){
        		return "Close Victory"
        	}
        	else if(l_nNewHealth <= 50){
        		return "Minor Victory"
        	}
        	else{
        		return "Major Victory"
        	}
        }
        
        public static function huntDamage():int{
        	var l_nDamage:int = (25 *  G._xCurrentDestination._nDefense / G.huntAttack() )
        	return l_nDamage
        }
        
        public static function huntAttack():int{
        	var l_nAttack:int = (G.BASE_STRENGTH + calculatedStressNumber()) 
        	return l_nAttack
        }
        
        public static function huntPlunder():int{
        	var l_nRating:int = G._xCurrentDestination._nRating;
        	var l_nSize:int = G._xCurrentDestination._nType;
        	return (l_nRating+1) * (l_nSize+1) * 10
        }
        
        
        //
        // FINE
        //
        
        
        public static  function calcFine():int{
        	return 20;
        }
        
        public static function calcBinyah():int{
        	return Events.BINYAH_HUNGER*50
        }
        
		public static function calcDistanceToDestination(_sDestination:String = null):Number{
			if (!_sDestination) _sDestination = G._sCurrentDestination
			var l_xDest:Destination = G._aCities[_sDestination];
			var l_nTotalDistance:Number = Math.sqrt( Math.pow( ( l_xDest._nX - G._nX ),2)+ Math.pow(( l_xDest._nY - G._nY ),2)   );
			return l_nTotalDistance;
		}
        
        
        public static function roll(p_nSides:int = 100):int{
        	var l_nRoll:int = Math.floor(Math.random()*p_nSides);
        	return l_nRoll;
        }
        
        
        public static var _aPrefix:Array = new Array('Old ','North ','East ','New ','Fort ','Mc','West ','South ','','','','','','');
        public static var _aMiddle:Array = new Array('Nowhere','Lonely','Alone','Dust','Concrete','Desolate','Tired','Barren','Bleak','Bare','Empty','Gloom','Dismal','Blight','Flat','Salt','Tar','Dead','Waste','Numb','Misery');
        public static var _aSuffix:Array = new Array('ville','wick','burg','tol','bury','shire','son',' Valley','dale',' Gulch','ton','point','caster',' Land','boro',' Point','bridge','ford');
        
        
        //
        // CHARS
        //
        
        public static var _aCarChars:Array = new Array(
	        new Passenger('Miss Natelie'),
	        new Passenger('James'),
	        new Passenger('Vanessa'),
	        new Passenger('Shaina')
        );
        
        public static var _xBinyah:Passenger = new Passenger('Binyah Binyah',100, true);
        
        
        // Ailments
        public static var _aAilments:Array = new Array() 
        
        public static function makeAilmentString(p_xChar:Object):String{
        	if(p_xChar._bDead) return 'Dead from '+p_xChar._sLastDamage
        	var l_aAilments:Array = p_xChar._aAilments
        	var l_sAilmentString:String = ''
        	var l_nCount:int = 0
        	for each(var l_sAilment:String in l_aAilments){
        		l_sAilmentString += (l_nCount?', ':'')+l_sAilment
        		l_nCount++
        	}
        	if(!l_nCount && G._nCurrentFood == 0) return 'Hungry'
        	if(!l_nCount) return 'Healthy'
        	return l_sAilmentString
        }
        
        
        public static function totalCount():int{
        	var l_nCount:int = 0
        	for(var i:int=0; i< G._aCarChars.length; i++){
        		var l_xChar:Passenger = G._aCarChars[i]
        		if(l_xChar._bDead) continue;
        		l_nCount++
         	}
			if (!G._xBinyah._bDead){
        		l_nCount++
   			}
   			return l_nCount
        }
        
        public static function passengerCount():int{
        	var l_nCount:int = 1
        	for(var i:int=0; i< G._aCarChars.length; i++){
        		var l_xChar:Passenger = G._aCarChars[i]
        		if(l_xChar._bDead) continue;
        		l_nCount++
         	}
			if (!G._xBinyah._bDead){
        		l_nCount++
   			}
   			return l_nCount
        }
        
        public static function randomChar():Passenger{
        	var l_aAliveChars:Array = new Array
        	for(var i:int=0; i< G._aCarChars.length; i++){
        		var l_xChar:Passenger = G._aCarChars[i]
        		if(l_xChar._bDead) continue;
        		l_aAliveChars.push(l_xChar)
        	}
        	var l_nRoll:int = G.roll(l_aAliveChars.length)
        	if (l_aAliveChars.length > 0){
        		return l_aAliveChars[l_nRoll]
        	}
        	if(!G._xBinyah._bDead){
        		return G._xBinyah
        	}
        	return null
        }
        
        
        // Upkeep
        
        public static function heal():void{
        	if(G.HEALTH < 100){
        		_application.addAnnouncement("You rest and heal your wounds.<br><br>+"+(100 - G.HEALTH)+" Health");
        		G.HEALTH = 100
        	}
        }
        
        public static function reduceRage():void{
        	if(G.STRESS > 0)
        		_application.addAnnouncement("The day of rest puts your mind at ease, and you wake up feeling refreshed and ready for the day.<br><br>-"+G.STRESS+" Rage");{
        		G.STRESS = 0
        	}
        }
        
        public static function reduceAlarm():void{
        	if(G._nCurrentAlert > 0){
        		_application.addAnnouncement("You hide out for the night and keep the police off your back.<br><br> -1 Alert");
        		G._nCurrentAlert -= 1
        	}
        }
        
        public static function daysOfFood():int{
        	var l_nDays:int = int(G._nCurrentFood / G.foodPerDay())
        	return l_nDays
        }
        
        public static const FOOD_PER_DAY:int = 1

        public static function foodPerDay():int{
        	var l_aAliveChars:int = 1
        	for(var i:int=0; i< G._aCarChars.length; i++){
        		var l_xChar:Passenger = G._aCarChars[i]
        		if(l_xChar._bDead) continue;
        		l_aAliveChars++
        	}

        	var l_nPerDay:int = FOOD_PER_DAY
        	l_nPerDay+=FOOD_PER_DAY*l_aAliveChars
        	if(_xBinyah._nHealth)
        		l_nPerDay+=FOOD_PER_DAY*3
        	
        	return l_nPerDay
        }
        
        
        
        // Utils
		public static function upperCase(str:String) : String {
		 var firstChar:String = str.substr(0, 1); 
		 var restOfString:String = str.substr(1, str.length);  
		    
		 return firstChar.toUpperCase()+restOfString.toLowerCase(); 
		}
		
		
		//Debug
		public static function var_dump(_obj:Object,_nCount:int=0):void{
	        var item:Object;
	        var spaces:String = ""
	        var space:String = " "
	        for(var i:int=0; i<_nCount; i++){
	        	spaces += space
	        }
	        switch (typeof(_obj)){
	            case "object":
	                    trace(spaces+"<object>");
	                    if(_obj){
		                    trace(spaces+space+_obj.toString());
		                    for each (item in _obj){
		                            G.var_dump(item,_nCount+1);
		                    };
	                    	
	                    }
	                    trace(spaces+"</object>");
	            break;
	            case "xml":
	                    trace("spaces+<xml>");
	                    trace(spaces+_obj);
	                    trace(spaces+"</xml>");
	            break;
	            default:
	                    trace(_obj + " (" + typeof(_obj) + ")");
	            break;
		    };
		} // analyze()

    }

	
}