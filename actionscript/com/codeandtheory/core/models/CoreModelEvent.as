package com.codeandtheory.core.models {
	import flash.events.Event;


	public class CoreModelEvent extends Event {
		protected var _data:Object = null;
		protected var _type:String = null;
		public function get data () : Object {
			return _data;
		}


		public function CoreModelEvent (p_sType:String, p_xData:Object = null, p_bBubbles:Boolean = false, p_bCancellable:Boolean = false) {
			_data = p_xData;
			_type = p_sType;
			super(p_sType, p_bBubbles, p_bCancellable);
		}
	}
}