package com.codeandtheory.core.net {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.ProgressEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;


	public class XmlApiProxy extends EventDispatcher {
		// state
		protected var _loading:Boolean = false;
		public function get loading () : Boolean {
			return _loading;
		}


		// helpers
		protected var _apiUrl:String = null;
		protected var _loader:URLLoader = null;


		// loaded data
		protected var _lastLoadedData:Object = null;
		public function get lastLoadedData () : Object {
			return _lastLoadedData;
		}


		public function XmlApiProxy (p_sApiUrl:String) {
			_apiUrl = p_sApiUrl;

			_loader = new URLLoader();
			_loader.addEventListener(ProgressEvent.PROGRESS, onProgress, false, 0, true);
			_loader.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
		}
		
		
		public function reset () : void {
			try {
				_loader.close();
			} catch (e:Error) {
				trace("error closing loader ["+ e.message +"]");
				// this throws an error if there's nothing open but we don't care
				// about that - we just want to make sure that it doesn't have
				// anything open
			}
		}


		protected function createRequest (p_xParameters:URLVariables) : URLRequest {
			var ret_xRequest:URLRequest = new URLRequest(_apiUrl);
			ret_xRequest.data = p_xParameters;
			ret_xRequest.method = URLRequestMethod.POST;

			return ret_xRequest;
		}


		protected final function call (p_xUrlRequest:URLRequest) : Boolean {
			// don't load if already loading
			if (_loading) {
				return false;
			}

			// otherwise, make the call
			_loading = true;
			_loader.load(p_xUrlRequest);

			// we were able to start loading
			return true;
		}


		protected final function onProgress (p_xEvent:ProgressEvent) : void {
			dispatchEvent(p_xEvent);
		}


		protected final function onComplete (p_xEvent:Event) : void {
			_loading = false;
			_lastLoadedData = _loader.data;
//@TESTING
//trace("-----------------------------------------------------");
//trace(_lastLoadedData);
			onCompleteHook(p_xEvent);
			dispatchEvent(p_xEvent);
		}


		protected function onCompleteHook (p_xEvent:Event) : void {
			// optional override
		}
	}
}