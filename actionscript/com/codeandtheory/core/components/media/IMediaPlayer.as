package com.codeandtheory.core.components.media {
	import com.codeandtheory.core.components.slider.ISliderObject;
	
	public interface IMediaPlayer extends IMediaPlayerEvents, ISliderObject {
		function startSeeking():void;
		function stopSeeking():void;
		function isSeeking():Boolean;
		function seekToPercent(p_nSeekPercent:Number):void;
		function getLastSeekPercent():Number;
		function getLastSeekTime():Number;
		function getLastSeekTimeRemaining():Number;
		function stopActiveSlider():void
		
		function rewind():void;
		function pause():void;
		function resume():void;
		function isPaused():Boolean;
		function isAtPlaybackStart():Boolean;
		function isAtPlaybackEnd():Boolean;
		function getPercentPlayed():Number;
		function getElapsedTime():Number;
		function getDuration():Number;
		function getTimeRemaining():Number;
		
		function getBytesLoaded():Number;
		function getBytesTotal():Number;
		function isStreaming():Boolean;
		function getPercentLoaded():Number;
		function isLoadStarted():Boolean;
		
		function isBuffering():Boolean;
		
		function updatePositions():void;
		
		function getVolume():Number;
		function setVolume(p_xVolumePercent:Number):void;
		function isMuted():Boolean;
		function unmute():void;
		function mute():void;
	}
}