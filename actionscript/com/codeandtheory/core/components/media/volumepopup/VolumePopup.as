package com.codeandtheory.core.components.media.volumepopup {
	import com.codeandtheory.core.components.media.IMediaPlayer;
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	public class VolumePopup extends EventDispatcher {
		
		public static const SHOW_VOLUME_POPUP:String = "MEDIA_VOLUME_POPUP_SHOW_VOLUME_POPUP";
		public static const HIDE_VOLUME_POPUP:String = "MEDIA_VOLUME_POPUP_HIDE_VOLUME_POPUP";
		public static const VOLUME_POPUP_SHOWN:String = "MEDIA_VOLUME_POPUP_VOLUME_POPUP_SHOWN";
		public static const VOLUME_POPUP_HIDDEN:String = "MEDIA_VOLUME_POPUP_VOLUME_POPUP_HIDDEN";
		
		private var _xVolumeControlsSymbol:MovieClip;
		private var _xMediaPlayer:IMediaPlayer;
		private var _bVolumePopupShown:Boolean = false;
		
		public function VolumePopup(p_xVolumeControlsSymbol:MovieClip, p_xMediaPlayer:IMediaPlayer) {
			_xVolumeControlsSymbol = p_xVolumeControlsSymbol;
			_xMediaPlayer = p_xMediaPlayer;
			
			_xVolumeControlsSymbol.muteButton.addEventListener(MouseEvent.MOUSE_OVER, onVolumeOver);
			_xVolumeControlsSymbol.unmuteButton.addEventListener(MouseEvent.MOUSE_OVER, onVolumeOver);
			_xVolumeControlsSymbol.addEventListener(MouseEvent.MOUSE_OUT, onVolumeOut);
			_xVolumeControlsSymbol.volumePopup.visible = false;
			_xVolumeControlsSymbol.background.visible = false;
		}
		
		private function onVolumeOver(p_xEvent:MouseEvent):void {
			showVolumePopup(true);
		}
		
		private function onVolumeOut(p_xEvent:MouseEvent):void {
			if ( !_xVolumeControlsSymbol.hitTestPoint(_xVolumeControlsSymbol.stage.mouseX, _xVolumeControlsSymbol.stage.mouseY, true) ) {
				_xMediaPlayer.stopActiveSlider();
				showVolumePopup(false);
			}
		}
		
		private function onStageOut(p_xEvent:Event):void {
			_xMediaPlayer.stopActiveSlider();
			showVolumePopup(false);
		}
		
		private function showVolumePopup(p_bShow:Boolean):void {
			if ( p_bShow == _bVolumePopupShown ) {
				return;
			}
			var l_xVolumeContent:MovieClip = _xVolumeControlsSymbol.volumePopup.volumeContent;
			if ( p_bShow ) {
				dispatchEvent(new Event(SHOW_VOLUME_POPUP));
				_xVolumeControlsSymbol.stage.addEventListener(Event.MOUSE_LEAVE, onStageOut);
				
				_bVolumePopupShown = true;
				_xVolumeControlsSymbol.volumePopup.visible = true;
				_xVolumeControlsSymbol.background.visible = true;
				l_xVolumeContent.y = l_xVolumeContent.height;
				TweenLite.to(l_xVolumeContent, 0.5, { y:0, onComplete: function():void {
					dispatchEvent(new Event(VOLUME_POPUP_SHOWN));
					if ( !_xVolumeControlsSymbol.hitTestPoint(_xVolumeControlsSymbol.stage.mouseX, _xVolumeControlsSymbol.stage.mouseY, true) ) {
						showVolumePopup(false);
					}
				} });
			}
			else {
				dispatchEvent(new Event(HIDE_VOLUME_POPUP));
				_xVolumeControlsSymbol.stage.removeEventListener(Event.MOUSE_LEAVE, onStageOut);
				
				_bVolumePopupShown = false;
				_xVolumeControlsSymbol.background.visible = false;
				TweenLite.to(l_xVolumeContent, 0.5, { y:l_xVolumeContent.height, onComplete: function():void {
					dispatchEvent(new Event(VOLUME_POPUP_HIDDEN));
					_xVolumeControlsSymbol.volumePopup.visible = false;
				} });
			}
		}
	}
}