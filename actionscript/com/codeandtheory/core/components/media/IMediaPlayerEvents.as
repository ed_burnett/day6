package com.codeandtheory.core.components.media {
	
	public interface IMediaPlayerEvents {
		function addElapsedTimeUpdatedListener(p_xFunction:Function):void;
		function addSeekPerformedListener(p_xFunction:Function):void;
		function addLoadProgressUpdatedListener(p_xFunction:Function):void;
		function addMediaOpenedListener(p_xFunction:Function):void;
		function addMediaClosedListener(p_xFunction:Function):void;
		function addPlaybackStoppedListener(p_xFunction:Function):void;
		function addPlaybackPausedForAutoplayListener(p_xFunction:Function):void;
		function addBufferingStartedListener(p_xFunction:Function):void;
		function addBufferingStoppedListener(p_xFunction:Function):void;
		function addVolumeChangedListener(p_xFunction:Function):void;
		function addVolumeMutedListener(p_xFunction:Function):void;
		function addVolumeUnmutedListener(p_xFunction:Function):void;
		function addShowPlayListener(p_xFunction:Function):void;
		function addShowPauseListener(p_xFunction:Function):void;
		function addShowPostRollListener(p_xFunction:Function):void;
		function addHidePostRollListener(p_xFunction:Function):void;
		function addShowPreRollListener(p_xFunction:Function):void;
		function addHidePreRollListener(p_xFunction:Function):void;
		function addShowPushToPlayListener(p_xFunction:Function):void;
		function addHidePushToPlayListener(p_xFunction:Function):void;
		function addShowReplayListener(p_xFunction:Function):void;
		function addHideReplayListener(p_xFunction:Function):void;
	}
}