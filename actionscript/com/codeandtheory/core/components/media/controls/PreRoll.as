/**
* Author: Jon Harris
* Company: Code And Theory
* Date: September 2008
*/

package com.codeandtheory.core.components.media.controls {
	import com.codeandtheory.core.components.media.IMediaPlayer;
	
	import flash.display.DisplayObject;
	import flash.events.Event;

	public class PreRoll {
		private var _xMediaPlayer:IMediaPlayer;
		private var _xPreRoll:DisplayObject;
		private var _xShowPreRollFunction:Function;
		private var _xHidePreRollFunction:Function;
		private var _bPreRollShowing:Boolean = false;
		private var _bFirstShow:Boolean = true;
		private var _bDisabled:Boolean = false;
		
		public function PreRoll(p_xMediaPlayer:IMediaPlayer, p_xPreRoll:DisplayObject,
								p_xShowPreRollFunction:Function, p_xHidePreRollFunction:Function) {
			_xMediaPlayer = p_xMediaPlayer;
			_xPreRoll = p_xPreRoll;
			_xShowPreRollFunction = p_xShowPreRollFunction;
			_xHidePreRollFunction = p_xHidePreRollFunction;
			_xMediaPlayer.addShowPreRollListener(showPreRoll);
			_xMediaPlayer.addHidePreRollListener(hidePreRoll);
			if ( _xShowPreRollFunction == null ) {
				_xShowPreRollFunction = Controls.FUNCTION_SHOW_VISIBLE;
			}
			if ( _xHidePreRollFunction == null ) {
				_xHidePreRollFunction = Controls.FUNCTION_HIDE_VISIBLE;
			}
			showPreRollObject(_xMediaPlayer.isAtPlaybackStart());
		}
		
		public function isDisabled():Boolean { return _bDisabled; }
		
		public function setDisabled(p_bDisabled:Boolean):void { _bDisabled = p_bDisabled; }
		
		private function showPreRoll(p_xEvent:Event):void {
			if ( !isDisabled() ) {
				showPreRollObject(true);
			}
		}
		
		private function hidePreRoll(p_xEvent:Event):void {
			showPreRollObject(false);
		}
		
		private function showPreRollObject(p_bShow:Boolean):void {
			if ( _bFirstShow ) {
				_bFirstShow = false;
			}
			else if ( p_bShow == _bPreRollShowing ) {
				return;
			}
			_bPreRollShowing = p_bShow;
			if ( p_bShow ) {
				_xShowPreRollFunction.call(null, _xPreRoll);
			}
			else {
				_xHidePreRollFunction.call(null, _xPreRoll);
			}
		}
	}
}
