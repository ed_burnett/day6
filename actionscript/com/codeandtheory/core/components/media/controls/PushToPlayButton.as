/**
* Author: Jon Harris
* Company: Code And Theory
* Date: September 2008
*/

package com.codeandtheory.core.components.media.controls {
	import com.codeandtheory.core.components.media.IMediaPlayer;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class PushToPlayButton {
		private var _xMediaPlayer:IMediaPlayer;
		private var _xButton:DisplayObject;
		private var _xShowPushToPlayFunction:Function;
		private var _xHidePushToPlayFunction:Function;
		private var _bPushToPlayShowing:Boolean = false;
		private var _bFirstShow:Boolean = true;
		private var _bDisabled:Boolean = false;
		
		public function PushToPlayButton(p_xMediaPlayer:IMediaPlayer, p_xButton:DisplayObject,
										 p_xShowPushToPlayFunction:Function, p_xHidePushToPlayFunction:Function) {
			_xMediaPlayer = p_xMediaPlayer;
			_xButton = p_xButton;
			_xShowPushToPlayFunction = p_xShowPushToPlayFunction;
			_xHidePushToPlayFunction = p_xHidePushToPlayFunction;
			_xButton.addEventListener(MouseEvent.CLICK, buttonClicked);
			_xMediaPlayer.addShowPushToPlayListener(showPushToPlay);
			_xMediaPlayer.addHidePushToPlayListener(hidePushToPlay);
			if ( _xShowPushToPlayFunction == null ) {
				_xShowPushToPlayFunction = Controls.FUNCTION_SHOW_VISIBLE;
			}
			if ( _xHidePushToPlayFunction == null ) {
				_xHidePushToPlayFunction = Controls.FUNCTION_HIDE_VISIBLE;
			}
			showPushToPlayButton(_xMediaPlayer.isAtPlaybackStart() && !_xMediaPlayer.isLoadStarted());
		}

		public function isDisabled():Boolean { return _bDisabled; }
		
		public function setDisabled(p_bDisabled:Boolean):void { _bDisabled = p_bDisabled; }

		private function buttonClicked(p_xEvent:MouseEvent):void {
			if ( _xMediaPlayer.isAtPlaybackStart() || !_xMediaPlayer.isLoadStarted() ) {
				_xMediaPlayer.resume();
			}
		}
		
		private function showPushToPlay(p_xEvent:Event):void {
			if ( !isDisabled() ) {
				showPushToPlayButton(true);
			}
		}
		
		private function hidePushToPlay(p_xEvent:Event):void {
			showPushToPlayButton(false);
		}
		
		private function showPushToPlayButton(p_bShow:Boolean):void {
			if ( _bFirstShow ) {
				_bFirstShow = false;
			}
			else if ( p_bShow == _bPushToPlayShowing ) {
				return;
			}
			_bPushToPlayShowing = p_bShow;
			if ( p_bShow ) {
				_xShowPushToPlayFunction.call(null, _xButton);
			}
			else {
				_xHidePushToPlayFunction.call(null, _xButton);
			}
		}
	}
}
