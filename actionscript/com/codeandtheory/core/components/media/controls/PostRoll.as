/**
* Author: Jon Harris
* Company: Code And Theory
* Date: September 2008
*/

package com.codeandtheory.core.components.media.controls {
	import com.codeandtheory.core.components.media.IMediaPlayer;
	
	import flash.display.DisplayObject;
	import flash.events.Event;

	public class PostRoll {
		private var _xMediaPlayer:IMediaPlayer;
		private var _xPostRoll:DisplayObject;
		private var _xShowPostRollFunction:Function;
		private var _xHidePostRollFunction:Function;
		private var _bPostRollShowing:Boolean = false;
		private var _bFirstShow:Boolean = true;
		private var _bDisabled:Boolean = false;
		
		public function PostRoll(p_xMediaPlayer:IMediaPlayer, p_xPostRoll:DisplayObject,
								 p_xShowPostRollFunction:Function, p_xHidePostRollFunction:Function) {
			_xMediaPlayer = p_xMediaPlayer;
			_xPostRoll = p_xPostRoll;
			_xShowPostRollFunction = p_xShowPostRollFunction;
			_xHidePostRollFunction = p_xHidePostRollFunction;
			_xMediaPlayer.addShowPostRollListener(showPostRoll);
			_xMediaPlayer.addHidePostRollListener(hidePostRoll);
			if ( _xShowPostRollFunction == null ) {
				_xShowPostRollFunction = Controls.FUNCTION_SHOW_VISIBLE;
			}
			if ( _xHidePostRollFunction == null ) {
				_xHidePostRollFunction = Controls.FUNCTION_HIDE_VISIBLE;
			}
			showPostRollObject(_xMediaPlayer.isAtPlaybackEnd());
		}
		
		public function isDisabled():Boolean { return _bDisabled; }
		
		public function setDisabled(p_bDisabled:Boolean):void { _bDisabled = p_bDisabled; }
		
		private function showPostRoll(p_xEvent:Event):void {
			if ( !isDisabled() ) {
				showPostRollObject(true);
			}
		}
		
		private function hidePostRoll(p_xEvent:Event):void {
			showPostRollObject(false);
		}
		
		private function showPostRollObject(p_bShow:Boolean):void {
			if ( _bFirstShow ) {
				_bFirstShow = false;
			}
			else if ( p_bShow == _bPostRollShowing ) {
				return;
			}
			_bPostRollShowing = p_bShow;
			if ( p_bShow ) {
				_xShowPostRollFunction.call(null, _xPostRoll);
			}
			else {
				_xHidePostRollFunction.call(null, _xPostRoll);
			}
		}
	}
}
