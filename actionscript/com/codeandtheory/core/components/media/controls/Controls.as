/**
* Author: Jon Harris
* Company: Code And Theory
* Date: October 2008
*/

package com.codeandtheory.core.components.media.controls {
	
	import flash.display.DisplayObject;
	
	public class Controls {
		
		public static const FUNCTION_SHOW_VISIBLE:Function =
			function(p_xDisplayObject:DisplayObject):void {
				p_xDisplayObject.visible = true;
			};
			
		public static const FUNCTION_HIDE_VISIBLE:Function =
			function(p_xDisplayObject:DisplayObject):void {
				p_xDisplayObject.visible = false;
			};
		
		public static const FUNCTION_TOGGLE_HIDE_A_SHOW_B_VISIBLE:Function =
			function(p_xDisplayObjectA:DisplayObject, p_xDisplayObjectB:DisplayObject):void {
				p_xDisplayObjectA.visible = false;
				p_xDisplayObjectB.visible = true;
			};
			
		public static const FUNCTION_TOGGLE_SHOW_A_HIDE_B_VISIBLE:Function =
			function(p_xDisplayObjectA:DisplayObject, p_xDisplayObjectB:DisplayObject):void {
				p_xDisplayObjectA.visible = true;
				p_xDisplayObjectB.visible = false;
			};
		
		public static const FUNCTION_DO_NOTHING:Function =
			function(p_xDisplayObject:DisplayObject):void { };
		
		public function Controls() {
			
		}
	}
}