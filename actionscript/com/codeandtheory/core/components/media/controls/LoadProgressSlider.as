/**
* Author: Jon Harris
* Company: Code And Theory
* Date: July 2008
*/

package com.codeandtheory.core.components.media.controls {
	import com.codeandtheory.core.components.media.IMediaPlayer;	import com.codeandtheory.core.components.slider.Slider;		import flash.display.DisplayObject;	import flash.display.Sprite;	import flash.events.Event;
	public class LoadProgressSlider extends Slider {
		
		private var _xMediaPlayer:IMediaPlayer;
		
		public function LoadProgressSlider(p_xMediaPlayer:IMediaPlayer, p_xLoadProgressHolder:DisplayObject,
										   p_xLoadProgressDisplay:Sprite, p_nType:int, p_nDirection:int) {
			super(p_xMediaPlayer, p_xLoadProgressHolder, p_xLoadProgressDisplay, null, p_nType, p_nDirection, Slider.INTERACTION_NONE);
			_xMediaPlayer = p_xMediaPlayer;
			_xMediaPlayer.addLoadProgressUpdatedListener(loadProgressUpdated);
			_xMediaPlayer.addMediaOpenedListener(videoOpened);
			_xMediaPlayer.addMediaClosedListener(videoClosed);
		}
		
		protected override function getPercentage():Number {
			return _xMediaPlayer.getPercentLoaded();
		}
		
		private function loadProgressUpdated(p_xEvent:Event):void {
			updatePosition();
		}
		
		private function videoOpened(p_xEvent:Event):void {
			updatePosition();
		}
		
		private function videoClosed(p_xEvent:Event):void {
			updatePosition();
		}
	}
}