package com.codeandtheory.core.components.slicedimage {
	
	public interface ISlicedImageData {
		function get id () : int;
		function get imageId () : String;
		function get zoom () : int;
		function get zoomMax () : int;
		function get totalWidth () : Number;
		function get totalHeight () : Number;
		function get sliceUrls () : Array;
		function clone () : ISlicedImageData;
	}
}