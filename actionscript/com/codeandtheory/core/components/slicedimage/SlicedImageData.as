package com.codeandtheory.core.components.slicedimage {
	public class SlicedImageData implements ISlicedImageData {
		protected var _id:int = 0;
		protected var _imageId:String = null;
		protected var _zoom:int = 0;
		protected var _zoomMax:int = 0;
		protected var _totalWidth:Number = 0;
		protected var _totalHeight:Number = 0;
		protected var _sliceUrls:Array = null;


		public function SlicedImageData (p_nId:int, p_sImageId:String, p_nZoom:int, p_nZoomMax:int, p_nTotalWidth:Number, p_nTotalHeight:Number, p_asSliceUrls:Array) {
			_id = p_nId;
			_imageId = p_sImageId;
			_zoom = p_nZoom;
			_zoomMax = p_nZoomMax;
			_totalWidth = p_nTotalWidth;
			_totalHeight = p_nTotalHeight;
			_sliceUrls = p_asSliceUrls;
		}
		
		
		public function get id () : int {
			return _id;
		}
		public function get imageId () : String {
			return _imageId;
		}
		public function get zoom () : int {
			return _zoom;
		}
		public function get zoomMax () : int {
			return _zoomMax;
		}
		public function get totalWidth () : Number {
			return _totalWidth;
		}
		public function get totalHeight () : Number {
			return _totalHeight;
		}
		public function get sliceUrls () : Array {
			return _sliceUrls;
		}
		
		
		public function clone () : ISlicedImageData {
			return new SlicedImageData(_id, _imageId, _zoom, _zoomMax, _totalWidth, _totalHeight, cloneSlices());
		}
		protected function cloneSlices () : Array {
			var ret_asClonedArray:Array = new Array();
			for (var i:int=0; i<_sliceUrls.length; i++) {
				ret_asClonedArray[i] = new Array();
				for (var j:int=0; j<_sliceUrls[i].length; j++) {
					ret_asClonedArray[i][j] = _sliceUrls[i][j];
				}
			}

			return ret_asClonedArray;
		}
		
		
		public function toString () : String {
			return "DATA["+ _id +"] - zoom=["+ _zoom +"],totalWidth=["+ _totalWidth +"],totalHeight=["+ _totalHeight +"],sliceUrls[i][j]=["+ _sliceUrls.length +"]["+ _sliceUrls[0].length +"]";
		}
	}
}