package com.codeandtheory.core.components.slicedimage {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FullScreenEvent;


	public class SlicedImage extends Sprite {
		// config
		protected var _config:ISlicedImageConfig = null;
		public function get config () : ISlicedImageConfig {
			return _config;
		}


		// model
		protected var _model:SlicedImageModel = null;
		public function get model () : SlicedImageModel {
			return _model;
		}


		// view
		protected var _view:SlicedImageView = null;
		public function get view () : SlicedImageView {
			return _view;
		}


		public function SlicedImage (p_xProvider:ISlicedImageConfig) {
			_config = p_xProvider;
			initializeComponent();
		}


		protected function initializeComponent () : void {
			_model = new SlicedImageModel(_config.dataSourceApiUrl, _config.zoomMax, _config.zoomMin);

			_view = new SlicedImageView(_model, _config.viewportWidth, _config.viewportHeight);
			addChild(_view);
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		
		protected function onAddedToStage (p_xEvent:Event) : void {
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			stage.addEventListener(FullScreenEvent.FULL_SCREEN, onFullscreenUpdated, false, 0, true);
		}


		protected function onFullscreenUpdated (p_xEvent:FullScreenEvent) : void {
			_model.fullscreen = p_xEvent.fullScreen;
		}
	}
}