package com.codeandtheory.core.components.slicedimage {
	import com.codeandtheory.core.net.XmlApiProxy;
	import com.codeandtheory.core.utils.StringUtils;
	
	import flash.events.Event;
	import flash.net.URLVariables;


	public class SlicedImageDataSource extends XmlApiProxy implements ISlicedImageDataSource {
		protected var _data:SlicedImageData = null;


		public function SlicedImageDataSource (p_sApiUrl:String) {
			super(p_sApiUrl);
		}


		public function loadSlicedImageData (p_sSlicedImageDataId:String, p_nZoomLevel:int) : Boolean {
			var l_xParameters:URLVariables = new URLVariables();
			l_xParameters.a = "slicedimage";
			l_xParameters.image = p_sSlicedImageDataId;
			l_xParameters.zoom = p_nZoomLevel;

			return call(createRequest(l_xParameters)); 
		}


		override protected function onCompleteHook (p_xEvent:Event) : void {
			_data = parseSlicedImageData(new XML(_lastLoadedData));
		}
		
		
		protected function parseSlicedImageData (p_xXml:XML) : SlicedImageData {
			var l_nId:int = parseInt(p_xXml.@id);
			var l_sImageId:String = p_xXml.@imageId;
			var l_nZoom:int = parseInt(p_xXml.@zoom);
			var l_nZoomMax:int = parseInt(p_xXml.@zoomMax);
			var l_nTotalWidth:Number = new Number(p_xXml.@totalWidth);
			var l_nTotalHeight:Number = new Number(p_xXml.@totalHeight);
			var l_nSlicesWidth:int = parseInt(p_xXml.@slicesWidth);
			var l_nSlicesHeight:int = parseInt(p_xXml.@slicesHeight);
			var l_sSlicesBaseUrl:String = p_xXml.@slicesBaseUrl;
			var l_sSlicesFileExtension:String = p_xXml.@slicesFileExtension;
			var l_asSliceUrls:Array = new Array();
			var l_nTempIndex:int = 1;
			for (var i:int=0; i<l_nSlicesHeight; i++) {
				l_asSliceUrls[i] = new Array();
				for (var j:int=0; j<l_nSlicesWidth; j++) {
					if (l_sImageId == "test1") {
						l_asSliceUrls[i][j] = l_sSlicesBaseUrl + l_sImageId +"_"+ (i + 1) +"_"+ (j + 1) +"."+ l_sSlicesFileExtension;
					} else {					
						l_asSliceUrls[i][j] = l_sSlicesBaseUrl + l_sImageId +"_"+ StringUtils.forceTwoDigitString(l_nTempIndex) + "."+ l_sSlicesFileExtension;
					}
					l_nTempIndex++;
				}
			}

			return new SlicedImageData(l_nId, l_sImageId, l_nZoom, l_nZoomMax, l_nTotalWidth, l_nTotalHeight, l_asSliceUrls);
		}
		
		
		public function get slicedImageData () : ISlicedImageData {
			return _data;
		}
	}
}