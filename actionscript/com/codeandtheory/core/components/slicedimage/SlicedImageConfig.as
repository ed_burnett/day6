package com.codeandtheory.core.components.slicedimage {
	public class SlicedImageConfig implements ISlicedImageConfig {
		protected var _dataSourceApiUrl:String = null;
		protected var _viewportWidth:Number = 0;
		protected var _viewportHeight:Number = 0;
		protected var _zoomMax:int = 1;
		protected var _zoomMin:int = 1;


		public function SlicedImageConfig (p_sDataSourceApiUrl:String, p_nViewportWidth:Number, p_nViewportHeight:Number, p_nZoomMax:int = 1, p_nZoomMin:int = 1) {
			_dataSourceApiUrl = p_sDataSourceApiUrl;
			_viewportWidth = p_nViewportWidth;
			_viewportHeight = p_nViewportHeight;
			_zoomMax = p_nZoomMax;
			_zoomMin = p_nZoomMin;
		}


		public function get dataSourceApiUrl () : String {
			return _dataSourceApiUrl;
		}
		public function get viewportWidth () : Number {
			return _viewportWidth;
		}
		public function get viewportHeight () : Number {
			return _viewportHeight;
		}
		public function get zoomMax () : int {
			return _zoomMax;
		}
		public function get zoomMin () : int {
			return _zoomMin;
		}
	}
}