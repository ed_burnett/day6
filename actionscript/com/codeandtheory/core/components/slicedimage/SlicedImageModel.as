package com.codeandtheory.core.components.slicedimage {
	import flash.events.Event;
	import flash.events.EventDispatcher;


	public class SlicedImageModel extends EventDispatcher {
		public static const DATA_UPDATED:String = "DATA_UPDATED";
		public static const FULLSCREEN_UPDATED:String = "FULLSCREEN_UPDATED";
		
		
		protected var _dataSource:ISlicedImageDataSource = null;
		protected var _data:ISlicedImageData = null;


		protected var _zoom:int = 1;
		protected var _zoomMax:int = 1;
		protected var _zoomMin:int = 1;
		
		
		protected var _fullscreen:Boolean = false;


		public function SlicedImageModel (p_sDataSourceApiUrl:String, p_nZoomMax:int = 1, p_nZoomMin:int = 1) {
			_dataSource = new SlicedImageDataSource(p_sDataSourceApiUrl);
			_zoomMax = p_nZoomMax;
			_zoomMin = p_nZoomMin;
		}
		
		
		public function setDataSource(p_xDataSource:ISlicedImageDataSource) : void {
			_dataSource = p_xDataSource;
		}
		
		
//---------------------------------------------------------------------------------------
// FULLSCREEN FUNCTIONS
		public function get fullscreen () : Boolean {
			return _fullscreen;
		}


		public function set fullscreen (p_bFullscreen:Boolean) : void {
			_fullscreen = p_bFullscreen;
			onFullscreenUpdated();
		}
		
		
		public function get zoom () : int {
			return _zoom;
		}


		protected function onFullscreenUpdated () : void {
			dispatchEvent(new Event(FULLSCREEN_UPDATED));
		}


//---------------------------------------------------------------------------------------
// DATA FUNCTIONS
		public function updateSlicedImageData (p_sId:String, p_nZoom:int) : void {
			_zoom = p_nZoom;
			_dataSource.addEventListener(Event.COMPLETE, onDataLoaded, false, 0, true);
			_dataSource.loadSlicedImageData(p_sId, p_nZoom);
		}
		
		
		public function onDataLoaded (p_xEvent:Event) : void {
			_data = _dataSource.slicedImageData.clone();
			_zoomMax = _data.zoomMax;
			dispatchEvent(new Event(DATA_UPDATED));
		}


		public function get data () : ISlicedImageData {
			return _data;
		}


//---------------------------------------------------------------------------------------
// ZOOM FUNCTIONS
		public function zoomIn () : void {
			if (_data != null && _data.zoom < _zoomMax) {
				updateSlicedImageData(_data.imageId, _data.zoom + 1);
			}
		}


		public function zoomOut () : void {
			if (_data != null && _data.zoom > _zoomMin) {
				updateSlicedImageData(_data.imageId, _data.zoom - 1);
			}
		}
		
		
		public function get zoomMax () : int {
			return _zoomMax;
		}
		
		
		public function get zoomMin () : int {
			return _zoomMin;
		}
	}
}