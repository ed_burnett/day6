package com.codeandtheory.core.components.slicedimage {
	public interface ISlicedImageConfig {
		function get dataSourceApiUrl () : String;
		function get zoomMin () : int;
		function get zoomMax () : int;
		function get viewportWidth () : Number;
		function get viewportHeight () : Number;
	}
}