package com.codeandtheory.core.components.slicedimage {
	import com.codeandtheory.core.components.image.Image;
	import com.codeandtheory.core.components.image.ImageProvider;
	import com.codeandtheory.core.utils.ShapeUtils;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;


	public class SlicedImageView extends Sprite {
		// event constants
		public static const SLICES_WIDTH_HEIGHT_RATIO_UPDATED:String = "SLICES_WIDTH_HEIGHT_RATIO_UPDATED";
		public static const POSITION_UPDATED:String = "POSITION_UPDATED";


		// model
		protected var _model:SlicedImageModel = null;


		// ui
		protected var _viewport:Sprite = null;
		protected var _viewportMask:Sprite = null;
		protected var _viewportDrag:Sprite = null;
		protected var _slicesContainer:Sprite = null;
		protected var _slices:Array = null;


		// ui properties
		protected var _viewportWidth:Number = 400;
		protected var _viewportHeight:Number = 300;
		protected var _restoreViewportWidth:Number = 400;
		protected var _restoreViewportHeight:Number = 300;
		protected var _restoreParent:DisplayObjectContainer = null;
		
		
		// ui calculated properties
		protected var _sliceCountWidth:int = 0;
		protected var _sliceCountHeight:int = 0;
		protected var _sliceWidth:Number = 0;
		protected var _sliceHeight:Number = 0;
		protected var _adjustedTotalWidth:Number = 0;
		protected var _adjustedTotalHeight:Number = 0;
		protected var _viewportTotalWidthRatio:Number = 0;
		protected var _viewportTotalHeightRatio:Number = 0;


		// ui state
		protected var _firstVisibleSliceRow:int = 0;
		protected var _firstVisibleSliceColumn:int = 0;
		protected var _slicesPerViewportHeight:int = 0;
		protected var _slicesPerViewportWidth:int = 0;
		protected var _slicesContainerPercentageX:Number = 0;
		protected var _slicesContainerPercentageY:Number = 0;
		protected var _visibleSlices:Array = null;


		public function SlicedImageView (p_xModel:SlicedImageModel, p_nViewportWidth:Number = 400, p_nViewportHeight:Number = 300) {
			_viewportWidth = p_nViewportWidth;
			_viewportHeight = p_nViewportHeight;
			_restoreViewportWidth = p_nViewportWidth;
			_restoreViewportHeight = p_nViewportHeight;
			initializeView();

			_model = p_xModel;
			_model.addEventListener(SlicedImageModel.DATA_UPDATED, onDataUpdated, false, 0, true);
			_model.addEventListener(SlicedImageModel.FULLSCREEN_UPDATED, onFullscreenUpdated, false, 0, true);
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		
		public function reflectPositionPercentage (p_nPercentageX:Number, p_nPercentageY:Number) : void {
			_viewportDrag.x = (_viewportWidth - _adjustedTotalWidth) * p_nPercentageX;
			_slicesContainer.x = _viewportDrag.x;
			_slicesContainerPercentageX = _slicesContainer.x / (_viewportWidth - _adjustedTotalWidth);
			
			_viewportDrag.y = (_viewportHeight - _adjustedTotalHeight) * p_nPercentageY;
			_slicesContainer.y = _viewportDrag.y;
			_slicesContainerPercentageY = _slicesContainer.y / (_viewportHeight - _adjustedTotalHeight);

			updateVisibleSlices();
		}
		
		
		protected function onAddedToStage (p_xEvent:Event) : void {
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

			_restoreParent = this.parent;
		}
		
		
//---------------------------------------------------------------------------------------
// MODEL RESPONSE FUNCTIONS
		protected function onDataUpdated (p_xEvent:Event) : void {
			clearSlices();
			updateSlicesCalculations();
			updateSlicesContainer();
			updateVisibleSlices();
		}


		protected function onFullscreenUpdated (p_xEvent:Event) : void {
			if (_model.fullscreen) {
				_viewportWidth = stage.stageWidth;
				_viewportHeight = stage.stageHeight;
				stage.addChild(this);
			} else {
				_viewportWidth = _restoreViewportWidth;
				_viewportHeight = _restoreViewportHeight;
				_restoreParent.addChild(this);
			}

			updateSlicesCalculations();
			updateSlicesContainer();
			updateVisibleSlices();
		}


//---------------------------------------------------------------------------------------
// HELPER FUNCTIONS
		protected function clearSlices () : void {
			if (_slices != null) {
				for (var i:int=0; i<_slices.length; i++) {
					_slicesContainer.removeChild(_slices[i]);
				}
			}
			_slices = new Array();
		}
		
		
		protected function updateSlicesCalculations () : void {
			_sliceCountWidth = _model.data.sliceUrls[0].length;
			_sliceCountHeight = _model.data.sliceUrls.length;

			_sliceWidth = Math.floor(_model.data.totalWidth / _sliceCountWidth);
			_sliceHeight = Math.floor(_model.data.totalHeight / _sliceCountHeight);

			_slicesPerViewportHeight = Math.ceil(_viewportWidth / _sliceWidth);
			_slicesPerViewportWidth = Math.ceil(_viewportHeight / _sliceHeight);

			_adjustedTotalWidth = _sliceCountWidth * _sliceWidth;
			_adjustedTotalHeight = _sliceCountHeight * _sliceHeight;
			
			_viewportTotalWidthRatio = _viewportWidth / _adjustedTotalWidth;
			_viewportTotalHeightRatio = _viewportHeight / _adjustedTotalHeight;

			_firstVisibleSliceColumn = -1;
			_firstVisibleSliceRow = -1;
		}


		protected function updateSlicesContainer () : void {
			// once new slices are created, everything gets wiped back to not visible
			_visibleSlices = new Array();


			// create images for each slice position (providers will be updated only based on what's visible)
			var l_xTempSlice:Sprite = null;
			var l_nTempX:Number = 0;
			var l_nTempY:Number = 0;
			for (var i:int=0; i<_model.data.sliceUrls.length; i++) {
				_visibleSlices[i] = new Array();
				
				for (var j:int=0; j<_model.data.sliceUrls[i].length; j++) {
					l_xTempSlice = new Image();
					l_xTempSlice.name = "slice"+ i +"_"+ j;
					l_xTempSlice.x = l_nTempX;
					l_xTempSlice.y = l_nTempY;

					_slicesContainer.addChild(l_xTempSlice);
					_slices.push(l_xTempSlice);
					
					_visibleSlices[i][j] = false;

					l_nTempX += _sliceWidth;
				}

				l_nTempX = 0;
				l_nTempY += _sliceHeight;
			}

			// redraw the drag rectangle (this should be the total width / height for dragging purposes)
			ShapeUtils.redrawRectangle(_viewportDrag.graphics, _adjustedTotalWidth, _adjustedTotalHeight);
			
			// redraw the mask
			ShapeUtils.redrawRectangle(_viewportMask.graphics, _viewportWidth, _viewportHeight);

			// update the new rectangle's position based on it's previous percentage
			var l_nMinX:Number = _viewportWidth - _adjustedTotalWidth;
			var l_nX:Number = _slicesContainerPercentageX * l_nMinX;
			if (l_nX > 0) {
				l_nX = 0;
			} else if (l_nX < l_nMinX && l_nMinX < 0) {// if l_nMinX is greater than 0, then that means viewport is bigger than the image and it should just be placed top left alignment
				l_nX = l_nMinX;
			}  
			var l_nMinY:Number = _viewportHeight - _adjustedTotalHeight;
			var l_nY:Number = _slicesContainerPercentageY * l_nMinY;
			if (l_nY > 0) {
				l_nY = 0;
			} else if (l_nY < l_nMinY && l_nMinY < 0) {// same for l_nMinY as described for l_nMinX above
				l_nY = l_nMinY;
			}
			_slicesContainer.x = l_nX;
			_slicesContainer.y = l_nY;
			_viewportDrag.x = l_nX;
			_viewportDrag.y = l_nY;
			
			// dispatch event
			dispatchEvent(new Event(SLICES_WIDTH_HEIGHT_RATIO_UPDATED));
		}
		
		
		protected function updateVisibleSlices () : void {
			// calculate the first visible row / column
			var l_nFirstColumn:int = _slicesContainer.x > 0 ? 0 : Math.floor(Math.abs(_slicesContainer.x) / _sliceWidth);
			var l_nFirstRow:int = _slicesContainer.y > 0 ? 0 : Math.floor(Math.abs(_slicesContainer.y) / _sliceHeight);

			// only update when necessary
			if ((_firstVisibleSliceColumn != l_nFirstColumn) || (_firstVisibleSliceRow != l_nFirstRow)) {
				_firstVisibleSliceRow = l_nFirstRow;
				_firstVisibleSliceColumn = l_nFirstColumn;
				updateSliceImageProviders();
			}
		}
		
		
		protected function updateSliceImageProviders () : void {
			var l_nLastRow:int = _firstVisibleSliceRow + _slicesPerViewportWidth + 1;
			if (l_nLastRow > _sliceCountHeight) {
				l_nLastRow = _sliceCountHeight;
			}
			var l_nLastColumn:int = _firstVisibleSliceColumn + _slicesPerViewportHeight + 1;
			if (l_nLastColumn > _sliceCountWidth) {
				l_nLastColumn = _sliceCountWidth;
			}
			
			var l_xTempSlice:Image = null;
			for (var i:int=_firstVisibleSliceRow; i<l_nLastRow; i++) {
				for (var j:int=_firstVisibleSliceColumn; j<l_nLastColumn; j++) {
					if (!_visibleSlices[i][j]) {
						l_xTempSlice = _slicesContainer.getChildByName("slice"+ i +"_"+ j) as Image;
						l_xTempSlice.provider = new ImageProvider(_model.data.sliceUrls[i][j], _sliceWidth, _sliceHeight, 0xFFFFFF, false, true);
						_visibleSlices[i][j] = true;
					}
				}
			}
		}
		
		
//---------------------------------------------------------------------------------------
// DRAGGING FUNCTIONS
		protected function onDragMouseDown (p_xEvent:MouseEvent) : void {
			_viewportDrag.startDrag(false, new Rectangle(0, 0, _viewportWidth - _adjustedTotalWidth, _viewportHeight - _adjustedTotalHeight));
			_viewportDrag.addEventListener(Event.ENTER_FRAME, onDragEnterFrame, false, 0, true);
		}


		protected function onDragMouseUp (p_xEvent:MouseEvent) : void {
			_viewportDrag.stopDrag();
			_viewportDrag.removeEventListener(Event.ENTER_FRAME, onDragEnterFrame);			
		}


		protected function onDragEnterFrame (p_xEvent:Event) : void {
			_slicesContainer.x = _viewportDrag.x;
			_slicesContainerPercentageX = _model.zoom == 1 ? 0 : _slicesContainer.x / (_viewportWidth - _adjustedTotalWidth);
			
			_slicesContainer.y = _viewportDrag.y;
			_slicesContainerPercentageY = _model.zoom == 1 ? 0 : _slicesContainer.y / (_viewportHeight - _adjustedTotalHeight);
			
			updateVisibleSlices();
			
			dispatchEvent(new Event(POSITION_UPDATED));
		}
		
		
//---------------------------------------------------------------------------------------
// GETTER / SETTER FUNCTIONS
		public function get slicesContainerPercentageX () : Number {
			return _slicesContainerPercentageX;
		}
		
		
		public function get slicesContainerPercentageY () : Number {
			return _slicesContainerPercentageY;
		}


		public function get viewportTotalWidthRatio () : Number {
			return _viewportTotalWidthRatio;
		}
		
		
		public function get viewportTotalHeightRatio () : Number {
			return _viewportTotalHeightRatio;
		}


//---------------------------------------------------------------------------------------
// INITIALIZE FUNCTIONS
		protected function initializeView () : void {
			// mask
			_viewportMask = ShapeUtils.newRectangle(_viewportWidth, _viewportHeight);

			// drag
			_viewportDrag = new Sprite();
			_viewportDrag.alpha = 0;
			_viewportDrag.useHandCursor = true;
			_viewportDrag.buttonMode = true;
			_viewportDrag.addEventListener(MouseEvent.MOUSE_DOWN, onDragMouseDown, false, 0, true);
			_viewportDrag.addEventListener(MouseEvent.MOUSE_UP, onDragMouseUp, false, 0, true);

			// viewport
			_viewport = new Sprite();

			// slices container
			_slicesContainer = new Sprite();

			// display list ordering
			_viewport.addChild(_slicesContainer);
			_viewport.addChild(_viewportDrag);
			addChild(_viewport);
			addChild(_viewportMask);
			
			// set mask
			_viewport.mask = _viewportMask;
		}
	}
}