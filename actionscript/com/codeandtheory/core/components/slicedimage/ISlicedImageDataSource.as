package com.codeandtheory.core.components.slicedimage {
	import flash.events.IEventDispatcher;
	
	public interface ISlicedImageDataSource extends IEventDispatcher {
		
		function loadSlicedImageData (p_sSlicedImageDataId:String, p_nZoomLevel:int) : Boolean;
		function get slicedImageData () : ISlicedImageData;
	}
}