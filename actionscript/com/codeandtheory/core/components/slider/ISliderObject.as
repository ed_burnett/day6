package com.codeandtheory.core.components.slider {
	
	public interface ISliderObject {
		function setActiveSlider(p_xSlider:Slider):void;
		function getRoundSliderDisplayPositions():Boolean;
	}
}