/**
* Author: Jon Harris
* Company: Code And Theory
* Date: July 2008
*/

package com.codeandtheory.core.components.video.test {
	import flash.text.TextFieldAutoSize;	
	import flash.text.TextField;
	import flash.display.Sprite;
	
	public class TesterButton extends Sprite {
		private var _xLabelText:TextField;
		private var _xBackground:Sprite;
		
		public function TesterButton(p_sLabel:String) {
			_xLabelText = new TextField();
			_xLabelText.multiline = false;
			_xLabelText.autoSize = TextFieldAutoSize.LEFT;
			_xLabelText.wordWrap = false;
			_xLabelText.selectable = false;
			_xLabelText.text = p_sLabel;
			
			_xBackground = new Sprite();
			_xBackground.graphics.lineStyle(1, 0x000000);
			_xBackground.graphics.beginFill(0xeeeeee, 1);
			_xBackground.graphics.drawRect(0, 0, _xLabelText.width, _xLabelText.height);
			_xBackground.graphics.endFill();
			
			addChild(_xBackground);
			addChild(_xLabelText);
		}
	}
}
