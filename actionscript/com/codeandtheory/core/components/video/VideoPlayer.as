﻿/**
* Author: Jon Harris
* Company: Code And Theory
* Date: July 2008
*/

package com.codeandtheory.core.components.video {
	import com.codeandtheory.core.components.media.MediaPlayerEvents;
	import com.codeandtheory.core.utils.AspectUtils;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	import flash.events.NetStatusEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.media.SoundTransform;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.utils.Timer;

	public class VideoPlayer extends MediaPlayerEvents {
		private var _xNetConnection:NetConnection;
		private var _xTunnelingNetConnection:NetConnection;
		private var _xNetStream:NetStream;
		private var _xVideo:Video;
		private var _xSizeHolder:Sprite;
		private var _bPreserveAspectRatio:Boolean;
		private var _bVideoDurationDetermined:Boolean;
		private var _bVideoSizeDetermined:Boolean;
		private var _nVideoWidth:Number;
		private var _nVideoHeight:Number;
		private var _nPreFullScreenWidth:Number;
		private var _nPreFullScreenHeight:Number;
		private var _nPreFullScreenX:Number;
		private var _nPreFullScreenY:Number;
		private var _xPreFullScreenParent:DisplayObjectContainer;
		private var _nPreFullScreenParentChildIndex:int;
		private var _bFullScreen:Boolean;
		private var _sVideoFile:String;
		private var _bFirstPlay:Boolean;
		private var _bAutoload:Boolean;
		private var _bAutoplay:Boolean;
		private var _bNoAutoplayShowFrame:Boolean;
		private var _nNoAutoplayShowFrameOffset:Number;
		private var _bPerformingAutoplaySeek:Boolean;
		private var _bPausedForAutoplay:Boolean;
		private var _nDuration:Number;
		private var _bAtPlaybackStart:Boolean;
		private var _bAtPlaybackEnd:Boolean;
		private var _bRewindOnStop:Boolean;
		private var _bPlaybackBuffering:Boolean;
		private var _bLoadBuffering:Boolean;
		private var _bSeeking:Boolean;
		private var _nLastSeekPercent:Number;
		private var _bPlayingAtSeekStart:Boolean;
		private var _bPaused:Boolean;
		private var _bVideoLoadStarted:Boolean;
		private var _bVideoLoadCompleted:Boolean;
		private var _nLastBytesLoaded:Number;
		private var _nLastElapsedTime:Number;
		private var _nVolume:Number;
		private var _bMuted:Boolean;
		private var _xTimer:Timer;
		private var _nReportingInterval:int;
		private var _nBufferTime:Number;
		private var _bStreaming:Boolean;
		private var _sStreamUrl:String;
		private var _sTunnelingStreamUrl:String;
		private var _sStreamFile:String;
		private var _bExpediteStreamTunneling:Boolean = true;
		private var _bNetStreamConnected:Boolean;
		private var _bPreVideoOrnamentsShown:Boolean = false;
		private var _bPostVideoOrnamentsShown:Boolean = false;
		
		public function VideoPlayer() {
			init();
		}
		
		public function getVideoFile():String { return _sVideoFile; }
		
		public function isStreaming():Boolean { return _bStreaming; }
		
		public function getStreamUrl():String { return _sStreamUrl; }
		
		public function getTunnelingStreamUrl():String { return _sTunnelingStreamUrl; }
		
		public function isAutoload():Boolean { return _bAutoload || isAutoplay(); }
		
		public function isAutoplay():Boolean { return _bAutoplay; }
		
		public function isNoAutoplayShowFrame():Boolean { return !isAutoplay() && _bNoAutoplayShowFrame; }
		
		public function getNoAutoplayShowFrameOffset():Number { return _nNoAutoplayShowFrameOffset; }
		
		public function isLoadingForAutoplayShowFrame():Boolean {
			return !isAutoload() && !isAutoplay() && isNoAutoplayShowFrame() && _bFirstPlay;
		}
		
		public function isPausedForAutoplay():Boolean { return _bPausedForAutoplay; }
		
		public function isPreserveAspectRatio():Boolean { return _bPreserveAspectRatio; }
		
		public function isLoadStarted():Boolean { return _bVideoLoadStarted; }
		
		public function isLoadCompleted():Boolean { return _bVideoLoadCompleted; }
		
		public function isAtPlaybackStart():Boolean { return _bAtPlaybackStart; }
		
		public function isAtPlaybackEnd():Boolean { return _bAtPlaybackEnd; }
		
		public function isRewindOnStop():Boolean { return _bRewindOnStop; }
		
		public function isBuffering():Boolean { return isLoadBuffering() || isPlaybackBuffering(); }
		
		public function isLoadBuffering():Boolean { return _bLoadBuffering; }
		
		public function isPlaybackBuffering():Boolean { return _bPlaybackBuffering; }
		
		public function isSeeking():Boolean { return _bSeeking; }
		
		public function isPaused():Boolean { return _bPaused; }
		
		public function isExpediteStreamTunneling():Boolean { return _bExpediteStreamTunneling; }
		
		public function getReportingInterval():int { return _nReportingInterval; }
		
		public function getBufferTime():Number { return _nBufferTime; }
		
		public function getPercentPlayed():Number {
			if ( !isLoadStarted() || !isDurationDetermined() ) {
				return 0;
			}
			if ( isAtPlaybackEnd() ) {
				return 1.0;
			}
			return _xNetStream.time / _nDuration;
		}
		
		public function getPercentLoaded():Number {
			if ( !isLoadStarted() || isLoadingForAutoplayShowFrame() ) {
				return 0;
			}
			if ( isStreaming() ) {
				return 0;
			}
			return _xNetStream.bytesLoaded / _xNetStream.bytesTotal;
		}
		
		public function getBytesLoaded():Number {
			if ( !isLoadStarted() || isLoadingForAutoplayShowFrame() ) {
				return 0;
			}
			if ( isStreaming() ) {
				return 0;
			}
			return _xNetStream.bytesLoaded;
		}
		
		public function getBytesTotal():Number {
			if ( !isLoadStarted() || isLoadingForAutoplayShowFrame() ) {
				return 0;
			}
			if ( isStreaming() ) {
				return 0;
			}
			return _xNetStream.bytesTotal;
		}
		
		public function getElapsedTime():Number {
			if ( isAtPlaybackEnd() ) {
				return getDuration();
			}
			else {
				return getElapsedTimeInternal();
			}
		}
		
		public function getTimeRemaining():Number {
			return getDuration() - getElapsedTime();
		}
		
		private function getElapsedTimeInternal():Number {
			if ( !isLoadStarted() || !isDurationDetermined() ) {
				return 0;
			}
			return _xNetStream.time;
		}
		
		public function getLastSeekPercent():Number { return _nLastSeekPercent; }
		
		public function getLastSeekTime():Number { return getLastSeekPercent() * getDuration(); }
		
		public function getLastSeekTimeRemaining():Number { return getDuration() - getLastSeekTime(); }
		
		public function getDuration():Number {
			if ( !isLoadStarted() || !isDurationDetermined() ) {
				return 0;
			}
			return _nDuration;
		}
		
		public function isDurationDetermined():Boolean {
			return _bVideoDurationDetermined;
		}
		
		public function isVideoSizeDetermined():Boolean {
			return _bVideoSizeDetermined;
		}
		
		public function getVideoWidth():Number {
			if ( _bVideoSizeDetermined ) {
				return _nVideoWidth;
			}
			else {
				return _xVideo.videoWidth;
			}
		}
		
		public function getVideoHeight():Number {
			if ( _bVideoSizeDetermined ) {
				return _nVideoHeight;
			}
			else {
				return _xVideo.videoHeight;
			}
		}
		
		public function getVideoDisplayWidth():Number {
			return _xVideo.width * scaleX;
		}
		
		public function getVideoDisplayHeight():Number {
			return _xVideo.height * scaleY;
		}
		
		public function isFullScreen():Boolean { return _bFullScreen; }
		
		public function isMuted():Boolean { return _bMuted; }
		
		public function getVolume():Number { return _nVolume; }
		
		private function init():void {
			_bAutoplay = true;
			_bAutoload = true;
			_bNoAutoplayShowFrame = false;
			_nNoAutoplayShowFrameOffset = 0;
			_bRewindOnStop = false;
			_nBufferTime = 2;
			_nVolume = 1;
			
			initShowHideListeners();
			
			_bFullScreen = false;
			_bPreserveAspectRatio = true;
			_nReportingInterval = 10;
			
			_xSizeHolder = new Sprite();
			_xSizeHolder.graphics.beginFill(0x000000, 1);
			_xSizeHolder.graphics.drawRect(0, 0, 500, 500);
			_xSizeHolder.graphics.endFill();
			_xSizeHolder.alpha = 0;
			addChild(_xSizeHolder);
			
			_xNetConnection = new NetConnection();
			//_xNetConnection.objectEncoding = ObjectEncoding.AMF0;
			var l_xConnectionClient:Object = new Object();
			l_xConnectionClient._xPlayerVideo = this;
			l_xConnectionClient.onBWDone = function():void {
				
			}
			_xNetConnection.client = l_xConnectionClient;
			_xNetConnection.addEventListener(NetStatusEvent.NET_STATUS, onNetConnectionStatus);
			_xNetConnection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
			_xVideo = new Video();
			addChild(_xVideo);
			_xTimer = new Timer(_nReportingInterval, 0);
			_xTimer.addEventListener(TimerEvent.TIMER, onTimerUpdate);
			
			_xTunnelingNetConnection = new NetConnection();
			//_xTunnelingNetConnection.objectEncoding = ObjectEncoding.AMF0;
			var l_xTunnelingConnectionClient:Object = new Object();
			l_xTunnelingConnectionClient._xPlayerVideo = this;
			l_xTunnelingConnectionClient.onBWDone = function():void {
				
			}
			_xTunnelingNetConnection.client = l_xTunnelingConnectionClient;
			_xTunnelingNetConnection.addEventListener(NetStatusEvent.NET_STATUS, onTunnelingNetConnectionStatus);
			_xTunnelingNetConnection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
			
			initPlaybackFields();
		}
		
		private function initShowHideListeners():void {
			addPlaybackStartedListener(onPlaybackStarted);
			addPlaybackResumedListener(onPlaybackResumed);
			addPlaybackPausedListener(onPlaybackPaused);
			addPlaybackStoppedListener(onPlaybackStopped);
			addRewindPerformedListener(onRewindPerformed);
			addElapsedTimeUpdatedListener(onElapsedTimeUpdated);
			addMediaOpenedListener(onVideoOpened);
			addMediaNotFoundListener(onVideoNotFound);
			addPlaybackPausedForAutoplayListener(onPlaybackPausedForAutoplay);
		}
		
		private function onPlaybackStarted(p_xEvent:Event):void {
			if ( isAutoplay() && !isPaused() ) {
				dispatchPlayerEvent(SHOW_PAUSE);
			}
		}
		
		private function onPlaybackResumed(p_xEvent:Event):void {
			dispatchPlayerEvent(SHOW_PAUSE);
		}
		
		private function onPlaybackPaused(p_xEvent:Event):void {
			dispatchPlayerEvent(SHOW_PLAY);
		}
		
		private function onPlaybackStopped(p_xEvent:Event):void {
			if ( !isRewindOnStop() ) {
				dispatchPlayerEvent(SHOW_REPLAY);
				dispatchPlayerEvent(SHOW_POST_ROLL);
				_bPostVideoOrnamentsShown = true;
			}
			else {
				rewind();
				dispatchPlayerEvent(SHOW_PRE_ROLL);
				_bPreVideoOrnamentsShown = true;
			}
		}
		
		private function onElapsedTimeUpdated(p_xEvent:Event):void {
			if ( !isAtPlaybackEnd() ) {
				if ( _bPostVideoOrnamentsShown ) {
					dispatchPlayerEvent(HIDE_REPLAY);
					dispatchPlayerEvent(HIDE_POST_ROLL);
					_bPostVideoOrnamentsShown = false;
				}
			}
			if ( !isAtPlaybackStart() && !isLoadingForAutoplayShowFrame() ) {
				if ( _bPreVideoOrnamentsShown ) {
					dispatchPlayerEvent(HIDE_PRE_ROLL);
					dispatchPlayerEvent(HIDE_PUSH_TO_PLAY);
					_bPreVideoOrnamentsShown = false;
				}
			}
		}
		
		private function onRewindPerformed(p_xEvent:Event):void {
			dispatchPlayerEvent(SHOW_PRE_ROLL);
			_bPreVideoOrnamentsShown = true;
		}
		
		private function onVideoOpened(p_xEvent:Event):void {
			dispatchPlayerEvent(HIDE_REPLAY);
			dispatchPlayerEvent(HIDE_POST_ROLL);
			_bPostVideoOrnamentsShown = false;
			if ( !isAutoplay() ) {
				if ( !isNoAutoplayShowFrame() ) {
					dispatchPlayerEvent(SHOW_PRE_ROLL);
				}
				dispatchPlayerEvent(SHOW_PUSH_TO_PLAY);
			}
		}
		
		private function onPlaybackPausedForAutoplay(p_xEvent:Event):void {
			if ( isStreaming() ) {
				_bLoadBuffering = false;
				dispatchPlayerEvent(BUFFERING_STOPPED);
			}
			if ( !isNoAutoplayShowFrame() ) {
				dispatchPlayerEvent(SHOW_PRE_ROLL);
			}
			dispatchPlayerEvent(SHOW_PUSH_TO_PLAY);
			_bPreVideoOrnamentsShown = true;
		}
		
		private function onVideoNotFound(p_xEvent:Event):void {
			if ( isAtPlaybackEnd() ) {
				dispatchPlayerEvent(HIDE_REPLAY);
				dispatchPlayerEvent(HIDE_POST_ROLL);
				_bPostVideoOrnamentsShown = false;
			}
			dispatchPlayerEvent(HIDE_PRE_ROLL);
			dispatchPlayerEvent(HIDE_PUSH_TO_PLAY);
			_bPreVideoOrnamentsShown = false;
		}
		
		public function setExpediteStreamTunneling(p_bExpediteStreamTunneling:Boolean):void {
			_bExpediteStreamTunneling = p_bExpediteStreamTunneling;
		}
		
		private function initNetStream(p_xNetConnection:NetConnection):void {
			_xNetStream = new NetStream(p_xNetConnection);
			_xNetStream.bufferTime = _nBufferTime;;
			var l_xMetaDataClient:Object = new Object();
			l_xMetaDataClient._xPlayerVideo = this;
			l_xMetaDataClient.onMetaData = function(p_xMetaData:Object):void {
				this._xPlayerVideo.onMetaData(p_xMetaData);
			}
			_xNetStream.client = l_xMetaDataClient;
			_xNetStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStreamStatus);
			if ( isMuted() ) {
				setStreamVolume(0);
			}
			else {
				setStreamVolume(_nVolume);
			}
			_xVideo.attachNetStream(_xNetStream);
		}
		
		public function setReportingInterval(p_nInterval:int):void {
			_nReportingInterval = p_nInterval;
			if ( _xTimer != null ) {
				_xTimer.delay = _nReportingInterval;
			}
		}
		
		public function setBufferTime(p_nBufferTime:Number):void {
			_nBufferTime = p_nBufferTime;
			if ( _xNetStream != null ) {
				_xNetStream.bufferTime = _nBufferTime;
			}
		}
		
		private function initPlaybackFields():void {
			_nDuration = 0;
			_nLastBytesLoaded = -1;
			_nLastElapsedTime = -1;
			_bVideoSizeDetermined = false;
			_bVideoDurationDetermined = false;
			_nVideoWidth = 0;
			_nVideoHeight = 0;
			_bFirstPlay = true;
			_bPausedForAutoplay = false;
			_bPerformingAutoplaySeek = false;
			_bVideoLoadStarted = false;
			_bVideoLoadCompleted = false;
			_bPaused = false;
			_bAtPlaybackStart = true;
			_bAtPlaybackEnd = false;
			_bPlaybackBuffering = false;
			_bLoadBuffering = false;
			_bSeeking = false;
			_nLastSeekPercent = 0;
			_bPlayingAtSeekStart = false;
			_bStreaming = false;
		}

		public function setRewindOnStop(p_bRewindOnStop:Boolean):void {
			_bRewindOnStop = p_bRewindOnStop;
		}
		
		public function setAutoplay(p_bAutoplay:Boolean):void {
			_bAutoplay = p_bAutoplay;
		}
		
		public function setAutoload(p_bAutoload:Boolean):void {
			_bAutoload = p_bAutoload;
		}

		public function setNoAutoplayShowFrame(p_bNoAutoplayShowFrame:Boolean):void {
			_bNoAutoplayShowFrame = p_bNoAutoplayShowFrame;
		}
		
		public function setNoAutoplayShowFrameOffset(p_nOffset:Number):void {
			_nNoAutoplayShowFrameOffset = p_nOffset;
		}

		public function clear():void {
			if ( isLoadStarted() ) {
				_xTimer.stop();
				if ( _xNetConnection != null ) {
					_xNetConnection.close();
				}
				if ( _xTunnelingNetConnection != null ) {
					_xTunnelingNetConnection.close();
				}
				if ( _xNetStream != null ) {
					var l_xMetaDataClient:Object = new Object();
					l_xMetaDataClient.onMetaData = function(p_xMetaData:Object):void {}
					_xNetStream.client = l_xMetaDataClient;
					_xNetStream.removeEventListener(NetStatusEvent.NET_STATUS, onNetStreamStatus);
					_xNetStream.close();
					_xNetStream = null;
				}
				_xVideo.clear();
				_sVideoFile = null;
				_sStreamUrl = null;
				_sTunnelingStreamUrl = null;
				_sStreamFile = null;
				
				var l_bWasBuffering:Boolean = isBuffering();
				
				initPlaybackFields();
				
				if ( l_bWasBuffering ) {
					dispatchPlayerEvent(BUFFERING_STOPPED);
				}
				
				dispatchPlayerEvent(MEDIA_CLOSED);
			}
			else {
				initPlaybackFields();
			}
		}
		
		public function play(p_sVideoUrl:String, p_sVideoFileForStreaming:String = null):void {
			clear();
			if ( p_sVideoUrl.substring(0,4) == 'rtmp' ) {
				if ( p_sVideoFileForStreaming == null ) {
					trace ( 'ERROR: video file parameter not specified streaming video!' );
					return;
				}
				if ( p_sVideoFileForStreaming.substr(-4) == '.mp4' || p_sVideoFileForStreaming.substr(-4) == '.mov' || 
					 p_sVideoFileForStreaming.substr(-4) == '.aac' || p_sVideoFileForStreaming.substr(-4) == '.m4a' ) {
					_sStreamFile = 'mp4:' + p_sVideoFileForStreaming.substr(0,p_sVideoFileForStreaming.length-4);
					_sStreamUrl = p_sVideoUrl;
					_bStreaming = true;
					updateTunnelingStreamUrl();
				}
				else if ( p_sVideoFileForStreaming.substr(-4) == '.flv' ) {
					_sStreamFile = p_sVideoFileForStreaming.substr(0,p_sVideoFileForStreaming.length-4);
					_sStreamUrl = p_sVideoUrl;
					_bStreaming = true;
					updateTunnelingStreamUrl();
				}
				else {
					trace ( 'ERROR: unrecognized video file extension for streaming video!' );
					return;
				}
			}
			else {
				_sVideoFile = p_sVideoUrl;
			}
			_bAtPlaybackStart = true;
			_bPreVideoOrnamentsShown = true;
			if ( isAutoload() || isNoAutoplayShowFrame() ) {
				if ( isNoAutoplayShowFrame() ) {
					setStreamVolume(0);
				}
				loadAndPlayVideo();
			}
			else {
				dispatchPlayerEvent(PLAYBACK_PAUSED_FOR_AUTOPLAY);
			}
		}
		
		private function updateTunnelingStreamUrl():void {
			if ( _sStreamUrl.substr(0, 5) == "rtmp:" && _sStreamUrl.indexOf(":80") == -1 ) {
				var l_sUrlWithoutProtocol:String = _sStreamUrl.substring(7, _sStreamUrl.length);
				var l_nFirstSlashIndex:int = l_sUrlWithoutProtocol.indexOf("/");
				_sTunnelingStreamUrl = "rtmpt://" + l_sUrlWithoutProtocol.substring(0, l_nFirstSlashIndex) + ":80" +
					l_sUrlWithoutProtocol.substring(l_nFirstSlashIndex, l_sUrlWithoutProtocol.length);
				//trace ( 'tunneling stream url: ' + _sTunnelingStreamUrl );
			}
			else {
				_sTunnelingStreamUrl = null;
			}
		}
		
		private function loadAndPlayVideo():void {
			_bVideoLoadStarted = true;
			_bNetStreamConnected = false;
			if ( isStreaming() ) {
				_xNetConnection.connect(_sStreamUrl);
				if ( isExpediteStreamTunneling() && getTunnelingStreamUrl() != null ) {
					_xTunnelingNetConnection.connect(_sTunnelingStreamUrl);
				}
			}
			else {
				_xNetConnection.connect(null);
			}
			_bLoadBuffering = true;
			dispatchPlayerEvent(BUFFERING_STARTED);
		}
		
		public function pause():void {
			if ( !isLoadStarted() || isPaused() ) {
				return;
			}
			_bPaused = true;
			_xNetStream.pause();
			dispatchPlayerEvent(PLAYBACK_PAUSED);
		}
		
		public function resume():void {			
			if ( (isLoadStarted() && !isNoAutoplayShowFrame()) && !isPaused() ) {
				return;
			}
			if ( isAtPlaybackEnd() ) {
				return;
			}
			if ( !isLoadStarted() ) {
				loadAndPlayVideo();
			}
			if ( isStreaming() ) {
				_bPlaybackBuffering = true;
				dispatchPlayerEvent(BUFFERING_STARTED);
			}
			_bPaused = false;
			_bPausedForAutoplay = false;
			_xNetStream.resume();
			dispatchPlayerEvent(PLAYBACK_RESUMED);
		}
		
		public function startSeeking():void {
			_bPlayingAtSeekStart = !isPaused();
			_bSeeking = true;
		}
		
		public function stopSeeking():void { _bSeeking = false; }
		
		public function rewind():void {
			seekToTime(0);
			_bAtPlaybackStart = true;
			_bAtPlaybackEnd = false;
			dispatchPlayerEvent(REWIND_PERFORMED);
			pause();
		}
		
		public function seekToTime(p_nTime:Number):void {
			performSeek(p_nTime, Math.floor(_nDuration) == 0 ? 0 : p_nTime / Math.floor(_nDuration));
		}
		
		public function seekToPercent(p_nPercent:Number):void {
			performSeek(p_nPercent * Math.floor(_nDuration), p_nPercent);
		}
		
		private function performSeek(p_nTime:Number, p_nPercent:Number):void {
			if ( !isLoadStarted() ) {
				return;
			}
			if ( isAtPlaybackEnd() && p_nTime >= Math.floor(_nDuration) ) {
				return;
			}
			if ( isAtPlaybackStart() && p_nTime == 0 ) {
				return;
			}
			if ( p_nTime < 0 ) {
				p_nTime = 0;
				p_nPercent = 0;
			}
			if ( p_nTime > Math.floor(_nDuration) ) {
				p_nTime = Math.floor(_nDuration);
				p_nPercent = 1;
			}
			if ( isAtPlaybackStart() && p_nTime > 0 ) {
				_bAtPlaybackStart = false;
			}
			_nLastSeekPercent = p_nPercent;
			_xNetStream.seek(p_nTime);
			dispatchPlayerEvent(SEEK_PERFORMED);
			if ( isSeeking() && _bPlayingAtSeekStart && isPaused() ) {
				resume();
			}
			updateBuffer();
		}
		
		private function updateBuffer():void {
			if ( isLoadCompleted() ) {
				return;
			}
			if ( isPlaybackBuffering() && _xNetStream.bufferLength >= _xNetStream.bufferTime ) {
				_bPlaybackBuffering = false;
				if ( !isLoadBuffering() ) {
					dispatchPlayerEvent(BUFFERING_STOPPED);
				}
			}
			else if ( !isPlaybackBuffering() && _xNetStream.bufferLength < _xNetStream.bufferTime && !isAtPlaybackEnd() &&
					  !(isStreaming() && isPaused()) ) {
				_bPlaybackBuffering = true;
				if ( !isLoadBuffering() ) {
					dispatchPlayerEvent(BUFFERING_STARTED);
				}
			}
		}
		
		public function goFullScreen():void {
			_nPreFullScreenWidth = _xSizeHolder.width;
			_nPreFullScreenHeight = _xSizeHolder.height;
			_nPreFullScreenX = x;
			_nPreFullScreenY = y;
			_xPreFullScreenParent = parent;
			_nPreFullScreenParentChildIndex = parent.getChildIndex(this);
			var l_xStage:Stage = stage;
			parent.removeChild(this);
			l_xStage.addChild(this);
			l_xStage.addEventListener(FullScreenEvent.FULL_SCREEN, onFullScreen);
			l_xStage.displayState = StageDisplayState.FULL_SCREEN;
		}
		
		public function exitFullScreen():void {
			stage.displayState = StageDisplayState.NORMAL;
		}
		
		private function onFullScreen(p_xEvent:FullScreenEvent):void {
			if ( !isFullScreen() && p_xEvent.fullScreen ) {
				_bFullScreen = true;
				_xSizeHolder.width = stage.stageWidth;
				_xSizeHolder.height = stage.stageHeight;
				_xSizeHolder.alpha = 1;
				x = 0;
				y = 0;
				updateVideoSize();
				dispatchPlayerEvent(FULL_SCREEN_ENTERED);
			}
			else if ( isFullScreen() && !p_xEvent.fullScreen ) {
				_bFullScreen = false;
				parent.removeChild(this);
				_xPreFullScreenParent.addChildAt(this, _nPreFullScreenParentChildIndex);
				_xSizeHolder.width = _nPreFullScreenWidth;
				_xSizeHolder.height = _nPreFullScreenHeight;
				_xSizeHolder.alpha = 0;
				x = _nPreFullScreenX;
				y = _nPreFullScreenY;
				updateVideoSize();
				dispatchPlayerEvent(FULL_SCREEN_EXITED);
				stage.removeEventListener(FullScreenEvent.FULL_SCREEN, onFullScreen);
			}
		}

		public function setMuted(p_bMuted:Boolean) : void {
			if ( p_bMuted ) {
				mute();
			}
			else {
				unmute();
			}
		}
		
		public function mute() : void {
			if ( isMuted() ) {
				return;
			}
			_bMuted = true;
			setStreamVolume(0);
			dispatchPlayerEvent(VOLUME_MUTED);
		}
		
		public function unmute() : void {
			if ( !isMuted() ) {
				return;
			}
			_bMuted = false;
			setStreamVolume(_nVolume);
			dispatchPlayerEvent(VOLUME_UNMUTED);
		}
		
		private function setStreamVolume(p_nVolume:Number) : void {
			_xNetStream.soundTransform = new SoundTransform(p_nVolume);
		}
		
		public function setVolume(p_nVolume:Number) : void {
			if ( p_nVolume == getVolume() ) {
				return;
			}
			if ( p_nVolume < 0 ) {
				p_nVolume = 0;
			}
			if ( p_nVolume > 1 ) {
				p_nVolume = 1;
			}
			_nVolume = p_nVolume;
			if ( !isMuted() ) {
				setStreamVolume(_nVolume);
			}
			dispatchPlayerEvent(VOLUME_CHANGED);
		}
		
		public function setVideoDisplaySize(p_nWidth:Number, p_nHeight:Number):void {
			if ( p_nWidth <= 0 || p_nHeight <= 0 ) {
				return;
			}
			if ( p_nWidth > _xSizeHolder.width ) { 
				p_nWidth = _xSizeHolder.width; }
			if ( p_nHeight > _xSizeHolder.height ) {
				p_nHeight = _xSizeHolder.height;
			}
			_xVideo.width = p_nWidth / scaleX;
			_xVideo.height = p_nHeight / scaleY;
			_xVideo.x = ((_xSizeHolder.width - p_nWidth) / 2.0) / scaleX;
			_xVideo.y = ((_xSizeHolder.height - p_nHeight) / 2.0) / scaleY;
			dispatchPlayerEvent(SIZE_CHANGED);
		}
		
		public function setVideoSmoothing(p_bSmoothing:Boolean):void {
			_xVideo.smoothing = p_bSmoothing;
		}
		
		private function updateVideoSize():void {
			if ( isPreserveAspectRatio() && isVideoSizeDetermined() ) {
				var l_aWidthAndHeight:Array = AspectUtils.getResizedDimensions(
					getVideoWidth(), getVideoHeight(), _xSizeHolder.width, _xSizeHolder.height);
				setVideoDisplaySize(l_aWidthAndHeight[0], l_aWidthAndHeight[1]);
			}
			else {
				setVideoDisplaySize(_xSizeHolder.width, _xSizeHolder.height);
			}
		}
		
		public function setPreserveAspectRatio(p_bPreserveAspectRatio:Boolean):void {
			_bPreserveAspectRatio = p_bPreserveAspectRatio;
			updateVideoSize();
		}
		
		public override function set width(p_nWidth:Number):void {
			_xSizeHolder.width = p_nWidth;
			updateVideoSize();
		}
		
		public override function set height(p_nHeight:Number):void {
			_xSizeHolder.height = p_nHeight;
			updateVideoSize();
		}
		
		private function onSecurityError(p_xEvent:SecurityErrorEvent):void {
			trace ( 'ERROR: Security error occurred while connecting video player');
		}
		
		private function onMetaData(p_xMetaData:Object):void {
			if ( getDuration() == 0 ) {
				_nDuration = p_xMetaData.duration;
				_nVideoWidth = p_xMetaData.width;
				_nVideoHeight = p_xMetaData.height;
				
				if ( isNaN(_nDuration) ) {
					dispatchPlayerEvent(DURATION_NOT_DETERMINED);
				}
				else {
					_bVideoDurationDetermined = true;
					dispatchPlayerEvent(DURATION_DETERMINED);
				}
				if ( !isVideoSizeDetermined() ) {
					if ( isNaN(_nVideoWidth) || isNaN(_nVideoHeight) ) {
						dispatchPlayerEvent(DIMENSIONS_NOT_DETERMINED);
					}
					else {
						_bVideoSizeDetermined = true;
						updateVideoSize();
						dispatchPlayerEvent(DIMENSIONS_DETERMINED);
					}
				}
			}
		}
		
		private function onNetConnectionStatus(p_xNetStatus:Object):void {
			//trace ( '****** onNetConnectionStatus: ' + p_xNetStatus.info.code );
			switch ( p_xNetStatus.info.code ) {
				case "NetConnection.Connect.Success":
					if ( isStreaming() ) {
						if ( _bNetStreamConnected ) {
							return;
						}
						_bNetStreamConnected = true;
						initNetStream(_xNetConnection);
						_xTimer.start();
						dispatchPlayerEvent(MEDIA_OPENED);
						_xNetStream.play(_sStreamFile);
					}
					else {
						initNetStream(_xNetConnection);
						_xTimer.start();
						dispatchPlayerEvent(MEDIA_OPENED);
						_xNetStream.play(_sVideoFile);
					}
					break;
			}
		}
		
		private function onTunnelingNetConnectionStatus(p_xNetStatus:Object):void {
			//trace ( '****** onTunnelingNetConnectionStatus: ' + p_xNetStatus.info.code );
			switch ( p_xNetStatus.info.code ) {
				case "NetConnection.Connect.Success":
					if ( isStreaming() ) {
						if ( _bNetStreamConnected ) {
							return;
						}
						_bNetStreamConnected = true;
						initNetStream(_xTunnelingNetConnection);
						_xTimer.start();
						dispatchPlayerEvent(MEDIA_OPENED);
						_xNetStream.play(_sStreamFile);
					}
					break;
			}
		}
		
		private function onNetStreamStatus(p_xNetStatus:Object):void {
			//trace ( '****** onNetStreamStatus: ' + p_xNetStatus.info.code + _xNetStream.bufferLength + ' ' + _xNetStream.bufferTime);
			switch ( p_xNetStatus.info.code ) {
				case "NetStream.Play.StreamNotFound":
					dispatchPlayerEvent(MEDIA_NOT_FOUND);
					break;
				case "NetStream.Play.Start":
					if ( isAtPlaybackStart() && !isLoadingForAutoplayShowFrame() ) {
						dispatchPlayerEvent(PLAYBACK_STARTED);
					}
					break;
				case "NetStream.Play.Stop":
					if ( isStreaming() ) {
						// flash fires this for streaming videos when the whole video is buffered, even if playback is paused
						if ( getDuration() > 0 ) {
							if ( getElapsedTimeInternal() / getDuration() > 0.99 ) {
								if ( !isAtPlaybackEnd() ) {
									pause();
									if ( !isLoadingForAutoplayShowFrame() ) {
										_bAtPlaybackEnd = true;
									}
									if ( isPlaybackBuffering() ) {
										_bPlaybackBuffering = false;
										if ( !isLoadBuffering() ) {
											dispatchPlayerEvent(BUFFERING_STOPPED);
										}
									}
									if ( _nLastElapsedTime != getDuration() ) {
										if ( _bPerformingAutoplaySeek ) {
											_bPerformingAutoplaySeek = false;
										}
										else {
											dispatchPlayerEvent(ELAPSED_TIME_UPDATED);
										}
										_nLastElapsedTime = getDuration();
									}
									if ( !isLoadingForAutoplayShowFrame() ) {
										dispatchPlayerEvent(PLAYBACK_STOPPED);
									}
								}
							}
						}
					}
					else if ( !isAtPlaybackEnd() ) {
						pause();
						if ( !isLoadingForAutoplayShowFrame() ) {
							_bAtPlaybackEnd = true;
						}
						if ( isPlaybackBuffering() ) {
							_bPlaybackBuffering = false;
							if ( !isLoadBuffering() ) {
								dispatchPlayerEvent(BUFFERING_STOPPED);
							}
						}
						if ( _nLastElapsedTime != getDuration() ) {
							if ( _bPerformingAutoplaySeek ) {
								_bPerformingAutoplaySeek = false;
							}
							else {
								dispatchPlayerEvent(ELAPSED_TIME_UPDATED);
							}
							_nLastElapsedTime = getDuration();
						}
						if ( !isLoadingForAutoplayShowFrame() ) {
							dispatchPlayerEvent(PLAYBACK_STOPPED);
						}
					}
					break;
				case "NetStream.Buffer.Empty":
					if ( isStreaming() && getElapsedTimeInternal() / getDuration() > 0.99 ) {
						if ( !isAtPlaybackEnd() ) {
							pause();
							if ( !isLoadingForAutoplayShowFrame() ) {
								_bAtPlaybackEnd = true;
							}
							if ( isPlaybackBuffering() ) {
								_bPlaybackBuffering = false;
								if ( !isLoadBuffering() ) {
									dispatchPlayerEvent(BUFFERING_STOPPED);
								}
							}
							if ( _nLastElapsedTime != getDuration() ) {
								if ( _bPerformingAutoplaySeek ) {
									_bPerformingAutoplaySeek = false;
								}
								else {
									dispatchPlayerEvent(ELAPSED_TIME_UPDATED);
								}
								_nLastElapsedTime = getDuration();
							}
							if ( !isLoadingForAutoplayShowFrame() ) {
								dispatchPlayerEvent(PLAYBACK_STOPPED);
							}
						}
					}
					else if ( !isLoadCompleted() && !isPlaybackBuffering() ) {
						_bPlaybackBuffering = true;
						if ( !isLoadBuffering() ) {
							dispatchPlayerEvent(BUFFERING_STARTED);
						}
					}
					break;
				case "NetStream.Buffer.Full":
					if ( isPlaybackBuffering() ) {
						_bPlaybackBuffering = false;
						if ( !isLoadBuffering() ) {
							dispatchPlayerEvent(BUFFERING_STOPPED);
						}
					}
					break;
				case "NetStream.Seek.Notify":
					if ( isAtPlaybackEnd()  && getElapsedTimeInternal() < getDuration() ) {
						_bAtPlaybackEnd = false;
					}
					break;
				case "NetStream.Seek.InvalidTime":
					if ( !isAtPlaybackEnd() && isLoadCompleted() ) {
						pause();
						if ( !isLoadingForAutoplayShowFrame() ) {
							_bAtPlaybackEnd = true;
						}
						if ( isPlaybackBuffering() ) {
							_bPlaybackBuffering = false;
							if ( !isLoadBuffering() ) {
								dispatchPlayerEvent(BUFFERING_STOPPED);
							}
						}
						if ( _nLastElapsedTime != getDuration() ) {
							if ( _bPerformingAutoplaySeek ) {
								_bPerformingAutoplaySeek = false;
							}
							else {
								dispatchPlayerEvent(ELAPSED_TIME_UPDATED);
							}
							_nLastElapsedTime = getDuration();
						}
						if ( !isLoadingForAutoplayShowFrame() ) {
							dispatchPlayerEvent(PLAYBACK_STOPPED);
						}
					}
					break;
			}
		}
		
		private function updateDimensions():void {
			if ( !isVideoSizeDetermined() && !isAtPlaybackStart() && isLoadStarted() && _xVideo.videoWidth > 0 && _xVideo.videoHeight > 0 ) {
				_bVideoSizeDetermined = true;
				_nVideoWidth = _xVideo.videoWidth;
				_nVideoHeight = _xVideo.videoHeight;
				updateVideoSize();
				dispatchPlayerEvent(DIMENSIONS_DETERMINED);
			}
		}
		
		private function updateAutoplay():void {
			if ( _bFirstPlay && !isAutoplay() && getDuration() > 0 && getElapsedTime() > 0 && 
				 ( !isNoAutoplayShowFrame() || getElapsedTime() >= getNoAutoplayShowFrameOffset() ) ) {
				_bFirstPlay = false;
				setStreamVolume(_nVolume);
				if ( isAutoload() || isNoAutoplayShowFrame() ) {
					_bPausedForAutoplay = true;
					pause();
					dispatchPlayerEvent(PLAYBACK_PAUSED_FOR_AUTOPLAY);
					_bPerformingAutoplaySeek = true;
					if ( isNoAutoplayShowFrame() ) {
						seekToTime(getNoAutoplayShowFrameOffset());
					}
					else {
						rewind();
					}
					if ( !isAutoload() ) {
						_xTimer.stop();
						_xNetStream.close();
						if ( isBuffering() ) {
							_bLoadBuffering = false;
							_bPlaybackBuffering = false;
							dispatchPlayerEvent(BUFFERING_STOPPED);
						}
						_bVideoLoadStarted = false;
						_bVideoLoadCompleted = false;
						_bPlaybackBuffering = false;
						_bLoadBuffering = false;
						_bSeeking = false;
						_nLastSeekPercent = 0;
						_bPlayingAtSeekStart = false;
						dispatchPlayerEvent(MEDIA_CLOSED);
					}
				}
			}
		}
		
		private function updateElapsedTime():void {
			if ( ( !_bFirstPlay || isAutoplay() ) && !isPausedForAutoplay() && _nLastElapsedTime != getElapsedTime() ) {
				if ( _nLastElapsedTime == 0 ) {
					_bLoadBuffering = false;
					if ( !isPlaybackBuffering() ) {
						dispatchPlayerEvent(BUFFERING_STOPPED);
					}
				}
				if ( isAtPlaybackStart() && getElapsedTime() > 0 ) {
					_bAtPlaybackStart = false;
				}
				if ( isAtPlaybackEnd()  && getElapsedTimeInternal() < getDuration() ) {
					_bAtPlaybackEnd = false;
				}
				if ( _bPerformingAutoplaySeek ) {
					_bPerformingAutoplaySeek = false;
				}
				else {
					dispatchPlayerEvent(ELAPSED_TIME_UPDATED);
				}
				_nLastElapsedTime = getElapsedTime();
			}
		}
		
		private function updateBytesLoaded():void {
			if ( !isStreaming() && ( isAutoload() || isAutoplay() || ( !_bFirstPlay && !isPausedForAutoplay() ) ) && _nLastBytesLoaded != getBytesLoaded() ) {
				dispatchPlayerEvent(LOAD_PROGRESS_UPDATED);
				if ( !isLoadCompleted() && getPercentLoaded() == 1 ) {
					if ( isBuffering() ) {
						_bLoadBuffering = false;
						_bPlaybackBuffering = false;
						dispatchPlayerEvent(BUFFERING_STOPPED);
					}
					_bVideoLoadCompleted = true;
					dispatchPlayerEvent(LOAD_COMPLETED);
					_nLastBytesLoaded = getBytesLoaded();
				}
			}
		}
		
		private function updateBuffering():void {
			if ( isLoadBuffering() && isLoadStarted() && !isPausedForAutoplay() && getElapsedTime() > 0 ) {
				if ( isLoadBuffering() ) {
					_bLoadBuffering = false;
					if ( !isPlaybackBuffering() ) {
						dispatchPlayerEvent(BUFFERING_STOPPED);
					}
				}
			}
			if ( !isLoadCompleted() && isPaused() && isPlaybackBuffering() && _xNetStream.bufferLength >= _xNetStream.bufferTime ) {
				_bPlaybackBuffering = false;
				dispatchPlayerEvent(BUFFERING_STOPPED);
			}
		}
		
		private function onTimerUpdate(p_xEvent:TimerEvent):void {
			if ( !isLoadStarted() ) {
				return;
			}
			updateDimensions();
			updateAutoplay();
			updateElapsedTime();
			updateBytesLoaded();
			updateBuffering();
		}
	}
}