package com.codeandtheory.core.components.audio.test {
	import com.codeandtheory.core.components.audio.controls.AudioPlayerController;
	
	import flash.display.Sprite;
	
	public class Tester extends Sprite {
		
		private var _xAudioPlayer:AudioPlayerController;
		
		public function Tester() {
			_xAudioPlayer = new AudioPlayerController();
			
			var l_xPlay:Sprite = new Sprite();
			l_xPlay.graphics.beginFill(0xff0000, 1);
			l_xPlay.graphics.drawRect(0,0,10,10);
			l_xPlay.x = 5;
			l_xPlay.y = 5;
			
			var l_xPause:Sprite = new Sprite();
			l_xPause.graphics.beginFill(0x00ff00, 1);
			l_xPause.graphics.drawRect(0,0,10,10);
			l_xPause.x = 20;
			l_xPause.y = 5;
			
			var l_xMute:Sprite = new Sprite();
			l_xMute.graphics.beginFill(0x0000ff, 1);
			l_xMute.graphics.drawRect(0,0,10,10);
			l_xMute.x = 35;
			l_xMute.y = 5;
			
			var l_xUnmute:Sprite = new Sprite();
			l_xUnmute.graphics.beginFill(0xcc00cc, 1);
			l_xUnmute.graphics.drawRect(0,0,10,10);
			l_xUnmute.x = 50;
			l_xUnmute.y = 5;
			
			addChild(l_xPlay);
			addChild(l_xPause);
			addChild(l_xMute);
			addChild(l_xUnmute);
			
			_xAudioPlayer.attachTogglePauseButton(l_xPause, l_xPlay);
			_xAudioPlayer.attachToggleMuteButton(l_xMute, l_xUnmute);
			
			_xAudioPlayer.play('http://candt.interview/files/test.mp3');
		}
	}
}