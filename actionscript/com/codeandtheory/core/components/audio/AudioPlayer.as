package com.codeandtheory.core.components.audio {
	import com.codeandtheory.core.components.media.MediaPlayerEvents;
	import com.codeandtheory.core.utils.TimeUtils;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundLoaderContext;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	public class AudioPlayer extends MediaPlayerEvents {
		
		private var _xSound:Sound = null;
		private var _xSoundChannel:SoundChannel = null;
		private var _sAudioFile:String = null;
		private var _bSeeking:Boolean;
		private var _bPlayingAtSeekStart:Boolean;
		private var _nLastSeekPercent:Number;
		private var _bFirstPlay:Boolean;
		private var _bAutoload:Boolean;
		private var _bAutoplay:Boolean;
		private var _bPaused:Boolean;
		private var _bPausedForAutoplay:Boolean;
		private var _nDuration:Number;
		private var _bAtPlaybackStart:Boolean;
		private var _bAtPlaybackEnd:Boolean;
		private var _bLoadStarted:Boolean;
		private var _bLoadCompleted:Boolean;
		private var _bPlaybackBuffering:Boolean;
		private var _bLoadBuffering:Boolean;
		private var _nVolume:Number;
		private var _bMuted:Boolean;
		private var _bDurationDetermined:Boolean;
		private var _bDurationEstimated:Boolean;
		private var _xTimer:Timer;
		private var _nReportingInterval:int;
		private var _nLastBytesLoaded:Number;
		private var _nLastElapsedTime:Number;
		private var _bRewindOnStop:Boolean;
		private var _nLastSeekTimeWhilePaused:Number;
		
		public function AudioPlayer() {
			init();
		}
		
		public function getAudioFile():String { return _sAudioFile; }
		
		public function isAutoload():Boolean { return _bAutoload || isAutoplay(); }
		
		public function isAutoplay():Boolean { return _bAutoplay; }
		
		// TODO - add streaming mp3 support later
		public function isStreaming():Boolean { return false; }
		
		public function isSeeking():Boolean { return _bSeeking; }
		
		public function getLastSeekPercent():Number { return _nLastSeekPercent; }
		
		public function getLastSeekTime():Number { return getLastSeekPercent() * getDuration(); }
		
		public function getLastSeekTimeRemaining():Number { return getDuration() - getLastSeekTime(); }
		
		public function isPaused():Boolean { return _bPaused; }
		
		public function isPausedForAutoplay():Boolean { return _bPausedForAutoplay; }
		
		public function isRewindOnStop():Boolean { return _bRewindOnStop; }
		
		public function isAtPlaybackStart():Boolean { return _bAtPlaybackStart; }
		
		public function isAtPlaybackEnd():Boolean { return _bAtPlaybackEnd; }
		
		public function isDurationDetermined():Boolean {
			return _bDurationDetermined;
		}
		
		public function isDurationEstimated():Boolean {
			return _bDurationEstimated;
		}
		
		public function getPercentPlayed():Number {
			if ( !isLoadStarted() || !isDurationDetermined() ) {
				return 0;
			}
			if ( isAtPlaybackEnd() ) {
				return 1.0;
			}
			if ( isPaused() ) {
				return _nLastSeekTimeWhilePaused / (_nDuration * 1000.0);
			}
			else {
				return _xSoundChannel.position / (_nDuration * 1000.0);
			}
		}
		
		public function getElapsedTime():Number {
			if ( isAtPlaybackEnd() ) {
				return getDuration();
			}
			else {
				return getElapsedTimeInternal();
			}
		}
		
		public function getTimeRemaining():Number {
			return getDuration() - getElapsedTime();
		}
		
		private function getElapsedTimeInternal():Number {
			if ( !isLoadStarted() || !isDurationDetermined() ) {
				return 0;
			}
			if ( isPaused() ) {
				return _nLastSeekTimeWhilePaused / 1000.0;
			}
			else {
				return _xSoundChannel.position / 1000.0;
			}
		}
		
		public function getDuration():Number {
			if ( !isLoadStarted() || !isDurationDetermined() ) {
				return 0;
			}
			return _nDuration;
		}
		
		public function getPercentLoaded():Number {
			if ( !isLoadStarted() ) {
				return 0;
			}
			return _xSound.bytesLoaded / _xSound.bytesTotal;
		}
		
		public function getBytesLoaded():Number {
			if ( !isLoadStarted() ) {
				return 0;
			}
			return _xSound.bytesLoaded;
		}
		
		public function getBytesTotal():Number {
			if ( !isLoadStarted() ) {
				return 0;
			}
			return _xSound.bytesTotal;
		}
		
		public function isLoadStarted():Boolean { return _bLoadStarted; }
		
		public function isLoadCompleted():Boolean { return _bLoadCompleted; }
		
		public function isBuffering():Boolean { return isLoadBuffering() || isPlaybackBuffering(); }
		
		public function isLoadBuffering():Boolean { return _bLoadBuffering; }
		
		public function isPlaybackBuffering():Boolean { return _bPlaybackBuffering; }
		
		public function isMuted():Boolean { return _bMuted; }
		
		public function getVolume():Number { return _nVolume; }
		
		public function getReportingInterval():int { return _nReportingInterval; }
		
		private function init():void {
			_bAutoplay = true;
			_bAutoload = true;
			_bRewindOnStop = false;
			
			initShowHideListeners();
			
			_nReportingInterval = 10;
			
			_xSound = new Sound();
			_xSound.addEventListener(IOErrorEvent.IO_ERROR, onSoundIOError);
			_nVolume = 1;
			
			initPlaybackFields();
		}
		
		private function onSoundIOError(p_xEvent:IOErrorEvent):void {
			dispatchPlayerEvent(MEDIA_NOT_FOUND);
		}
		
		private function initShowHideListeners():void {
			addPlaybackStartedListener(onPlaybackStarted);
			addPlaybackResumedListener(onPlaybackResumed);
			addPlaybackPausedListener(onPlaybackPaused);
			addPlaybackStoppedListener(onPlaybackStopped);
			addRewindPerformedListener(onRewindPerformed);
			addElapsedTimeUpdatedListener(onElapsedTimeUpdated);
			addMediaOpenedListener(onAudioOpened);
			addMediaNotFoundListener(onAudioNotFound);
			addPlaybackPausedForAutoplayListener(onPlaybackPausedForAutoplay);
		}
		
		private function onPlaybackStarted(p_xEvent:Event):void {
			if ( isAutoplay() ) {
				dispatchPlayerEvent(SHOW_PAUSE);
			}
		}
		
		private function onPlaybackResumed(p_xEvent:Event):void {
			dispatchPlayerEvent(SHOW_PAUSE);
		}
		
		private function onPlaybackPaused(p_xEvent:Event):void {
			dispatchPlayerEvent(SHOW_PLAY);
		}
		
		private function onPlaybackStopped(p_xEvent:Event):void {
			if ( !isRewindOnStop() ) {
				dispatchPlayerEvent(SHOW_REPLAY);
				dispatchPlayerEvent(SHOW_POST_ROLL);
			}
			else {
				rewind();
				dispatchPlayerEvent(SHOW_PRE_ROLL);
			}
		}
		
		private function onElapsedTimeUpdated(p_xEvent:Event):void {
			if ( !isAtPlaybackEnd() ) {
				dispatchPlayerEvent(HIDE_REPLAY);
				dispatchPlayerEvent(HIDE_POST_ROLL);
			}
			if ( !isAtPlaybackStart() ) {
				dispatchPlayerEvent(HIDE_PRE_ROLL);
				dispatchPlayerEvent(HIDE_PUSH_TO_PLAY);
			}
		}
		
		private function onRewindPerformed(p_xEvent:Event):void {
			dispatchPlayerEvent(SHOW_PRE_ROLL);
		}
		
		private function onAudioOpened(p_xEvent:Event):void {
			dispatchPlayerEvent(HIDE_REPLAY);
			dispatchPlayerEvent(HIDE_POST_ROLL);
			if ( !isAutoplay() ) {
				dispatchPlayerEvent(SHOW_PRE_ROLL);
				dispatchPlayerEvent(SHOW_PUSH_TO_PLAY);
			}
		}
		
		private function onPlaybackPausedForAutoplay(p_xEvent:Event):void {
			dispatchPlayerEvent(SHOW_PRE_ROLL);
			dispatchPlayerEvent(SHOW_PUSH_TO_PLAY);
		}
		
		private function onAudioNotFound(p_xEvent:Event):void {
			if ( isAtPlaybackEnd() ) {
				dispatchPlayerEvent(HIDE_REPLAY);
				dispatchPlayerEvent(HIDE_POST_ROLL);
			}
			dispatchPlayerEvent(HIDE_PRE_ROLL);
			dispatchPlayerEvent(HIDE_PUSH_TO_PLAY);
		}
		
		private function initPlaybackFields():void {
			_nDuration = 0;
			_bSeeking = false;
			_bPlayingAtSeekStart = false;
			_nLastSeekPercent = 0;
			_bPaused = false;
			_bPausedForAutoplay = false;
			_bLoadStarted = false;
			_bLoadCompleted = false;
			_bPlaybackBuffering = false;
			_bLoadBuffering = false;
			_bDurationDetermined = false;
			_bDurationEstimated = true;
			_bFirstPlay = true;
			_nLastElapsedTime = -1;
			_nLastBytesLoaded = -1;
			_nLastSeekTimeWhilePaused = -1;
			_bAtPlaybackStart = true;
			_bAtPlaybackEnd = false;
		}
		
		public function clear():void {
			if ( isLoadStarted() ) {
				_xTimer.stop();
				if ( _xSoundChannel != null ) {
					_xSoundChannel.stop();
				}
				_xSoundChannel = null;
				if ( _xSound != null ) {
					try {
						_xSound.close();
					}
					catch ( e:Error ) {}
				}
				_xSound = new Sound();
				_xSound.addEventListener(IOErrorEvent.IO_ERROR, onSoundIOError);
				var l_bWasBuffering:Boolean = isBuffering();
				
				initPlaybackFields();
				
				if ( l_bWasBuffering ) {
					dispatchPlayerEvent(BUFFERING_STOPPED);
				}
				
				dispatchPlayerEvent(MEDIA_CLOSED);
			}
			else {
				initPlaybackFields();
			}
		}
		
		public function play(p_sAudioFile:String):void {
			clear();
			_sAudioFile = p_sAudioFile;
			_bAtPlaybackStart = true;
			if ( isAutoload() ) {
				loadAndPlayAudio();
			}
			else {
				dispatchPlayerEvent(PLAYBACK_PAUSED_FOR_AUTOPLAY);
			}
		}
		
		private function onSoundComplete(p_xEvent:Event):void {
			pause();
			_bAtPlaybackEnd = true;
			dispatchPlayerEvent(PLAYBACK_STOPPED);
		}
		
		private function stopAudio():void {			
			if ( _xSoundChannel != null ) {
				_xSoundChannel.stop();
			}
		}
		
		private function loadAndPlayAudio():void {
			_bLoadStarted = true;
			var l_xLoaderContext:SoundLoaderContext = new SoundLoaderContext();
			l_xLoaderContext.checkPolicyFile = true;
			_xSound.load(new URLRequest(_sAudioFile), l_xLoaderContext);
			_xSoundChannel = _xSound.play();
			_xSoundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			_xTimer = new Timer(_nReportingInterval, 0);
			_xTimer.addEventListener(TimerEvent.TIMER, onTimerUpdate);
			_xTimer.start();
			updateVolume();
			dispatchPlayerEvent(MEDIA_OPENED);
			_bLoadBuffering = true;
			dispatchPlayerEvent(BUFFERING_STARTED);
		}
		
		public function setReportingInterval(p_nInterval:int):void {
			_nReportingInterval = p_nInterval;
			if ( _xTimer != null ) {
				_xTimer.delay = _nReportingInterval;
			}
		}
		
		public function setRewindOnStop(p_bRewindOnStop:Boolean):void {
			_bRewindOnStop = p_bRewindOnStop;
		}
		
		public function setAutoplay(p_bAutoplay:Boolean):void {
			_bAutoplay = p_bAutoplay;
		}
		
		public function setAutoload(p_bAutoload:Boolean):void {
			_bAutoload = p_bAutoload;
		}
		
		public function startSeeking():void {
			_bPlayingAtSeekStart = !isPaused();
			_bSeeking = true;
		}
		
		public function stopSeeking():void { _bSeeking = false; }
		
		public function seekToTime(p_nTime:Number):void {
			performSeek(p_nTime, Math.floor(_nDuration) == 0 ? 0 : p_nTime / Math.floor(_nDuration));
		}
		
		public function seekToPercent(p_nPercent:Number):void {
			performSeek(p_nPercent * Math.floor(_nDuration), p_nPercent);
		}
		
		private function performSeek(p_nTime:Number, p_nPercent:Number):void {
			if ( !isLoadStarted() ) {
				return;
			}
			if ( isAtPlaybackEnd() && p_nTime >= Math.floor(_nDuration) ) {
				return;
			}
			if ( isAtPlaybackStart() && p_nTime == 0 ) {
				return;
			}
			if ( p_nTime < 0 ) {
				p_nTime = 0;
				p_nPercent = 0;
			}
			if ( p_nTime > Math.floor(_nDuration) ) {
				p_nTime = Math.floor(_nDuration);
				p_nPercent = 1;
			}
			if ( isAtPlaybackStart() && p_nPercent > 0 ) {
				_bAtPlaybackStart = false;
			}
			if ( isAtPlaybackEnd() && p_nPercent < 1 ) {
				_bAtPlaybackEnd = false;
			}
			_nLastSeekPercent = p_nPercent;
			if ( !isPaused() ) {
				_xSoundChannel.stop();
				_xSoundChannel = _xSound.play(p_nTime*1000.0);
				_xSoundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				updateVolume();
				_nLastSeekTimeWhilePaused = -1;
			}
			else {
				_nLastSeekTimeWhilePaused = p_nTime*1000.0;
			}
			dispatchPlayerEvent(SEEK_PERFORMED);
			if ( isSeeking() && _bPlayingAtSeekStart && isPaused() ) {
				resume();
			}
			//updateBuffer();
		}
		
		public function rewind():void {
			seekToTime(0);
			_bAtPlaybackStart = true;
			_bAtPlaybackEnd = false;
			dispatchPlayerEvent(REWIND_PERFORMED);
			pause();
		}
		
		public function pause():void {
			if ( !isLoadStarted() || isPaused() ) {
				return;
			}
			_bPaused = true;
			_nLastSeekTimeWhilePaused = _xSoundChannel.position;
			_xSoundChannel.stop();
			dispatchPlayerEvent(PLAYBACK_PAUSED);
		}
		
		public function resume():void {			
			if ( isLoadStarted() && !isPaused() ) {
				return;
			}
			if ( isAtPlaybackEnd() ) {
				return;
			}
			var l_bLoadStarted:Boolean = isLoadStarted();
			if ( !l_bLoadStarted ) {
				loadAndPlayAudio();
			}
			_bPaused = false;
			_bPausedForAutoplay = false;
			if ( l_bLoadStarted ) {
				if ( _nLastSeekTimeWhilePaused != -1 ) {
					_xSoundChannel = _xSound.play(_nLastSeekTimeWhilePaused);
					_nLastSeekTimeWhilePaused = -1;
				}
				else {
					_xSoundChannel = _xSound.play(_xSoundChannel.position);
				}
				_xSoundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				updateVolume();
			}
			dispatchPlayerEvent(PLAYBACK_RESUMED);
		}
		
		private function updateVolume():void {
			if ( !isMuted() ) {
				setStreamVolume(_nVolume);
			}
			else {
				setStreamVolume(0);
			}
		}
		
		public function setMuted(p_bMuted:Boolean) : void {
			if ( p_bMuted ) {
				mute();
			}
			else {
				unmute();
			}
		}
		
		public function mute() : void {
			if ( isMuted() ) {
				return;
			}
			_bMuted = true;
			setStreamVolume(0);
			dispatchPlayerEvent(VOLUME_MUTED);
		}
		
		public function unmute() : void {
			if ( !isMuted() ) {
				return;
			}
			_bMuted = false;
			setStreamVolume(_nVolume);
			dispatchPlayerEvent(VOLUME_UNMUTED);
		}
		
		private function setStreamVolume(p_nVolume:Number) : void {
			if ( _xSoundChannel != null ) {
				_xSoundChannel.soundTransform = new SoundTransform(p_nVolume);
			}
		}
		
		public function setVolume(p_nVolume:Number) : void {
			if ( p_nVolume == getVolume() ) {
				return;
			}
			if ( p_nVolume < 0 ) {
				p_nVolume = 0;
			}
			if ( p_nVolume > 1 ) {
				p_nVolume = 1;
			}
			_nVolume = p_nVolume;
			if ( !isMuted() ) {
				setStreamVolume(_nVolume);
			}
			dispatchPlayerEvent(VOLUME_CHANGED);
		}
		
		private function updateAutoplay():void {
			if ( _bFirstPlay && !isAutoplay() && getDuration() > 0 && getElapsedTime() > 0 ) {
				_bFirstPlay = false;
				if ( isAutoload() ) {
					_bPausedForAutoplay = true;
					pause();
					dispatchPlayerEvent(PLAYBACK_PAUSED_FOR_AUTOPLAY);
				}
			}
		}
		
		private function updateDuration():void {
			if ( !isDurationDetermined() && _xSound.id3.TLEN != null ) {
				_bDurationDetermined = true;
				_bDurationEstimated = false;
				_nDuration = _xSound.id3.TLEN / 1000.0;
				dispatchPlayerEvent(DURATION_DETERMINED);
				return;
			}
			if ( !isDurationDetermined() && _xSound.id3.TIME != null ) {
				_bDurationDetermined = true;
				_bDurationEstimated = false;
				_nDuration = _xSound.id3.TIME / 1000.0;
				dispatchPlayerEvent(DURATION_DETERMINED);
				return;
			}
			if ( !isDurationDetermined() && _xSound.length > 0 ) {
				_bDurationDetermined = true;
				
				_nDuration = _xSound.length / 1000.0 * getPercentLoaded();
				_bDurationEstimated = getPercentLoaded() < 1;
				dispatchPlayerEvent(DURATION_DETERMINED);  
				return;
			}
			if ( isDurationEstimated() && _xSound.length > 0 ) {
				var l_nNewDuration:Number = _xSound.length / 1000.0 * getPercentLoaded();
				if ( l_nNewDuration != _nDuration ) {
					_nDuration = l_nNewDuration;
				}
				_bDurationEstimated = getPercentLoaded() < 1;
				if ( getElapsedTime() == 0 ) {
					_bLoadBuffering = false;
					_bAtPlaybackStart = true;
					dispatchPlayerEvent(PLAYBACK_STARTED);
					if ( !isPlaybackBuffering() ) {
						dispatchPlayerEvent(BUFFERING_STOPPED);
					}
				}
				if ( isAtPlaybackStart() && getElapsedTime() > 0 ) {
					_bAtPlaybackStart = false;
				}
				dispatchPlayerEvent(ELAPSED_TIME_UPDATED);
				_nLastElapsedTime = getElapsedTime();
			}
		}
		
		private function updateElapsedTime():void {
			if ( isDurationEstimated() ) {
				return;
			}
			if ( ( !_bFirstPlay || isAutoplay() ) && !isPausedForAutoplay() && _nLastElapsedTime != getElapsedTime() ) {
				if ( getElapsedTime() == 0 ) {
					_bLoadBuffering = false;
					_bAtPlaybackStart = true;
					dispatchPlayerEvent(PLAYBACK_STARTED);
					if ( !isPlaybackBuffering() ) {
						dispatchPlayerEvent(BUFFERING_STOPPED);
					}
				}
				if ( isAtPlaybackStart() && getElapsedTime() > 0 ) {
					_bAtPlaybackStart = false;
				}
				if ( isAtPlaybackEnd()  && getElapsedTimeInternal() < getDuration() && !isPaused() ) {
					_bAtPlaybackEnd = false;
				}
				else {
					dispatchPlayerEvent(ELAPSED_TIME_UPDATED);
				}
				if ( isDurationDetermined() && getElapsedTimeInternal() >= getDuration() ) {
					_bAtPlaybackEnd = true;
					pause();
					dispatchPlayerEvent(PLAYBACK_STOPPED);
				}
				_nLastElapsedTime = getElapsedTime();
			}
		}
		
		private function updateBytesLoaded():void {
			if ( ( isAutoload() || isAutoplay() || ( !_bFirstPlay && !isPausedForAutoplay() ) ) && _nLastBytesLoaded != getBytesLoaded() ) {
				dispatchPlayerEvent(LOAD_PROGRESS_UPDATED);
				if ( !isLoadCompleted() && getPercentLoaded() == 1 ) {
					if ( isBuffering() ) {
						_bLoadBuffering = false;
						_bPlaybackBuffering = false;
						dispatchPlayerEvent(BUFFERING_STOPPED);
					}
					_bLoadCompleted = true;
					dispatchPlayerEvent(LOAD_COMPLETED);
					_nLastBytesLoaded = getBytesLoaded();
				}
			}
		}
		
		private function updateBuffering():void {
			if ( !isLoadCompleted() ) {
				if ( !isDurationDetermined() ) {
					if ( !isBuffering() ) {
						_bLoadBuffering = true;
						dispatchPlayerEvent(BUFFERING_STARTED);
					}
				}
				else if ( _xSound.isBuffering != isBuffering() ) {
					if ( _xSound.isBuffering ) {
						if ( getElapsedTime() > 0 ) {
							_bPlaybackBuffering = true;
						}
						else {
							_bLoadBuffering = true;
						}
						dispatchPlayerEvent(BUFFERING_STARTED);
					}
					else {
						_bLoadBuffering = false;
						_bPlaybackBuffering = false;
						dispatchPlayerEvent(BUFFERING_STOPPED);
					}
				}
			}
			else if ( isBuffering() ) {
				_bLoadBuffering = false;
				_bPlaybackBuffering = false;
				dispatchPlayerEvent(BUFFERING_STOPPED);
			}
		}
		
		private function onTimerUpdate(p_xEvent:TimerEvent):void {
			if ( !isLoadStarted() ) {
				return;
			}
			updateAutoplay();
			updateBytesLoaded();
			updateDuration();
			updateElapsedTime();
			updateBuffering();
		}
	}
}