package com.codeandtheory.core.components.audio.controls {
	import com.codeandtheory.core.components.audio.AudioPlayer;
	import com.codeandtheory.core.components.media.IMediaPlayer;
	import com.codeandtheory.core.components.media.controls.*;
	import com.codeandtheory.core.components.slider.Slider;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.text.TextField;
	
	public class AudioPlayerController extends AudioPlayer implements IMediaPlayer {
		
		private var _aPauseButtons:Array = new Array();
		private var _aPlayButtons:Array = new Array();
		private var _aTogglePauseButtons:Array = new Array();
		private var _aReplayButtons:Array = new Array();
		private var _aMuteButtons:Array = new Array();
		private var _aUnmuteButtons:Array = new Array();
		private var _aToggleMuteButtons:Array = new Array();
		private var _aVideoProgressSliders:Array = new Array();
		private var _aVolumeSliders:Array = new Array();
		private var _aLoadProgressSliders:Array = new Array();
		private var _aVideoProgressLabels:Array = new Array();
		private var _aLoadProgressLabels:Array = new Array();
		private var _aBufferingIcons:Array = new Array();
		private var _aPostRolls:Array = new Array();
		private var _aPushToPlayButtons:Array = new Array();
		private var _xActiveSlider:Slider;
		private var _bRoundSliderDisplayPositions:Boolean = false;
		
		public function AudioPlayerController() {
			super();
		}
		
		public function getRoundSliderDisplayPositions():Boolean {
			return _bRoundSliderDisplayPositions;
		}

		public function setRoundSliderDisplayPositions(p_bRound:Boolean): void {
			_bRoundSliderDisplayPositions = p_bRound;
		}
		
		public function updatePositions():void {
			for each ( var l_xLoadProgressSlider:LoadProgressSlider in _aLoadProgressSliders ) {
				l_xLoadProgressSlider.updatePosition();
			}
			for each ( var l_xVideoProgressSlider:PlaybackProgressSlider in _aVideoProgressSliders ) {
				l_xVideoProgressSlider.updatePosition();
			}
			for each ( var l_xVolumeSlider:VolumeSlider in _aVolumeSliders ) {
				l_xVolumeSlider.updatePosition();
			}
		}
		
		public function stopActiveSlider():void {
			if ( _xActiveSlider != null ) {
				//_xActiveSlider.onDisplayReleased(); // called by stopDrag
				_xActiveSlider.stopDrag();
			}
		}
		
		public function setActiveSlider(p_xSlider:Slider):void {
			_xActiveSlider = p_xSlider;
		}
		
		public function attachPauseButton(p_xButton:DisplayObject):PauseButton {
			var r_xPauseButton:PauseButton = new PauseButton(this, p_xButton);
			_aPauseButtons.push(r_xPauseButton);
			return r_xPauseButton;
		}
		
		public function attachPlayButton(p_xButton:DisplayObject):PlayButton {
			var r_xPlayButton:PlayButton = new PlayButton(this, p_xButton);
			_aPlayButtons.push(r_xPlayButton);
			return r_xPlayButton;
		}
		
		public function attachTogglePauseButton(p_xPauseButton:DisplayObject, p_xPlayButton:DisplayObject, p_xShowPauseFunction:Function = null, p_xShowPlayFunction:Function = null):TogglePauseButton {
			var r_xTogglePauseButton:TogglePauseButton = new TogglePauseButton(this, p_xPauseButton, p_xPlayButton, p_xShowPauseFunction, p_xShowPlayFunction);
			_aTogglePauseButtons.push(r_xTogglePauseButton);
			return r_xTogglePauseButton;
		}
		
		public function attachReplayButton(p_xButton:DisplayObject, p_xShowReplayFunction:Function = null, p_xHideReplayFunction:Function = null):ReplayButton {
			var r_xReplayButton:ReplayButton = new ReplayButton(this, p_xButton, p_xShowReplayFunction, p_xHideReplayFunction);
			_aReplayButtons.push(r_xReplayButton);
			return r_xReplayButton;
		}
		
		public function attachMuteButton(p_xButton:DisplayObject):MuteButton {
			var r_xMuteButton:MuteButton = new MuteButton(this, p_xButton);
			_aMuteButtons.push(r_xMuteButton);
			return r_xMuteButton;
		}
		
		public function attachUnmuteButton(p_xButton:DisplayObject):UnmuteButton {
			var r_xUnmuteButton:UnmuteButton = new UnmuteButton(this, p_xButton);
			_aUnmuteButtons.push(r_xUnmuteButton);
			return r_xUnmuteButton;
		}
		
		public function attachToggleMuteButton(p_xMuteButton:DisplayObject, p_xUnmuteButton:DisplayObject, p_xShowMuteFunction:Function = null, p_xShowUnmuteFunction:Function = null):ToggleMuteButton {
			var r_xToggleMuteButton:ToggleMuteButton = new ToggleMuteButton(this, p_xMuteButton, p_xUnmuteButton, p_xShowMuteFunction, p_xShowUnmuteFunction);
			_aToggleMuteButtons.push(r_xToggleMuteButton);
			return r_xToggleMuteButton;
		}
		
		public function attachPlaybackProgressSlider(p_xHolder:DisplayObject, p_xDisplay:Sprite,
													 p_xMouseArea:DisplayObject,
													 p_nType:int, p_nDirection:int, p_nInteraction:int):PlaybackProgressSlider {
			var r_xPlaybackProgressSlider:PlaybackProgressSlider = new PlaybackProgressSlider(this,
				p_xHolder, p_xDisplay, p_xMouseArea, p_nType, p_nDirection, p_nInteraction);
			_aVideoProgressSliders.push(r_xPlaybackProgressSlider);
			return r_xPlaybackProgressSlider;
		}
		
		public function attachVolumeSlider(p_xVolumeHolder:DisplayObject, p_xVolumeDisplay:Sprite,
										   p_xMouseArea:DisplayObject, p_bMuteInteraction:Boolean,
										   p_nType:int, p_nDirection:int, p_nInteraction:int):VolumeSlider {
			var r_xVolumeSlider:VolumeSlider = new VolumeSlider(this, p_xVolumeHolder, p_xVolumeDisplay,
				p_xMouseArea, p_bMuteInteraction, p_nType, p_nDirection, p_nInteraction);
			_aVolumeSliders.push(r_xVolumeSlider);
			return r_xVolumeSlider;
		}
		
		public function attachLoadProgressSlider(p_xLoadProgressHolder:DisplayObject, p_xLoadProgressDisplay:Sprite,
												 p_nType:int = -1, p_nDirection:int = -1):LoadProgressSlider {
			var r_xLoadProgressSlider:LoadProgressSlider = new LoadProgressSlider(this, p_xLoadProgressHolder, p_xLoadProgressDisplay, p_nType, p_nDirection);
			_aLoadProgressSliders.push(r_xLoadProgressSlider);
			return r_xLoadProgressSlider;
		}
		
		public function attachPlaybackProgressLabel(p_xVideoProgressLabel:TextField, p_sFormat:String = null):PlaybackProgressLabel {
			var r_xPlaybackProgressLabel:PlaybackProgressLabel = new PlaybackProgressLabel(this, p_xVideoProgressLabel, p_sFormat);
			_aVideoProgressLabels.push(r_xPlaybackProgressLabel);
			return r_xPlaybackProgressLabel;
		}
		
		public function attachLoadProgressLabel(p_xLoadProgressLabel:TextField, p_sFormat:String = null):LoadProgressLabel {
			var r_xLoadProgressLabel:LoadProgressLabel = new LoadProgressLabel(this, p_xLoadProgressLabel, p_sFormat);
			_aLoadProgressLabels.push(r_xLoadProgressLabel);
			return r_xLoadProgressLabel;
		}
		
		public function attachBufferingIcon(p_xBufferingIcon:DisplayObject, p_xShowIconFunction:Function = null, p_xHideIconFunction:Function = null):BufferingIcon {
			var r_xBufferingIcon:BufferingIcon = new BufferingIcon(this, p_xBufferingIcon, p_xShowIconFunction, p_xHideIconFunction);
			_aBufferingIcons.push(r_xBufferingIcon);
			return r_xBufferingIcon;
		}
		
		public function attachPreRoll(p_xPreRoll:DisplayObject, p_xShowThumbFunction:Function = null, p_xHideThumbFunction:Function = null):PreRoll {
			var r_xPreRoll:PreRoll = new PreRoll(this, p_xPreRoll, p_xShowThumbFunction, p_xHideThumbFunction);
			_aPostRolls.push(r_xPreRoll);
			return r_xPreRoll;
		}
		
		public function attachPostRoll(p_xPostRoll:DisplayObject, p_xShowPostRollFunction:Function = null, p_xHidePostRollFunction:Function = null):PostRoll {
			var r_xPostRoll:PostRoll = new PostRoll(this, p_xPostRoll, p_xShowPostRollFunction, p_xHidePostRollFunction);
			_aPostRolls.push(r_xPostRoll);
			return r_xPostRoll;
		}
		
		public function attachPushToPlayButton(p_xPushToPlayButton:DisplayObject, p_xShowPushToPlayFunction:Function = null, p_xHidePushToPlayFunction:Function = null):PushToPlayButton {
			var r_xPushToPlayButton:PushToPlayButton = new PushToPlayButton(this, p_xPushToPlayButton, p_xShowPushToPlayFunction, p_xHidePushToPlayFunction);
			_aPushToPlayButtons.push(r_xPushToPlayButton);
			return r_xPushToPlayButton;
		}
	}
}