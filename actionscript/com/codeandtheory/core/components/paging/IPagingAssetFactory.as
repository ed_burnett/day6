package com.codeandtheory.core.components.paging {
	import flash.display.Sprite;


	public interface IPagingAssetFactory {
		function newPageButton (p_nPageNumber:int) : PageButton;
		function newPageSetButton (p_nStartPageNumber:int, p_nEndPageNumber:int) : PageSetButton;

		function newNextPageButton () : Sprite;
		function newPreviousPageButton () : Sprite;

		function newNextPageSetButton () : Sprite;
		function newPreviousPageSetButton () : Sprite;
	}
}