package com.codeandtheory.core.components.paging {
	import flash.events.Event;
	import flash.events.EventDispatcher;


	public class PagingModel extends EventDispatcher {
		public static const CURRENT_PAGE_UPDATED:String = "CURRENT_PAGE_UPDATED";
		public static const NUM_ITEMS_UPDATED:String = "NUM_ITEMS_UPDATED";
		public static const ENABLED_TOGGLED:String = "ENABLED_TOGGLED";


		protected var _itemsPerPage:int = 25;
		protected var _pagesPerPageSet:int = 5;
		
		
		protected var _numItems:int = 0;
		protected var _numPages:int = 0;
		protected var _numPageSets:int = 0;


		protected var _currentPage:int = 0;
		protected var _currentPageSet:int = 0;
		
		
		protected var _enabled:Boolean = true;
		
		
		public function PagingModel (p_nItemsPerPage:int, p_nPagesPerPage:int) {
			_itemsPerPage = p_nItemsPerPage;
			_pagesPerPageSet = p_nPagesPerPage;
		}
		
		
		public function next () : void {
			updateCurrentPage(_currentPage + 1);
		}
		public function previous () : void {
			updateCurrentPage(_currentPage - 1);
		}
		public function goto (p_nPage:int) : void {
			updateCurrentPage(p_nPage);
		}
		protected function updateCurrentPage (p_nPage:int) : void {
			if (!_enabled) {// don't do anything if not enabled
				return;
			}
			if (p_nPage != _currentPage && p_nPage >= 0 && p_nPage < _numPages) {// don't go out of bounds
				_currentPage = p_nPage;
				_currentPageSet = Math.floor(_currentPage / _pagesPerPageSet);
				dispatchEvent(new Event(CURRENT_PAGE_UPDATED));
			}
		}


		public function get numItems () : int {
			return _numItems;
		}
		public function set numItems (p_nNumItems:int) : void {
			if (!_enabled) {// don't do anything if not enabled
				return;
			}
			_numItems = p_nNumItems;
			onNumItemsUpdated();
		}
		protected function onNumItemsUpdated () : void {
			_numPages = Math.ceil(_numItems / _itemsPerPage);
			_numPageSets = Math.ceil(_numPages / _pagesPerPageSet);
			_currentPage = 0;
			_currentPageSet = 0;

			dispatchEvent(new Event(NUM_ITEMS_UPDATED));
		}
		
		
		public function get enabled () : Boolean {
			return _enabled;
		}
		public function set enabled (p_bEnabled:Boolean) : void {
			_enabled = p_bEnabled;
			onEnabledUpdated();
		}
		protected function onEnabledUpdated () : void {
			dispatchEvent(new Event(ENABLED_TOGGLED));
		}
		
		
		public function get itemsPerPage () : int {
			return _itemsPerPage;
		}
		public function get pagesPerPageSet () : int {
			return _pagesPerPageSet;
		}
		public function get numPages () : int {
			return _numPages;
		}
		public function get numPageSets () : int {
			return _numPageSets;
		}
		public function get currentPage () : int {
			return _currentPage;
		}
		public function get currentPageSet () : int {
			return _currentPageSet;
		}
	}
}