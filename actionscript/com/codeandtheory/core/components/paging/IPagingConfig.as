package com.codeandtheory.core.components.paging {
	public interface IPagingConfig {
		function get itemsPerPage () : int;
		function get pagesPerPageSet () : int;

		function get pagingWidth () : Number;
		function get pagingHeight () : Number;
		
		function get pageButtonSpacing () : Number;
		function get pageSetButtonSpacing () : Number;
		
		function get assetFactory () : IPagingAssetFactory;
	}
}