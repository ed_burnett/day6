package com.codeandtheory.core.components.paging {
	public class PagingConfig implements IPagingConfig {
		protected var _itemsPerPage:int = 25;
		protected var _pagesPerPageSet:int = 15;

		protected var _pagingWidth:Number = 0;
		protected var _pagingHeight:Number = 0;
		
		protected var _pageButtonSpacing:Number = 0;
		protected var _pageSetButtonSpacing:Number = 0;
		
		protected var _assetFactory:IPagingAssetFactory;


		public function PagingConfig (p_xAssetFactory:IPagingAssetFactory, p_nPagingWidth:Number, p_nPagingHeight:Number, p_nItemsPerPage:int = 25, p_nPagesPerPageSet:int = 15,
									  p_nPageButtonSpacing:Number = 12, p_nPageSetButtonSpacing:Number = 12) {
			_assetFactory = p_xAssetFactory;
			_pagingWidth = p_nPagingWidth;
			_pagingHeight = p_nPagingHeight;
			_itemsPerPage = p_nItemsPerPage;
			_pagesPerPageSet = p_nPagesPerPageSet;
			_pageButtonSpacing = p_nPageButtonSpacing;
			_pageSetButtonSpacing = p_nPageSetButtonSpacing;
		}


		public function get itemsPerPage () : int {
			return _itemsPerPage;
		}
		public function get pagesPerPageSet () : int {
			return _pagesPerPageSet;
		}
		public function get pagingWidth () : Number {
			return _pagingWidth;
		}
		public function get pagingHeight () : Number {
			return _pagingHeight;
		}
		public function get pageButtonSpacing () : Number {
			return _pageButtonSpacing;
		}
		public function get pageSetButtonSpacing () : Number {
			return _pageSetButtonSpacing;
		}
		public function get assetFactory () : IPagingAssetFactory {
			return _assetFactory;
		}
	}
}