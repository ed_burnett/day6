package com.codeandtheory.core.components.paging {
	public interface IPagingViewEventHandler {
		function onPageButtonRollover (p_xEvent:PagingViewEvent) : void;
		function onPageButtonRollout (p_xEvent:PagingViewEvent) : void;
		function onPageButtonClick (p_xEvent:PagingViewEvent) : void;
		function onNextPageClick (p_xEvent:PagingViewEvent) : void;
		function onPreviousPageClick (p_xEvent:PagingViewEvent) : void;

		function onPageSetButtonRollover (p_xEvent:PagingViewEvent) : void;
		function onPageSetButtonRollout (p_xEvent:PagingViewEvent) : void;
		function onPageSetButtonClick (p_xEvent:PagingViewEvent) : void;
		function onNextPageSetClick (p_xEvent:PagingViewEvent) : void;
		function onPreviousPageSetClick (p_xEvent:PagingViewEvent) : void;
	}
}