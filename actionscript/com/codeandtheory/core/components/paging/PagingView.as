package com.codeandtheory.core.components.paging {
	import com.codeandtheory.core.utils.DisplayObjectContainerUtils;
	import com.codeandtheory.core.utils.ShapeUtils;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;


	public class PagingView extends Sprite {
		// model
		protected var _model:PagingModel = null;

		// assets
		protected var _assetFactory:IPagingAssetFactory = null;

		// properties
		protected var _pixelWidth:Number = 0;
		protected var _pixelHeight:Number = 0;
		protected var _pageButtonSpacing:Number = 0;
		protected var _pageSetButtonSpacing:Number = 0;
		protected var _pageSetButtonVerticalPadding:Number = 10;
		
		// ui elements
		protected var _container:Sprite = null;
		protected var _containerMask:Sprite = null;
		protected var _pageButtonsContainer:Sprite = null;
		protected var _pageSetButtonsContainer:Sprite = null;
		protected var _nextPageButton:Sprite = null;
		protected var _previousPageButton:Sprite = null;
		protected var _nextPageSetButton:Sprite = null;
		protected var _previousPageSetButton:Sprite = null;
		
		// calculated properties
		protected var _pageButtonsContainerAvailableWidth:Number = 0;
		protected var _pageSetButtonsContainerAvailableWidth:Number = 0;


		public function PagingView (p_xModel:PagingModel, p_nWidth:Number, p_nHeight:Number, p_xAssetFactory:IPagingAssetFactory, p_nPageButtonSpacing:Number = 12, p_nPageSetButtonSpacing:Number = 12) {
			_model = p_xModel;
			_pixelWidth = p_nWidth;
			_pixelHeight = p_nHeight;
			_pageButtonSpacing = p_nPageButtonSpacing;
			_pageSetButtonSpacing = p_nPageSetButtonSpacing;
			_assetFactory = p_xAssetFactory;

			initializeView();

			_model.addEventListener(PagingModel.CURRENT_PAGE_UPDATED, onCurrentPageUpdated, false, 0, true);
			_model.addEventListener(PagingModel.NUM_ITEMS_UPDATED, onNumItemsUpdated, false, 0, true);
			_model.addEventListener(PagingModel.ENABLED_TOGGLED, onEnabledToggled, false, 0, true);
		}
		
		
		public function addViewEventHandler (p_xHandler:IPagingViewEventHandler) : void {
			this.addEventListener(PagingViewEvent.PAGE_BUTTON_CLICK, p_xHandler.onPageButtonClick, false, 0, true);
			this.addEventListener(PagingViewEvent.PAGE_BUTTON_ROLLOUT, p_xHandler.onPageButtonRollout, false, 0, true);
			this.addEventListener(PagingViewEvent.PAGE_BUTTON_ROLLOVER, p_xHandler.onPageButtonRollover, false, 0, true);
			this.addEventListener(PagingViewEvent.NEXT_PAGE_CLICK, p_xHandler.onNextPageClick, false, 0, true);
			this.addEventListener(PagingViewEvent.PREVIOUS_PAGE_CLICK, p_xHandler.onPreviousPageClick, false, 0, true);

			this.addEventListener(PagingViewEvent.PAGE_SET_BUTTON_CLICK, p_xHandler.onPageSetButtonClick, false, 0, true);
			this.addEventListener(PagingViewEvent.PAGE_SET_BUTTON_ROLLOUT, p_xHandler.onPageSetButtonRollout, false, 0, true);
			this.addEventListener(PagingViewEvent.PAGE_SET_BUTTON_ROLLOVER, p_xHandler.onPageSetButtonRollover, false, 0, true);
			this.addEventListener(PagingViewEvent.NEXT_PAGE_SET_CLICK, p_xHandler.onNextPageSetClick, false, 0, true);
			this.addEventListener(PagingViewEvent.PREVIOUS_PAGE_SET_CLICK, p_xHandler.onPreviousPageSetClick, false, 0, true);
		}
		public function removeViewEventHandler (p_xHandler:IPagingViewEventHandler) : void {
			this.removeEventListener(PagingViewEvent.PAGE_BUTTON_CLICK, p_xHandler.onPageButtonClick);
			this.removeEventListener(PagingViewEvent.PAGE_BUTTON_ROLLOUT, p_xHandler.onPageButtonRollout);
			this.removeEventListener(PagingViewEvent.PAGE_BUTTON_ROLLOVER, p_xHandler.onPageButtonRollover);
			this.removeEventListener(PagingViewEvent.NEXT_PAGE_CLICK, p_xHandler.onNextPageClick);
			this.removeEventListener(PagingViewEvent.PREVIOUS_PAGE_CLICK, p_xHandler.onPreviousPageClick);

			this.removeEventListener(PagingViewEvent.PAGE_SET_BUTTON_CLICK, p_xHandler.onPageSetButtonClick);
			this.removeEventListener(PagingViewEvent.PAGE_SET_BUTTON_ROLLOUT, p_xHandler.onPageSetButtonRollout);
			this.removeEventListener(PagingViewEvent.PAGE_SET_BUTTON_ROLLOVER, p_xHandler.onPageSetButtonRollover);
			this.removeEventListener(PagingViewEvent.NEXT_PAGE_SET_CLICK, p_xHandler.onNextPageSetClick);
			this.removeEventListener(PagingViewEvent.PREVIOUS_PAGE_SET_CLICK, p_xHandler.onPreviousPageSetClick);
		}
		
		
		protected function onCurrentPageUpdated (p_xEvent:Event) : void {
			updatePageButtons();
		}
		protected function onNumItemsUpdated (p_xEvent:Event) : void {
			updatePageButtons();
		}
		protected function updatePageButtons () : void {
			// calculate start / end pages
			var l_nTempStartPage:int = _model.currentPageSet * _model.pagesPerPageSet;
			var l_nTempEndPage:int = l_nTempStartPage + _model.pagesPerPageSet - 1;
			if (l_nTempEndPage > _model.numPages) {
				l_nTempEndPage = _model.numPages - 1; 
			}

			// recreate buttons
			DisplayObjectContainerUtils.removeAllChildren(_pageButtonsContainer);
			var i:int = 0;
			var l_xTempButton:PageButton = null;
			for (i=l_nTempStartPage; i<=l_nTempEndPage; i++) {
				l_xTempButton = _assetFactory.newPageButton(i);
				l_xTempButton.addEventListener(MouseEvent.CLICK, onPageButtonClick, false, 0, true);
				l_xTempButton.addEventListener(MouseEvent.ROLL_OVER, onPageButtonRollover, false, 0, true);
				l_xTempButton.addEventListener(MouseEvent.ROLL_OUT, onPageButtonRollout, false, 0, true);
				l_xTempButton.selected = i == _model.currentPage;

				_pageButtonsContainer.addChild(l_xTempButton);
			}
			
			// recreate page set buttons
			DisplayObjectContainerUtils.removeAllChildren(_pageSetButtonsContainer);
			var l_xTempPageSetButton:PageSetButton = null;
			for (i=0; i<_model.numPageSets; i++) {
				l_nTempStartPage = i * _model.pagesPerPageSet;
				l_nTempEndPage = (l_nTempStartPage + _model.pagesPerPageSet) - 1;
				if (l_nTempEndPage >= _model.numPages) {
					l_nTempEndPage = _model.numPages - 1;
				}
				
				l_xTempPageSetButton = _assetFactory.newPageSetButton(l_nTempStartPage, l_nTempEndPage);
				l_xTempPageSetButton.addEventListener(MouseEvent.CLICK, onPageSetButtonClick, false, 0, true);
				l_xTempPageSetButton.addEventListener(MouseEvent.ROLL_OVER, onPageSetButtonRollover, false, 0, true);
				l_xTempPageSetButton.addEventListener(MouseEvent.ROLL_OUT, onPageSetButtonRollout, false, 0, true);
				l_xTempPageSetButton.selected = i == _model.currentPageSet;
				
				_pageSetButtonsContainer.addChild(l_xTempPageSetButton);
			}

			// layout buttons
			layoutButtons();
		}
		protected function layoutButtons () : void {
			_previousPageButton.x = 0;
			_previousPageButton.visible = _model.currentPage != 0;

			_nextPageButton.x = _pixelWidth - _nextPageButton.width;
			_nextPageButton.visible = _model.currentPage != (_model.numPages - 1);

			DisplayObjectContainerUtils.spaceChildrenByWidth(_pageButtonsContainer, _pageButtonSpacing, DisplayObjectContainerUtils.REGISTRATION_POINT_LEFT);
			_pageButtonsContainer.x = (_pixelWidth - _pageButtonsContainer.width) / 2;


// OMIT FOR NOW
//			_previousPageSetButton.x = 0;
//			_previousPageSetButton.y = _previousPageButton.y + _previousPageButton.height + _pageSetButtonVerticalPadding;
//			_previousPageSetButton.visible = _model.currentPageSet != 0;
//
//			_nextPageSetButton.x = _pixelWidth - _nextPageSetButton.width;
//			_nextPageSetButton.y = _previousPageSetButton.y;
//			_nextPageSetButton.visible = _model.currentPageSet != (_model.numPageSets - 1);
			
			DisplayObjectContainerUtils.spaceChildrenByWidth(_pageSetButtonsContainer, _pageSetButtonSpacing, DisplayObjectContainerUtils.REGISTRATION_POINT_LEFT);
			_pageSetButtonsContainer.x = (_pixelWidth - _pageSetButtonsContainer.width) / 2;
			_pageSetButtonsContainer.y = _previousPageButton.y + _previousPageButton.height + _pageSetButtonVerticalPadding;
		}


		protected function onEnabledToggled (p_xEvent:Event) : void {
			// disable buttons
			_nextPageButton.useHandCursor = _model.enabled;
			_nextPageButton.buttonMode = _model.enabled;
			_previousPageButton.useHandCursor = _model.enabled;
			_previousPageButton.buttonMode = _model.enabled;
			
			_nextPageSetButton.useHandCursor = _model.enabled;
			_nextPageSetButton.buttonMode = _model.enabled;
			_previousPageSetButton.useHandCursor = _model.enabled;
			_previousPageSetButton.buttonMode = _model.enabled;

			var i:int = 0;
			var l_xTempButton:PageButton = null;
			for (i=0; i<_pageButtonsContainer.numChildren; i++) {
				l_xTempButton = _pageButtonsContainer.getChildAt(i) as PageButton;
				l_xTempButton.enabled = _model.enabled;
			}

			var l_xTempPageSetButton:PageSetButton = null;
			for (i=0; i<_pageSetButtonsContainer.numChildren; i++) {
				l_xTempPageSetButton = _pageSetButtonsContainer.getChildAt(i) as PageSetButton;
				l_xTempPageSetButton.enabled = _model.enabled;
			}
		}


		protected function initializeView () : void {
			_container = new Sprite();
			_containerMask = ShapeUtils.newRectangle(_pixelWidth, _pixelHeight);

			_nextPageButton = _assetFactory.newNextPageButton();
			_nextPageButton.visible = false;
			_nextPageButton.useHandCursor = true;
			_nextPageButton.buttonMode = true;
			_nextPageButton.addEventListener(MouseEvent.CLICK, onNextPageClick, false, 0, true);

			_previousPageButton = _assetFactory.newPreviousPageButton();
			_previousPageButton.visible = false;
			_previousPageButton.useHandCursor = true;
			_previousPageButton.buttonMode = true;
			_previousPageButton.addEventListener(MouseEvent.CLICK, onPreviousPageClick, false, 0, true);

			_nextPageSetButton = _assetFactory.newNextPageSetButton();
			_nextPageSetButton.visible = false;

			_previousPageSetButton = _assetFactory.newPreviousPageSetButton();
			_previousPageSetButton.visible = false;
			
			_pageButtonsContainer = new Sprite();
			_pageButtonsContainerAvailableWidth = _pixelWidth - (_nextPageButton.width + _previousPageButton.width);
			
			_pageSetButtonsContainer = new Sprite();
			_pageSetButtonsContainerAvailableWidth = _pixelWidth - (_nextPageSetButton.width + _previousPageSetButton.width);

			_container.addChild(_nextPageButton);
			_container.addChild(_previousPageButton);
			_container.addChild(_nextPageSetButton);
			_container.addChild(_previousPageSetButton);
			_container.addChild(_pageButtonsContainer);
			_container.addChild(_pageSetButtonsContainer);

			addChild(_container);
			addChild(_containerMask);
			_container.mask = _containerMask;
		}
		
		
		protected function onPageButtonClick (p_xEvent:MouseEvent) : void {
			dispatchEvent(new PagingViewEvent(PagingViewEvent.PAGE_BUTTON_CLICK, p_xEvent));
		}
		protected function onPageButtonRollover (p_xEvent:MouseEvent) : void {
			dispatchEvent(new PagingViewEvent(PagingViewEvent.PAGE_BUTTON_ROLLOVER, p_xEvent));
		}
		protected function onPageButtonRollout (p_xEvent:MouseEvent) : void {
			dispatchEvent(new PagingViewEvent(PagingViewEvent.PAGE_BUTTON_ROLLOUT, p_xEvent));
		}


		protected function onPageSetButtonClick (p_xEvent:MouseEvent) : void {
			dispatchEvent(new PagingViewEvent(PagingViewEvent.PAGE_SET_BUTTON_CLICK, p_xEvent));
		}
		protected function onPageSetButtonRollover (p_xEvent:MouseEvent) : void {
			dispatchEvent(new PagingViewEvent(PagingViewEvent.PAGE_SET_BUTTON_ROLLOVER, p_xEvent));
		}
		protected function onPageSetButtonRollout (p_xEvent:MouseEvent) : void {
			dispatchEvent(new PagingViewEvent(PagingViewEvent.PAGE_SET_BUTTON_ROLLOUT, p_xEvent));
		}


		protected function onNextPageClick (p_xEvent:MouseEvent) : void {
			dispatchEvent(new PagingViewEvent(PagingViewEvent.NEXT_PAGE_CLICK, p_xEvent));
		}
		protected function onPreviousPageClick (p_xEvent:MouseEvent) : void {
			dispatchEvent(new PagingViewEvent(PagingViewEvent.PREVIOUS_PAGE_CLICK, p_xEvent));
		}
	}
}