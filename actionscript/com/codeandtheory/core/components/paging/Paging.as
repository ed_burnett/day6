package com.codeandtheory.core.components.paging {
	import flash.display.Sprite;
	import flash.events.MouseEvent;


	public class Paging extends Sprite implements IPagingViewEventHandler {
		// config
		protected var _config:IPagingConfig = null;
		public function get config () : IPagingConfig {
			return _config;
		}


		// model
		protected var _model:PagingModel = null;
		public function get model () : PagingModel {
			return _model;
		}


		// view
		protected var _view:PagingView = null;
		public function get view () : PagingView {
			return _view;
		}


		public function Paging (p_xConfig:IPagingConfig) {
			_config = p_xConfig;
			initializeComponent();
		}
		
		
		protected function initializeComponent () : void {
			_model = new PagingModel(_config.itemsPerPage, _config.pagesPerPageSet);

			_view = new PagingView(_model, _config.pagingWidth, _config.pagingHeight, _config.assetFactory, _config.pageButtonSpacing, _config.pageSetButtonSpacing);
			_view.addViewEventHandler(this);
			addChild(_view);
		}
		
		
		public function onPageButtonRollover (p_xEvent:PagingViewEvent) : void {
			handleRoll(p_xEvent, true);
		}
		public function onPageButtonRollout (p_xEvent:PagingViewEvent) : void {
			handleRoll(p_xEvent, false);
		}
		protected function handleRoll (p_xEvent:PagingViewEvent, p_bRolled:Boolean) : void {
			var l_xPageButton:PageButton = getPageButtonFromEvent(p_xEvent);
			l_xPageButton.rolled = p_bRolled;
		}
		public function onPageButtonClick (p_xEvent:PagingViewEvent) : void {
			var l_xPageButton:PageButton = getPageButtonFromEvent(p_xEvent);
			_model.goto(l_xPageButton.pageNumber);
		}
		public function onNextPageClick (p_xEvent:PagingViewEvent) : void {
			_model.next();
		}
		public function onPreviousPageClick (p_xEvent:PagingViewEvent) : void {
			_model.previous();
		}



		public function onPageSetButtonRollover (p_xEvent:PagingViewEvent) : void {
			handlePageSetRoll(p_xEvent, true);
		}
		public function onPageSetButtonRollout (p_xEvent:PagingViewEvent) : void {
			handlePageSetRoll(p_xEvent, false);
		}
		protected function handlePageSetRoll (p_xEvent:PagingViewEvent, p_bRolled:Boolean) : void {
			var l_xPageSetButton:PageSetButton = getPageSetButtonFromEvent(p_xEvent);
			l_xPageSetButton.rolled = p_bRolled;
		}	
		public function onPageSetButtonClick (p_xEvent:PagingViewEvent) : void {
			var l_xPageSetButton:PageSetButton = getPageSetButtonFromEvent(p_xEvent);
			_model.goto(l_xPageSetButton.startPageNumber); 
		}
		public function onNextPageSetClick (p_xEvent:PagingViewEvent) : void {
			// nothing right now
		}
		public function onPreviousPageSetClick (p_xEvent:PagingViewEvent) : void {
			// nothing right now
		}


		protected function getPageButtonFromEvent (p_xEvent:PagingViewEvent) : PageButton {
			var l_xMouseEvent:MouseEvent = p_xEvent.originalEvent as MouseEvent;
			if (l_xMouseEvent != null) {
				return l_xMouseEvent.currentTarget as PageButton;
			}

			return p_xEvent.currentTarget as PageButton;
		}
		
		
		protected function getPageSetButtonFromEvent (p_xEvent:PagingViewEvent) : PageSetButton {
			var l_xMouseEvent:MouseEvent = p_xEvent.originalEvent as MouseEvent;
			if (l_xMouseEvent != null) {
				return l_xMouseEvent.currentTarget as PageSetButton;
			}

			return p_xEvent.currentTarget as PageSetButton;
		}
	}
}