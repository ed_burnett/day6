package com.codeandtheory.core.components.paging {
	import flash.display.Sprite;


	public class PageButton extends Sprite {
		protected var _enabled:Boolean = true;
		protected var _pageNumber:int = 0;
		protected var _selected:Boolean = false;
		protected var _rolled:Boolean = false;


		public function PageButton (p_nPageNumber:int) {
			_pageNumber = p_nPageNumber;
		}


		public function get pageNumber () : int {
			return _pageNumber;
		}
		
		
		public function get selected () : Boolean {
			return _selected;
		}
		public function set selected (p_bSelected:Boolean) : void {
			_selected = p_bSelected;
			onSelectedUpdated();
		}
		protected function onSelectedUpdated () : void {
			// OVERRIDE ME
		}
		
		
		public function get rolled () : Boolean {
			return _rolled;
		}
		public function set rolled (p_bRolled:Boolean) : void {
			_rolled = p_bRolled;
			onRolledUpdated();
		}
		protected function onRolledUpdated () : void {
			// OVERRIDE ME
		}
		
		
		public function get enabled () : Boolean {
			return _enabled;
		}
		public function set enabled (p_bEnabled:Boolean) : void {
			_enabled = p_bEnabled;
			onEnabledUpdated();
		}
		protected function onEnabledUpdated () : void {
			// OVERRIDE ME
		}
	}
}