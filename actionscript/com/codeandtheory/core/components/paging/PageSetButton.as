package com.codeandtheory.core.components.paging {
	import flash.display.Sprite;


	public class PageSetButton extends PageButton {
		protected var _endPageNumber:int = 0;


		public function PageSetButton (p_nStartPageNumber:int, p_nEndPageNumber:int) {
			super(p_nStartPageNumber);
			_endPageNumber = p_nEndPageNumber;
		}
		
		
		public function get startPageNumber () : int {
			return _pageNumber;
		}


		public function get endPageNumber () : int {
			return _endPageNumber;
		}
	}
}