package com.codeandtheory.core.components.paging {
	import flash.events.Event;


	public class PagingViewEvent extends Event {
		public static const PAGE_BUTTON_ROLLOVER:String = "PAGE_BUTTON_ROLLOVER";
		public static const PAGE_BUTTON_ROLLOUT:String = "PAGE_BUTTON_ROLLOUT";
		public static const PAGE_BUTTON_CLICK:String = "PAGE_BUTTON_CLICK";
		public static const NEXT_PAGE_CLICK:String = "NEXT_PAGE_CLICK";
		public static const PREVIOUS_PAGE_CLICK:String = "PREVIOUS_PAGE_CLICK";


		public static const PAGE_SET_BUTTON_ROLLOVER:String = "PAGE_SET_BUTTON_ROLLOVER";
		public static const PAGE_SET_BUTTON_ROLLOUT:String = "PAGE_SET_BUTTON_ROLLOUT";
		public static const PAGE_SET_BUTTON_CLICK:String = "PAGE_SET_BUTTON_CLICK";
		public static const NEXT_PAGE_SET_CLICK:String = "NEXT_PAGE_SET_CLICK";
		public static const PREVIOUS_PAGE_SET_CLICK:String = "PREVIOUS_PAGE_SET_CLICK";
		
		
		protected var _originalEvent:Event = null;
		
		
		public function PagingViewEvent (p_sType:String, p_xOriginalEvent:Event, p_bBubbles:Boolean = false, p_bCancellable:Boolean = false) {
			super(p_sType, p_bBubbles, p_bCancellable);
			_originalEvent = p_xOriginalEvent;
		}
		
		
		public function get originalEvent () : Event {
			return _originalEvent;
		}
	}
}