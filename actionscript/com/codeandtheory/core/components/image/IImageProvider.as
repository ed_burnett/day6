package com.codeandtheory.core.components.image {
	public interface IImageProvider {
		function get imageUrl () : String;
		function get forcedWidth () : Number;
		function get forcedHeight () :Number;
		function get preserveNativeDimensions () : Boolean;
		function get expandIfSmaller () : Boolean;
		function get backgroundColor () : Number;
		function get disableBackground () : Boolean;
		function set disableBackground (p_bDisableBackground:Boolean) : void
	}
}