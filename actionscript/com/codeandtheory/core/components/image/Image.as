package com.codeandtheory.core.components.image {
	import com.codeandtheory.core.utils.DimensionsUtils;
	import com.codeandtheory.core.utils.ShapeUtils;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;


	public class Image extends Sprite {
		public static const IMAGE_LOADED:String = "IMAGE_LOADED";
		public static const IMAGE_ERROR:String = "IMAGE_ERROR";


		protected var _provider:IImageProvider = null;


		protected var _imageContainer:Sprite = null;
		protected var _imageLoader:Loader = null;
		protected var _imageLoaderContext:LoaderContext = null;
		protected var _image:Bitmap = null;


		protected var _background:Sprite = null;
		protected var _loadingSprite:Sprite = null;
		protected var _errorSprite:Sprite = null;
		
		
		public function Image (p_xProvider:IImageProvider = null) {
			initializeView();
			if (p_xProvider != null) {
				provider = p_xProvider;// using public setter here on purpose to trigger all redraws / loading
			}
		}


		public function get provider () : IImageProvider {
			return _provider;
		}
		public function set provider (p_xProvider:IImageProvider) : void {
			_provider = p_xProvider;
			onProviderUpdated();
		}
		
		
		public function get actualImageWidth () : Number {
			return _image != null ? _image.width : _background.width;
		}
		public function get actualImageHeight () : Number {
			return _image != null ? _image.height : _background.height;
		}
		
		
		protected function onProviderUpdated () : void {
			// redraw ui
			redrawUI();

			// show loading
			if (_loadingSprite != null) {
				updateVisibility(_loadingSprite);
			}

			// discard old image
			if (_image != null && _imageContainer.contains(_image)) {
				_imageContainer.removeChild(_image);
			}

			// load the image
			if (_provider != null && _provider.imageUrl != null) {
				_imageLoader.load(new URLRequest(_provider.imageUrl), _imageLoaderContext);
			} else {
				updateVisibility(_errorSprite);
			}
		}
		
		
		protected function initializeView () : void {
			_background = new Sprite();
			_imageContainer = new Sprite();
			_loadingSprite = new Sprite();
			_errorSprite = new Sprite();

			addChild(_background);
			addChild(_imageContainer);
			addChild(_errorSprite);
			addChild(_loadingSprite);

			_imageLoaderContext = new LoaderContext(true);
			_imageLoader = new Loader();
			_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoaded, false, 0, true);
			_imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onImageError, false, 0, true);
		}


		protected function onImageLoaded (p_xEvent:Event) : void {
			_image = new Bitmap(Bitmap(_imageLoader.content).bitmapData, PixelSnapping.AUTO, true);
			DimensionsUtils.forceFit(_image, _provider.forcedWidth, _provider.forcedHeight, _provider.preserveNativeDimensions, _provider.expandIfSmaller);
			_imageContainer.addChild(_image);
			updateVisibility(_image);

			dispatchEvent(new Event(IMAGE_LOADED));
		}


		protected function onImageError (p_xEvent:IOErrorEvent) : void {
			//trace("error loading ["+ _provider.imageUrl +"] - ["+ p_xEvent +"]");
			dispatchEvent(new Event(IMAGE_ERROR));
		}
		
		
		protected function redrawUI () : void {
			if ( !_provider.disableBackground ) {
				redrawBackground();
				redrawLoading();
				redrawError();
			}
		}
		protected function redrawBackground () : void {
			ShapeUtils.redrawRectangle(_background.graphics, _provider.forcedWidth, _provider.forcedHeight, _provider.backgroundColor);
		}
		protected function redrawLoading () : void {
			ShapeUtils.redrawRectangle(_loadingSprite.graphics, _provider.forcedWidth, _provider.forcedHeight, 0x000000);
		}
		protected function redrawError () : void {
			ShapeUtils.redrawRectangle(_errorSprite.graphics, _provider.forcedWidth, _provider.forcedHeight, 0xEE0000);
		}


		protected function updateVisibility (p_xDisplayObject:DisplayObject) : void {
			if (_image != null) {
				_image.visible = p_xDisplayObject == _image;
			}
			_loadingSprite.visible = p_xDisplayObject == _loadingSprite;
			_errorSprite.visible = p_xDisplayObject == _errorSprite;
		}
	}
}