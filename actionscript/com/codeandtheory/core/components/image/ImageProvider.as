package com.codeandtheory.core.components.image {
	public class ImageProvider implements IImageProvider {
		protected var _imageUrl:String = null;
		protected var _forcedWidth:Number = 400;
		protected var _forcedHeight:Number = 300;
		protected var _backgroundColor:Number = 0;
		protected var _preserveNativeDimensions:Boolean = true;
		protected var _expandIfSmaller:Boolean = false;
		protected var _disableBackground:Boolean = false;

		public function ImageProvider (p_sImageUrl:String, p_nForcedWidth:Number = 400, p_nForcedHeight:Number = 300, p_nBackgroundColor:Number = 0x000000, p_bPreserveNativeDimensions:Boolean = true, p_bExpandIfSmaller:Boolean = false) {
			_imageUrl = p_sImageUrl;
			_forcedWidth = p_nForcedWidth;
			_forcedHeight = p_nForcedHeight;
			_backgroundColor = p_nBackgroundColor;
			_preserveNativeDimensions = p_bPreserveNativeDimensions;
			_expandIfSmaller = p_bExpandIfSmaller;
		}


		public function set disableBackground (p_bDisableBackground:Boolean) : void {
			_disableBackground = p_bDisableBackground;
		}


		public function get disableBackground () : Boolean {
			return _disableBackground;
		}


		public function get imageUrl () : String {
			return _imageUrl;
		}


		public function get forcedWidth () : Number {
			return _forcedWidth;
		}


		public function get forcedHeight () : Number {
			return _forcedHeight;
		}
		
		
		public function get backgroundColor () : Number {
			return _backgroundColor;
		}


		public function get preserveNativeDimensions () : Boolean {
			return _preserveNativeDimensions;
		}


		public function get expandIfSmaller () : Boolean {
			return _expandIfSmaller;
		}
	}
}