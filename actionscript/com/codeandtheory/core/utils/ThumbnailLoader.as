/**
* Author: Jon Harris
* Company: Code And Theory
* Date: September 2008
*/

package com.codeandtheory.core.utils {
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;	

	public class ThumbnailLoader extends Sprite {
		public static const THUMB_LOADED:String = 'THUMBNAIL_LOADER_THUMB_LOADED';
		public static const THUMB_FAILED:String = 'THUMBNAIL_LOADER_THUMB_FAILED';
		
		private var _sThumbnailUrl:String;
		private var _nWidth:Number;
		private var _nHeight:Number;
		private var _xLoader:Loader;
		private var _xImage:Bitmap;
		private var _bImageSmoothing:Boolean;
		private var _bPreserveAspectRatio:Boolean;
		private var _bLoading:Boolean;
		
		public function ThumbnailLoader() {
			
		}
		
		public function getImage():Bitmap {
			return _xImage;
		}
		
		public function getImageUrl():String {
			return _sThumbnailUrl;
		}
		
		public function load(p_sThumbnailUrl:String, p_nWidth:Number = -1, p_nHeight:Number = -1,
							 p_bPreserveAspectRatio:Boolean = true, p_bImageSmoothing:Boolean = true):void {
			_bLoading = true;
			if ( _xImage != null && contains(_xImage) ) {
				removeChild(_xImage);
			}
			_sThumbnailUrl = p_sThumbnailUrl;
			_nWidth = p_nWidth;
			_nHeight = p_nHeight;
			_bPreserveAspectRatio = p_bPreserveAspectRatio;
			_bImageSmoothing = p_bImageSmoothing;
			_xLoader = new Loader();
			var l_xLoaderContext:LoaderContext = new LoaderContext();
			l_xLoaderContext.checkPolicyFile = true;
			_xLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, thumbLoaded);
			_xLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, thumbFailed);
			_xLoader.load(new URLRequest(_sThumbnailUrl),l_xLoaderContext);
			if ( _nWidth == -1 && _nHeight == -1 ) {
				addChild(_xLoader);
			}
		}
		
		private function thumbLoaded(p_xEvent:Event):void {
			_bLoading = false;
			if ( _nWidth == -1 && _nHeight == -1 ) {
				removeChild(_xLoader);
			}
			_xImage = _xLoader.content as Bitmap;
			if ( _xImage == null ) {
				return;
			}
			if ( _nWidth != -1 && _nHeight != -1 ) {
				resize(_nWidth, _nHeight);
			}
			addChild(_xImage);
			dispatchEvent(new Event(THUMB_LOADED));
		}
		
		public function resize(p_nWidth:Number, p_nHeight:Number):void {
			_nWidth = p_nWidth;
			_nHeight = p_nHeight;
			if ( _bLoading || _xImage == null ) {
				return;
			}
			_xImage.smoothing = _bImageSmoothing;
			var l_nWidth:Number = _nWidth;
			var l_nHeight:Number = _nHeight;
			var l_nX:Number = 0;
			var l_nY:Number = 0;
			if ( _bPreserveAspectRatio ) {
				var l_aWidthAndHeight:Array = AspectUtils.getResizedDimensions(_xImage.width, _xImage.height, _nWidth, _nHeight);
				l_nWidth = Math.round(l_aWidthAndHeight[0]);
				l_nHeight = Math.round(l_aWidthAndHeight[1]);
				l_nX = (_nWidth - l_nWidth) / 2.0;
				l_nY = (_nHeight - l_nHeight) / 2.0;
			}
			_xImage.width = l_nWidth;
			_xImage.height = l_nHeight;
			_xImage.x = l_nX;
			_xImage.y = l_nY;
		}
		
		private function thumbFailed(p_xEvent:IOErrorEvent):void {
			_bLoading = false;
			if ( _nWidth == -1 && _nHeight == -1 ) {
				removeChild(_xLoader);
			}
			dispatchEvent(new Event(THUMB_FAILED));
		}
	}
}
