package com.codeandtheory.core.utils {
	import flash.display.DisplayObject;


	public class DimensionsUtils {
		public static function forceFit (p_xDisplayObject:DisplayObject, p_nWidth:Number, p_nHeight:Number, p_bPreserveNativeAspectRatio:Boolean = true, p_bExpandIfSmaller:Boolean = false) : void {
			// get original dimension information
			var l_nNativeWidth:Number = p_xDisplayObject.width;
			var l_nNativeHeight:Number = p_xDisplayObject.height;
			var l_nNativeAspectRatio:Number = l_nNativeWidth / l_nNativeHeight;


			if (p_bPreserveNativeAspectRatio) {
				if (l_nNativeAspectRatio >= 1) {// height priority (height is smaller than width)
					if (p_bExpandIfSmaller && (l_nNativeWidth < p_nWidth)) {
						p_xDisplayObject.width = p_nWidth;
						p_xDisplayObject.height = p_nWidth / l_nNativeAspectRatio;
					}
					if (l_nNativeHeight > p_nHeight) {
						p_xDisplayObject.height = p_nHeight;
						p_xDisplayObject.width = p_nHeight * l_nNativeAspectRatio; 
					}
					if (l_nNativeWidth > p_nWidth) {// then shrink width in case it's still bigger
						p_xDisplayObject.width = p_nWidth;
						p_xDisplayObject.height = p_nWidth / l_nNativeAspectRatio;
					}
				} else {// width priority (width is smaller than height)
					if (p_bExpandIfSmaller && (l_nNativeHeight < p_nHeight)) {
						p_xDisplayObject.height = p_nHeight;
						p_xDisplayObject.width = p_nHeight * l_nNativeAspectRatio;
					}
					if (l_nNativeWidth > p_nWidth) {
						p_xDisplayObject.width = p_nWidth;
						p_xDisplayObject.height = p_nWidth / l_nNativeAspectRatio;
					}
					if (l_nNativeHeight > p_nHeight) {// then shrink height in case it's still bigger
						p_xDisplayObject.height = p_nHeight;
						p_xDisplayObject.width = p_nHeight * l_nNativeAspectRatio; 
					}
				}
			} else {
				if (l_nNativeWidth > p_nWidth || l_nNativeHeight > p_nHeight || p_bExpandIfSmaller) {
					p_xDisplayObject.width = p_nWidth;
					p_xDisplayObject.height = p_nHeight;
				} 
			}
		}
		
		
		
		public function DimensionsUtils (p_xEnforcer:Class) {
			if (p_xEnforcer != StaticEnforcer) {
				throw new Error("static");
			}
		}
	}
}
class StaticEnforcer {}