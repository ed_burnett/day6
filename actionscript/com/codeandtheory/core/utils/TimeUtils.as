package com.codeandtheory.core.utils {

	public class TimeUtils {
		
		public static const SECS_PER_HOUR:Number = 3600;
		public static const SECS_PER_MIN:Number = 60;

		public function TimeUtils(p_xEnforcer:StaticEnforcer) {
			if (p_xEnforcer == null) {
				throw new Error("static only");
			}
		}
	
		public static function secsToHMS(secs:Number):String {
			if (isNaN(secs)) {
				return "0:00";
			} else {
				var val:String = "";
				if (secs>=SECS_PER_HOUR) {
					val += Math.floor(secs/SECS_PER_HOUR)+":";
					secs %= SECS_PER_HOUR;
				}
				if (secs>=SECS_PER_MIN) {
					if (val=="") {
						val = Math.floor(secs/SECS_PER_MIN)+":";
					} else {
						val += StringUtils.pad(Math.floor(secs/SECS_PER_MIN)+"", 2, "0", "left")+":";
					}
					secs %= SECS_PER_MIN;
				} else {
					if (val=="") {
						val = "0:";
					} else {
						val = "00:";
					}
					secs %= SECS_PER_MIN;
				} 
				return val + StringUtils.pad(secs+"", 2, "0", "left");
			}
		}
		
	}
	
}


class StaticEnforcer {}
