﻿/**
* Author: Jon Harris
* Company: Code And Theory
* Date: July 2008
*/

package com.codeandtheory.core.utils {
	import flash.net.ObjectEncoding;		import flash.net.SharedObject;	
	
	/**
	 * holds references the singular shared object for all comcast apps.
	 * class ok as a singleton.  each app that loads will call init and
	 * pass an EventManager for notification to the calling class.
	 */
	public class LocalStorage {
	
	    private var _so:SharedObject;
	    private var _id:String;
	
	    public function LocalStorage(id:String, as2:Boolean = true) {
		    _id = id;
		    if ( as2 ) {
		    	SharedObject.defaultObjectEncoding = ObjectEncoding.AMF0;
			}
	        _so = SharedObject.getLocal(id,"/");
	
	        if (_so.data.web==null) {
	            _so.data.web = new Object();
	        }
	   	}
	
	    public function setProperty(str:String,obj:Object):void {
	        if (str != null) {
	            _so.data.web[str] = obj;
	            _so.flush();
	        }
	    }
	
	    public function getProperty(str:String, dflt:Object):Object {
	        if (_so.data.web[str] != null) {
	            return _so.data.web[str];
	        } else {
	            return dflt;
	        }
	    }
	
	    public function removeProperty(str:String):void {
	        _so.data.web[str] = null;
	    }
	
	    public function commit():void {
	        _so.flush();
	    }
	
	    public function destroy():void {
	        _so.clear();
	    }	
	}
}