package com.codeandtheory.core.utils {
	public class StringUtils {
		
		public static function pad(str:String, places:Number, pad:String, side:String):String {
			while (str.length < places) {
				if (side == "left")
					str = pad + str;
				else
					str = str + pad;
			}
			return (str);
		}
		
		public static function forceTwoDigitString (p_nInteger:int) : String {
			if (p_nInteger < 10) {
				if (p_nInteger >= 0) {
					return "0"+ p_nInteger;
				} else if (p_nInteger < 0 && p_nInteger > -10) {
					return "-0"+ p_nInteger;
				}
			}

			return ""+ p_nInteger;
		}
		
		public static function stripHtmlTags(html:String, tags:String = ""):String {
			var tagsToBeKept:Array = new Array();
			if (tags.length > 0) {
				tagsToBeKept = tags.split(new RegExp("\\s*,\\s*"));
			}
			
			var tagsToKeep:Array = new Array();
			for (var i:int = 0; i < tagsToBeKept.length; i++) {
				if (tagsToBeKept[i] != null && tagsToBeKept[i] != "") {
					tagsToKeep.push(tagsToBeKept[i]);
				}
			}
			
			var toBeRemoved:Array = new Array();
			var tagRegExp:RegExp = new RegExp("<([^>\\s]+)(\\s[^>]+)*>", "g");
			
			var foundedStrings:Array = html.match(tagRegExp);
			for (i = 0; i < foundedStrings.length; i++) {
				var tagFlag:Boolean = false;
				if (tagsToKeep != null) {
					for (var j:int = 0; j < tagsToKeep.length; j++) {
						var tmpRegExp:RegExp = new RegExp("<\/?" + tagsToKeep[j] + "[^<>]*?>", "i");
						var tmpStr:String = foundedStrings[i] as String;
						if (tmpStr.search(tmpRegExp) != -1) { 
							tagFlag = true;
						}
					}
				}
				if (!tagFlag) {
					toBeRemoved.push(foundedStrings[i]);
				}
			}
			for (i = 0; i < toBeRemoved.length; i++) {
				var tmpRE:RegExp = new RegExp("([\+\*\$\/])","g");
				var tmpRemRE:RegExp = new RegExp((toBeRemoved[i] as String).replace(tmpRE, "\\$1"),"g");
				html = html.replace(tmpRemRE, "");
			}
			return html;
		}
		
		public static function trim(p_sString:String, p_sTrim:String = " \r\n\t"):String {
			var l_aTrimCharMap:Array = new Array();
			for ( var l_nTrimCharIndex:int = 0; l_nTrimCharIndex < p_sTrim.length; l_nTrimCharIndex++ ) {
				var l_sTrimChar:String = p_sTrim.charAt(l_nTrimCharIndex);
				l_aTrimCharMap[l_sTrimChar] = l_sTrimChar;
			}
			var r_sTrimmedString:String = '';
			for ( var l_nStartCharIndex:int = 0; l_nStartCharIndex < p_sString.length; l_nStartCharIndex++ ) {
				var l_sStartChar:String = p_sString.charAt(l_nStartCharIndex);
				if ( l_aTrimCharMap[l_sStartChar] != l_sStartChar ) {
					r_sTrimmedString = p_sString.substring(l_nStartCharIndex, p_sString.length);
					break;
				}
			}
			p_sString = new String(r_sTrimmedString);
			for ( var l_nEndCharIndex:int = p_sString.length -1; l_nEndCharIndex >= 0; l_nEndCharIndex-- ) {
				var l_sEndChar:String = p_sString.charAt(l_nEndCharIndex);
				
				if ( l_aTrimCharMap[l_sEndChar] != l_sEndChar ) {
					r_sTrimmedString = p_sString.substring(0, l_nEndCharIndex+1);
					break;
				}
			}
			return r_sTrimmedString;
		}
		
		public static function trimChar(p_sString:String, p_sTrim:String = " "):String {
			if ( p_sString != null ) {
				if ( p_sString.length > 0 ) { 
					while ( p_sString.indexOf(p_sTrim) == 0 ) {
						p_sString = p_sString.substring(p_sTrim.length, p_sString.length);
					}
					while ( p_sString.lastIndexOf(p_sTrim) == p_sString.length - p_sTrim.length ) {
						p_sString = p_sString.substring(0, p_sString.length - p_sTrim.length);
					}
				}
			}
			return p_sString;
		}
		
		public function StringUtils (p_xEnforcer:Class) {
			if (p_xEnforcer != StaticEnforcer) {
				throw new Error("static");
			}
		}
	}
}
class StaticEnforcer {}