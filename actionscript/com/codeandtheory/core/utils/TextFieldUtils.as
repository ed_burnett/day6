package com.codeandtheory.core.utils
{
	import flash.text.TextField;
	import flash.text.TextLineMetrics;
	
	public class TextFieldUtils {
		
		public static const CHARS_SPACE_TO_END:int = 5;
		
		
		public function TextFieldUtils(p_xEnforcer:StaticEnforcer) {
			if (p_xEnforcer == null) {
				throw new Error("static only");
			}
		}
		
		public static function truncateToMaxLines(p_xTextField:TextField, p_nMaxLines:int, p_sTruncateText:String = '...', p_bHtml:Boolean = false):void {
			if ( p_xTextField.numLines > p_nMaxLines ) {
				var l_sOriginalHtmlText:String = p_xTextField.htmlText;
				var l_sOriginalText:String = p_xTextField.text;
				
				var l_sShortenedText:String = '';
				for ( var l_nLineNumber:int = 0; l_nLineNumber < p_nMaxLines - 1; l_nLineNumber++ ) {
					l_sShortenedText+= p_xTextField.getLineText(l_nLineNumber);
				}
				var l_sLastLine:String = StringUtils.trim(p_xTextField.getLineText(p_nMaxLines-1));
				var l_xLastLineMetrics:TextLineMetrics = p_xTextField.getLineMetrics(p_nMaxLines-1);
				
				p_xTextField.text = p_sTruncateText;
				var l_xTruncateMetrics:TextLineMetrics = p_xTextField.getLineMetrics(0);
				
				if ( l_xLastLineMetrics.width + l_xTruncateMetrics.width <= p_xTextField.textWidth ) {
					l_sShortenedText+= l_sLastLine + p_sTruncateText;
				} 
				else {
					var l_nLastWordLength:int = l_sLastLine.length;
					if ( l_sLastLine.lastIndexOf(" ") != -1 ) {
						l_nLastWordLength = l_sLastLine.length - l_sLastLine.lastIndexOf(" ")
					}
					if ( l_nLastWordLength > 5 ) {
						l_sShortenedText+= l_sLastLine.substring(0, l_sLastLine.length - p_sTruncateText.length) + p_sTruncateText;
					}
					else {
						l_sShortenedText+= l_sLastLine.substring(0, l_sLastLine.length - l_nLastWordLength) + p_sTruncateText;
					}
				}
				if ( p_bHtml ) {
					var l_sShortenedHtmlText:String = '';
					var l_bInsideTag:Boolean = false;
					var l_bInsideCharEntity:Boolean = false;
					var l_sCurrentChar:String;
					var l_nNonHtmlCharCount:int = 0;
					for ( var l_nCharIndex:int = 0; l_nCharIndex < l_sOriginalHtmlText.length; l_nCharIndex++ ) {
						l_sCurrentChar = l_sOriginalHtmlText.charAt(l_nCharIndex);
						if ( !l_bInsideTag ) {
							if ( !l_bInsideCharEntity ) {
								if ( l_sCurrentChar == '<' ) {
									l_bInsideTag = true;
									l_sShortenedHtmlText+= l_sCurrentChar;
								}
								else if ( l_sCurrentChar == '&' ) {
									l_bInsideCharEntity = true;
									if ( l_nNonHtmlCharCount < l_sShortenedText.length - p_sTruncateText.length ) {
										l_sShortenedHtmlText+= l_sCurrentChar;
									}
								}
								else {
									if ( l_nNonHtmlCharCount < l_sShortenedText.length - p_sTruncateText.length ) {
										l_sShortenedHtmlText+= l_sCurrentChar;
									}
									else if ( l_nNonHtmlCharCount == l_sShortenedText.length - p_sTruncateText.length ) {
										l_sShortenedHtmlText+= p_sTruncateText;
									}
									l_nNonHtmlCharCount++;
								}
							}
							else {
								if ( l_nNonHtmlCharCount < l_sShortenedText.length - p_sTruncateText.length ) {
									l_sShortenedHtmlText+= l_sCurrentChar;
								}
								if ( l_sCurrentChar == ';' ) {
									l_bInsideCharEntity = false;
									if ( l_nNonHtmlCharCount == l_sShortenedText.length - p_sTruncateText.length ) {
										l_sShortenedHtmlText+= p_sTruncateText;
									}
									l_nNonHtmlCharCount++;
								}
							}
						}
						else {
							if ( l_sCurrentChar == '>' ) {
								l_bInsideTag = false;
							}
							l_sShortenedHtmlText+= l_sCurrentChar;
						}
					}
					p_xTextField.htmlText = l_sShortenedHtmlText;
				}
				else {
					p_xTextField.text = l_sShortenedText;
				}
			}
		}
	}
}

class StaticEnforcer {}