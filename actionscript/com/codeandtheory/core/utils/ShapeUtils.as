package com.codeandtheory.core.utils {
	import flash.display.CapsStyle;
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.display.Sprite;
	import flash.geom.Matrix;


	public class ShapeUtils {
		public static function newTriangle (p_nWidth:Number, p_nHeight:Number, p_nColor:Number = 0x00EE00) : Sprite {
			var ret_xTriangle:Sprite = new Sprite();
			redrawTriangle(ret_xTriangle.graphics, p_nWidth, p_nHeight, p_nColor);
			return ret_xTriangle;
		}


		public static function redrawTriangle (p_xGraphics:Graphics, p_nWidth:Number, p_nHeight:Number, p_nColor:Number = 0x00EE00) : void {
			p_xGraphics.clear();
			p_xGraphics.beginFill(p_nColor, 1);
			p_xGraphics.moveTo(0, p_nHeight);
			p_xGraphics.lineTo(p_nWidth, p_nHeight);
			p_xGraphics.lineTo((p_nWidth / 2), 0);
			p_xGraphics.lineTo(0, p_nHeight);
			p_xGraphics.endFill();
		}


		public static function newSquare (p_nLength:Number, p_nColor:Number = 0x00EE00) : Sprite {
			return newRectangle(p_nLength, p_nLength, p_nColor);
		}
		
		
		public static function redrawSquare (p_xGraphics:Graphics, p_nLength:Number, p_nColor:Number = 0x00EE00): void {
			redrawRectangle(p_xGraphics, p_nLength, p_nLength, p_nColor); 
		}


		public static function newRectangle (p_nWidth:Number, p_nHeight:Number, p_nColor:Number = 0x00EE00) : Sprite {
			var ret_xRectangle:Sprite = new Sprite();
			redrawRectangle(ret_xRectangle.graphics, p_nWidth, p_nHeight, p_nColor);
			return ret_xRectangle;
		}


		public static function redrawRectangle (p_xGraphics:Graphics, p_nWidth:Number, p_nHeight:Number, p_nColor:Number = 0x00EE00) : void {
			p_xGraphics.clear();
			p_xGraphics.beginFill(p_nColor, 1);
			p_xGraphics.drawRect(0, 0, p_nWidth, p_nHeight);
			p_xGraphics.endFill();
		}
		
		
		public static function newBorderRectange (p_nThickness:Number, p_nWidth:Number, p_nHeight:Number, p_nColor:Number = 0x00EE00) : Sprite {
			var ret_xRectange:Sprite = new Sprite();
			redrawBorderRectangle(ret_xRectange.graphics, p_nThickness, p_nWidth, p_nHeight, p_nColor);
			return ret_xRectange;
		}
		
		
		public static function redrawBorderRectangle (p_xGraphics:Graphics, p_nThickness:Number, p_nWidth:Number, p_nHeight:Number, p_nColor:Number = 0x00EE00) : void {
			var l_nHalf:Number = p_nThickness / 2;
			
			p_xGraphics.clear();
			p_xGraphics.lineStyle(p_nThickness, p_nColor, 1, true, LineScaleMode.NONE, CapsStyle.NONE, JointStyle.MITER);
			p_xGraphics.drawRect(l_nHalf, l_nHalf, p_nWidth - p_nThickness, p_nHeight - p_nThickness);
			p_xGraphics.endFill();
		}
		
		
		public static function newRoundedRectangle (p_nWidth:Number, p_nHeight:Number, p_nEllipseWidth:Number = 5, p_nEllipseHeight:Number = 5, p_nColor:Number = 0x00EE00, p_sGradientType:String = null, p_anColors:Array = null, p_anAlphas:Array = null, p_anRatios:Array = null, p_xMatrix:Matrix = null) : Sprite {
			var ret_xRoundedRectangle:Sprite = new Sprite();
			redrawRoundedRectangle(ret_xRoundedRectangle.graphics, p_nWidth, p_nHeight, p_nEllipseWidth, p_nEllipseHeight, p_nColor, p_sGradientType, p_anColors, p_anAlphas, p_anRatios, p_xMatrix);			
			return ret_xRoundedRectangle;
		}
		
		
		public static function redrawRoundedRectangle (p_xGraphics:Graphics, p_nWidth:Number, p_nHeight:Number, p_nEllipseWidth:Number = 5, p_nEllipseHeight:Number = 5, p_nColor:Number = 0x00EE00, p_sGradientType:String = null, p_anColors:Array = null, p_anAlphas:Array = null, p_anRatios:Array = null, p_xMatrix:Matrix = null) : void {
			p_xGraphics.clear();

			if (p_sGradientType == null) {
				p_xGraphics.beginFill(p_nColor, 1);
			} else {
				p_xGraphics.beginGradientFill(p_sGradientType, p_anColors, p_anAlphas, p_anRatios, p_xMatrix);
			}
			p_xGraphics.drawRoundRect(0, 0, p_nWidth, p_nHeight, p_nEllipseWidth, p_nEllipseHeight);
			p_xGraphics.endFill();
		}


		public static function newCircle (p_nDiameter:Number, p_nColor:Number = 0x00EE00) : Sprite {
			var ret_xCircle:Sprite = new Sprite();
			redrawCircle(ret_xCircle.graphics, p_nDiameter, p_nColor);
			return ret_xCircle;
		}


		public static function redrawCircle (p_xGraphics:Graphics, p_nDiameter:Number, p_nColor:Number = 0x00EE00) : void {
			p_xGraphics.clear();
			p_xGraphics.beginFill(p_nColor, 1);
			p_xGraphics.drawCircle(p_nDiameter / 2, p_nDiameter / 2, p_nDiameter / 2);
			p_xGraphics.endFill();			
		}


		public function ShapeUtils (p_xEnforcer:Class) {
			if (p_xEnforcer != StaticEnforcer) {
				throw new Error("static");
			}
		}
	}
}
class StaticEnforcer {}