package com.codeandtheory.core.utils {
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;


	public class DisplayObjectContainerUtils {
		public static const REGISTRATION_POINT_TOP:String = "REGISTRATION_POINT_TOP";
		public static const REGISTRATION_POINT_LEFT:String = "REGISTRATION_POINT_LEFT";
		public static const REGISTRATION_POINT_RIGHT:String = "REGISTRATION_POINT_RIGHT";
		public static const REGISTRATION_POINT_CENTER:String = "REGISTRATION_POINT_CENTER";
		public static const REGISTRATION_POINT_BOTTOM:String = "REGISTRATION_POINT_BOTTOM";


		public static function distibuteChildrenByWidth (p_xContainer:DisplayObjectContainer, p_sChildRegistrationPoint:String, p_nOverrideWidth:Number = -1) : void {
			// check parameters
			if (p_xContainer == null) {
				return;
			}

			// calculate distribution widths
			var l_nChildCount:int = p_xContainer.numChildren;
			var l_nTotalWidth:Number = p_nOverrideWidth >= 0 ? p_nOverrideWidth : p_xContainer.width;
			var l_nSectionWidth:Number = l_nTotalWidth / l_nChildCount;


			// distribute by width
			var l_nTempX:Number = l_nSectionWidth / 2;
			var l_xTempObject:DisplayObject = null;
			for (var i:int=0; i<l_nChildCount; i++) {
				l_xTempObject = p_xContainer.getChildAt(i);

				switch (p_sChildRegistrationPoint) {
					case REGISTRATION_POINT_CENTER:
						l_xTempObject.x = l_nTempX;
						break;
						
					case REGISTRATION_POINT_RIGHT:
						l_xTempObject.x = l_nTempX + l_xTempObject.width / 2;
						break;

					case REGISTRATION_POINT_LEFT:
					default:
						l_xTempObject.x = l_nTempX - l_xTempObject.width / 2;
						break;
				}

				l_nTempX += l_nSectionWidth;				
			}
		}
		
		
		public static function spaceChildrenByWidth (p_xContainer:DisplayObjectContainer, p_nSpacing:Number, p_sChildRegistrationPoint:String) : void {
			// check parameters
			if (p_xContainer == null) {
				return;
			}

			// space out children
			var l_nChildCount:int = p_xContainer.numChildren;
			var l_nTempX:Number = 0;
			var l_xTempObject:DisplayObject = null;
			for (var i:int=0; i<l_nChildCount; i++) {
				l_xTempObject = p_xContainer.getChildAt(i);

				switch (p_sChildRegistrationPoint) {
					case REGISTRATION_POINT_CENTER:
						l_xTempObject.x = l_nTempX + l_xTempObject.width / 2;
						break;
						
					case REGISTRATION_POINT_RIGHT:
						l_xTempObject.x = l_nTempX + l_xTempObject.width;
						break;

					case REGISTRATION_POINT_LEFT:
					default:
						l_xTempObject.x = l_nTempX;
						break;
				}

				l_nTempX += l_xTempObject.width + p_nSpacing;				
			}
		}



		public static function removeAllChildren (p_xContainer:DisplayObjectContainer) : void {
			// check parameters
			if (p_xContainer == null) {
				return;
			}

			// otherwise, remove all children
			var l_nChildCount:int = p_xContainer.numChildren;
			for (var i:int=(l_nChildCount - 1); i>=0; i--) {
				p_xContainer.removeChildAt(i);
			}
		}
		
		
		
		public function DisplayObjectContainerUtils (p_xEnforcer:Class) {
			if (p_xEnforcer != StaticEnforcer) {
				throw new Error("static");
			}
		}
	}
}
class StaticEnforcer {}