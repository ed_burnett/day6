/**
* Author: Jon Harris
* Company: Code And Theory
* Date: July 2008
*/

package com.codeandtheory.core.utils {
	import flash.display.DisplayObject;	import flash.display.DisplayObjectContainer;	import flash.display.Sprite;	
	public class DisplayObjectUtils {
		
		public function DisplayObjectUtils(p_xEnforcer:StaticEnforcer) {
			if (p_xEnforcer == null) {
				throw new Error("static only");
			}
		}
		
		public static function isDescendant(p_xChild:DisplayObject, p_xAncestor:DisplayObject):Boolean {
			var l_xCurrent:DisplayObject = p_xChild;
			if ( l_xCurrent == p_xAncestor ) {
				return true;
			}
			while ( l_xCurrent != null ) {
				if ( l_xCurrent.parent == p_xAncestor ) {
					return true;
				}
				l_xCurrent = l_xCurrent.parent;
			}
			return false;
		}
		
		public static function swapChild(p_xParent:DisplayObjectContainer, p_xOldChild:DisplayObject, p_xNewChild:DisplayObject):void {
			if ( p_xParent != null && p_xOldChild != null && p_xNewChild != null && p_xParent.contains(p_xOldChild) ) { 
				var l_nIndex:int = p_xParent.getChildIndex(p_xOldChild);
				p_xParent.removeChildAt(l_nIndex);
				p_xParent.addChildAt(p_xNewChild, l_nIndex);
			}
		}
		
		public static function setButtonMode(p_bButtonMode:Boolean, ...p_xSprites):void {
			for each ( var l_xSprite:Sprite in p_xSprites ) {
				l_xSprite.buttonMode = p_bButtonMode;
			}
		}
		
		public static function removeAllChildren(p_xParent:DisplayObjectContainer):void {
			if ( p_xParent == null ) { return; }
			var l_nNumChildren:int = p_xParent.numChildren;
			for ( var l_nIndex:int = 0; l_nIndex < l_nNumChildren; l_nIndex++ ) {
				p_xParent.removeChildAt(0);
			}
		}
	}
}
 
class StaticEnforcer {}
