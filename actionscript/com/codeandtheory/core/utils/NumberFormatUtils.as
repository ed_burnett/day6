package com.codeandtheory.core.utils {
	
	public class NumberFormatUtils {
		
		public static const SECS_PER_HOUR:Number = 3600;
		public static const SECS_PER_MIN:Number = 60;
		
		public function NumberFormatUtils(p_xEnforcer:StaticEnforcer) {
			if (p_xEnforcer == null) {
				throw new Error("static only");
			}
		}

		public static function secsToHMS(secs:Number, p_bForceTwoDigits:Boolean = false):String {
			if (isNaN(secs)) {
				return (p_bForceTwoDigits?"0":"")+"0:00";
			} else {
				var val:String = "";
				if (secs>=SECS_PER_HOUR) {
					val += Math.floor(secs/SECS_PER_HOUR)+":";
					secs %= SECS_PER_HOUR;
				}
				if (secs>=SECS_PER_MIN) {
					if (val=="") {
						if ( p_bForceTwoDigits ) {
							val = StringUtils.pad(Math.floor(secs/SECS_PER_MIN)+"", 2, "0", "left")+":";
						}
						else {
							val = Math.floor(secs/SECS_PER_MIN)+":";
						}
					} else {
						val += StringUtils.pad(Math.floor(secs/SECS_PER_MIN)+"", 2, "0", "left")+":";
					}
					secs %= SECS_PER_MIN;
				} else {
					if (val=="") {
						val = (p_bForceTwoDigits?"0":"")+"0:";
					} else {
						val = "00:";
					}
					secs %= SECS_PER_MIN;
				} 
				return val + StringUtils.pad(secs+"", 2, "0", "left");
			}
		}
		
		public static function formatPercentage(p_nPercentage:Number, p_bShowOutOfOneHundred:Boolean, p_nDecimalPlaces:int):Number {
			if ( p_bShowOutOfOneHundred ) {
				p_nPercentage*= 100;
			}
			var l_nMultiplier:int = Math.pow( 10, p_nDecimalPlaces );
			return Math.round(p_nPercentage * l_nMultiplier) / l_nMultiplier;
		}

	}
}

class StaticEnforcer {}