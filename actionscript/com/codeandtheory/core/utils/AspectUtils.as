/**
* Author: Jon Harris
* Company: Code And Theory
* Date: Feb 2008
*/

package com.codeandtheory.core.utils {

	public class AspectUtils {
		
		import flash.display.DisplayObject;
		
		public function AspectUtils(p_xEnforcer:StaticEnforcer) {
			if (p_xEnforcer == null) {
				throw new Error("static only");
			}
		}
		
		public static function forceFit (p_xDisplayObject:DisplayObject, p_nWidth:Number, p_nHeight:Number, p_bPreserveNativeAspectRatio:Boolean = true, p_bExpandIfSmaller:Boolean = false) : void {
			// get original dimension information
			var l_nNativeWidth:Number = p_xDisplayObject.width;
			var l_nNativeHeight:Number = p_xDisplayObject.height;
			var l_nNativeAspectRatio:Number = l_nNativeWidth / l_nNativeHeight;

			if (p_bPreserveNativeAspectRatio) {
				if (l_nNativeAspectRatio >= 1) {// height priority (height is smaller than width)
					if (p_bExpandIfSmaller && (l_nNativeWidth < p_nWidth)) {
						p_xDisplayObject.width = p_nWidth;
						p_xDisplayObject.height = p_nWidth / l_nNativeAspectRatio;
					}
					if (l_nNativeHeight > p_nHeight) {
						p_xDisplayObject.height = p_nHeight;
						p_xDisplayObject.width = p_nHeight * l_nNativeAspectRatio; 
					}
					if (l_nNativeWidth > p_nWidth) {// then shrink width in case it's still bigger
						p_xDisplayObject.width = p_nWidth;
						p_xDisplayObject.height = p_nWidth / l_nNativeAspectRatio;
					}
				} else {// width priority (width is smaller than height)
					if (p_bExpandIfSmaller && (l_nNativeHeight < p_nHeight)) {
						p_xDisplayObject.height = p_nHeight;
						p_xDisplayObject.width = p_nHeight * l_nNativeAspectRatio;
					}
					if (l_nNativeWidth > p_nWidth) {
						p_xDisplayObject.width = p_nWidth;
						p_xDisplayObject.height = p_nWidth / l_nNativeAspectRatio;
					}
					if (l_nNativeHeight > p_nHeight) {// then shrink height in case it's still bigger
						p_xDisplayObject.height = p_nHeight;
						p_xDisplayObject.width = p_nHeight * l_nNativeAspectRatio; 
					}
				}
			} else {
				if (l_nNativeWidth > p_nWidth || l_nNativeHeight > p_nHeight || p_bExpandIfSmaller) {
					p_xDisplayObject.width = p_nWidth;
					p_xDisplayObject.height = p_nHeight;
				} 
			}
		}
		
		public static function getResizedDimensions(p_nInputWidth:Number, p_nInputHeight:Number, p_nTargetWidth:Number, p_nTargetHeight:Number):Array {
			var l_nScale:Number = 1;
			if ( p_nInputWidth / (1.0 * p_nTargetWidth) > p_nInputHeight / (1.0 * p_nTargetHeight) ) {
				l_nScale = p_nTargetWidth / (1.0 * p_nInputWidth);
			}
			else {
				l_nScale = p_nTargetHeight / (1.0 * p_nInputHeight);
			}
			var r_nWidth:Number = p_nInputWidth * l_nScale;
			var r_nHeight:Number = p_nInputHeight * l_nScale;
			return [r_nWidth, r_nHeight];
		}
		
		public static function getResizedDimensionsByWidth(p_nInputWidth:Number, p_nInputHeight:Number, p_nTargetWidth:Number):Array {
			var l_nScale:Number = p_nTargetWidth / p_nInputWidth;
			var r_nWidth:Number = p_nTargetWidth;
			var r_nHeight:Number = p_nInputHeight * l_nScale;
			return [r_nWidth, r_nHeight];
		}
		
	}
	
}


class StaticEnforcer {}
