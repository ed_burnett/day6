package com.codeandtheory.core.utils {
	
	public class EmailUtils {
		
		public static var VALID_EMAIL_REGEXP:RegExp = /^([\w]+)(.[\w]+)*@([\w|\.|-]+)(\.[\w|-]{2,3}){1,2}$/; 
		
		public function EmailUtils(p_xEnforcer:Class) {
			if (p_xEnforcer != StaticEnforcer) {
				throw new Error("static");
			}
		}
		
		public static function isValidEmail(p_sEmail:String):Boolean {
			return VALID_EMAIL_REGEXP.test(p_sEmail);
		}
	}
}
class StaticEnforcer {}