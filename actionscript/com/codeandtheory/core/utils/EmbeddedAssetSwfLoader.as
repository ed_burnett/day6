/**
* Author: Jon Harris
* Company: Code And Theory
* Date: July 2008
*/

package com.codeandtheory.core.utils {
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.system.ApplicationDomain;
	
	import mx.core.MovieClipLoaderAsset;		

	public class EmbeddedAssetSwfLoader extends EventDispatcher {
		public static const EMBEDDED_ASSET_LOADED:String = "EMBEDDED_ASSET_LOADED";
		
		private var _xAssetSwfClass:Class;
		private var _xAssetSwfIntance:MovieClipLoaderAsset;
		private var _xApplicationDomain:ApplicationDomain;

		public function EmbeddedAssetSwfLoader(p_xAssetSwfClass:Class):void {
			_xAssetSwfClass = p_xAssetSwfClass;
			_xAssetSwfIntance = new _xAssetSwfClass();
		}
		
		public function loadAssetSwf():void {
			Loader(_xAssetSwfIntance.getChildAt(0)).contentLoaderInfo.addEventListener(
				Event.INIT, embeddedAssetSwfLoaded);
		}
		
		private function embeddedAssetSwfLoaded(p_xEvent:Event):void {
			_xApplicationDomain = p_xEvent.target.content.loaderInfo.applicationDomain;
			dispatchEvent(new Event(EMBEDDED_ASSET_LOADED));
		}
		
		public function getSymbolInstance(p_sSymbolClassName:String):MovieClip {
			var l_xSymbolClass:Class = _xApplicationDomain.getDefinition(p_sSymbolClassName) as Class;
			if ( l_xSymbolClass != null ) {
				return new l_xSymbolClass();
			}
			else {
				return null;
			}
		}
	}
}
