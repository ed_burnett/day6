/**
* Author: Jon Harris
* Company: Code And Theory
* Date: March 2008
*/

package com.codeandtheory.core.utils {
	
	import flash.display.Graphics;
		
	public class RoundCornersRectangle {
		
		public function RoundCornersRectangle() {
			
		}
		
		public static function drawRoundCornersRectangle(p_xGraphics:Graphics, p_nX:Number, p_nY:Number, p_nWidth:Number, p_nHeight:Number,
												         p_nTopLeftRadius:Number = 0, p_nTopRightRadius:Number = 0,
													     p_nBottomRightRadius:Number = 0, p_nBottomLeftRadius:Number = 0) : void {
			
			p_xGraphics.moveTo(p_nX, p_nY+p_nTopLeftRadius);
			p_xGraphics.curveTo(p_nX, p_nY, p_nX+p_nTopLeftRadius, p_nY);
			p_xGraphics.lineTo(p_nX+p_nWidth-p_nTopRightRadius, p_nY);
			p_xGraphics.curveTo(p_nX+p_nWidth, p_nY, p_nX+p_nWidth, p_nY+p_nTopRightRadius);
			p_xGraphics.lineTo(p_nX+p_nWidth, p_nY+p_nHeight-p_nBottomRightRadius);
			p_xGraphics.curveTo(p_nX+p_nWidth, p_nY+p_nHeight, p_nX+p_nWidth-p_nBottomRightRadius, p_nY+p_nHeight);
			p_xGraphics.lineTo(p_nX+p_nBottomLeftRadius, p_nY+p_nHeight);
			p_xGraphics.curveTo(p_nX, p_nY+p_nHeight, p_nX, p_nY+p_nHeight - p_nBottomLeftRadius);
			p_xGraphics.lineTo(p_nX, p_nY+p_nTopLeftRadius);
		}
		
	}
	
}