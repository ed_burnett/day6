package com.codeandtheory.core.utils {
	public class XmlUtils {
		public static function getFirstChildNode (p_xNode:XML, p_sName:String = null) : XML {
			var l_xChildren:XMLList = p_sName == null ? p_xNode.children() : p_xNode.child(p_sName);
			for each (var l_xTempChild:XML in l_xChildren) {
				return l_xTempChild;
			}
			
			return null;
		}
	
		
		public static function getFirstChildNodeValue (p_xNode:XML, p_sNodeName:String) : String {
			var l_xChildren:XMLList = p_xNode.child(p_sNodeName);
			for each (var l_xTempChild:XML in l_xChildren) {
				return l_xTempChild.valueOf().toString();
			}
			
			return null;
		}


		public function XmlUtils (p_xEnforcer:Class) {
			if (p_xEnforcer != StaticEnforcer) {
				throw new Error("static");
			}
		}
	}
}
class StaticEnforcer {}