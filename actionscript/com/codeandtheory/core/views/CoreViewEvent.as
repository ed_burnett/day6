package com.codeandtheory.core.views {
	import flash.events.Event;


	public class CoreViewEvent extends Event {
		private var _data:Object = null;
		public function get data () : Object {
			return _data;
		}


		public function CoreViewEvent (p_sType:String, p_xData:Object = null, p_bBubbles:Boolean = false, p_bCancellable:Boolean = false) {
			_data = p_xData;
			super(p_sType, p_bBubbles, p_bCancellable);
		}
	}
}