package com.codeandtheory.core.assets {
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.system.ApplicationDomain;


	public class CoreEmbeddedAssetSwf extends EventDispatcher {
		private var AssetClass:Class = null;
		private var _loader:Loader = null;
		private var _applicationDomain:ApplicationDomain = null;


		public function CoreEmbeddedAssetSwf (p_xAssetSwfClass:Class) {
			AssetClass = p_xAssetSwfClass;
		}


		public function load () : void {
			// not if we are loading or already loaded
			if (_loader != null || _applicationDomain != null) {
				return;
			}

			// create the loader and go
			var l_xAssetSwfInstance:Sprite = new AssetClass();
			_loader = Loader(l_xAssetSwfInstance.getChildAt(0));
			_loader.contentLoaderInfo.addEventListener(Event.INIT, onInit, false, 0, true);
		}
		
		
		public function get loaded () : Boolean {
			return _loader != null && _loader.content != null;
		}


		public function newSymbolInstance (p_sSymbolClassName:String) : MovieClip {
			// if we are not loaded, then return nothing
			if (_applicationDomain == null) {
				return null;
			}

			// if this symbol exists, return it
			try {
				var l_xSymbolClass:Class = _applicationDomain.getDefinition(p_sSymbolClassName) as Class;
				if (l_xSymbolClass != null) {
					return new l_xSymbolClass();
				}
			} catch (e:Error) {// use a try catch b/c if symbol name doesn't exist, you get an error
				return null;
			}

			// otherwise, return null
			return null;
		}


		private function onInit (p_xEvent:Event) : void {
			// stop listening
			_loader.removeEventListener(Event.INIT, onInit);

			// set the domain
			_applicationDomain = p_xEvent.target.content.loaderInfo.applicationDomain;

			// notify any listeners
			dispatchEvent(new Event(Event.INIT));
		}
	}
}